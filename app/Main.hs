module Main where

import Aoc_2020
import Aoc_2021
import Aoc_2023

main :: IO ()
main = do
    putStrLn "Choose a year: [2020,2021,2023]"
    year <- read <$> getLine :: IO Int
    putStrLn $ "Which challenge from " ++ show year ++ " should be executed? [1-25]"
    task <- (read :: String -> Int) <$> getLine
    putStrLn "Choose a task or example: [input,example] (input is default)"
    variant <- getLine :: IO String
    let dirPath = "data/" ++ show year ++ "/" ++ show task ++ "/"
    input <- if variant == "example"
        then readFile $ dirPath ++ "example.txt"
        else readFile $ dirPath ++ "input.txt"
    case year of
        2020 -> run2020 task input
        2021 -> run2021 task input
        2023 -> run2023 task input
        _ -> putStrLn "Year not implemented (yet)"
