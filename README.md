# AOC
My advent of code solutions (see https://adventofcode.com/ for more information).

Current State:
- 2020: Missing following tasks:
    1. Day 20: Part2
- 2021: Completed

# Build the project
To run this project the Haskell Build Tool [Stack](https://docs.haskellstack.org/en/stable/) is
required. It will download and compile all dependencies for you when running:

```shell
stack build
```

# Execute Tasks
To execute any task just run:
```shell
echo "<YEAR>\n<DAY>\n<TYPE>" | stack run
```
where type is either _example_ or any other word, representing the puzzle input.
_example_ runs the example from the website (if it exists) and otherwise
the puzzle input.
Both can be found under `data/<YEAR>/<DAY>`.

To run the example task for 2021/19, following can be executed:

```shell
echo "2021\n19\nexample" | stack run
```

# Update Compiler Version
1. Check the newest haskell-language-server version available (recommended is to use [ghcup](https://www.haskell.org/ghcup/) for this).
2. Lookup https://www.stackage.org/ for the latest LTS version matching this version.
3. Update `resolver` in `stack.yaml`
