{-# LANGUAGE TemplateHaskell #-}
module Y2023.Aoc_2023_02
    (run02,
    )
where

import Control.Lens (view)
import Control.Lens.TH (makeLenses)
import Data.Functor.Identity
import qualified Data.Map as M
import Text.Parsec hiding (Empty, State)

import Utility.BaseUtil (computationTime)
import Utility.ParseUtil (parsePositiveNumber)

data Draw = Draw {
    _red :: !Int,
    _green :: !Int,
    _blue :: !Int
} deriving (Show, Eq)
makeLenses ''Draw
data Game = Game {
    _gameId :: !Int,
    _draws :: ![Draw]
    } deriving (Show, Eq)
makeLenses ''Game

--- Parsing ---
parseInput :: ParsecT String u Identity [Game]
parseInput = parseLine `endBy` newline

parseLine :: ParsecT String u Identity Game
parseLine = do
    string "Game "
    id <- parsePositiveNumber
    char ':'
    draws <- parseDraw `sepBy` char ';'
    return Game { _gameId = id, _draws = draws}

parseDraw :: ParsecT String u Identity Draw
parseDraw = do
     spaces
     colorPairs <- M.fromList <$> parseColor `sepBy` char ','
     return Draw {
             _blue = M.findWithDefault 0 "blue" colorPairs,
             _red = M.findWithDefault 0 "red" colorPairs,
             _green = M.findWithDefault 0 "green" colorPairs
             }

parseColor :: ParsecT String u Identity (String, Int)
parseColor = do
    spaces
    amount <- parsePositiveNumber
    spaces
    color <- many1 (noneOf ",; \n")
    return (color, amount)

--- part 1 & 2 ---
thresholdRed = 12
thresholdGreen = 13
thresholdBlue = 14

isGameValid :: Game -> Bool
isGameValid = areDrawsValid . view draws

areDrawsValid :: [Draw] -> Bool
areDrawsValid = all isDrawValid

isDrawValid :: Draw -> Bool
isDrawValid d = view red d <= thresholdRed && view blue d <= thresholdBlue && view green d <= thresholdGreen

filterValidGames :: [Game] -> [Game]
filterValidGames = filter isGameValid

part1 :: [Game] -> Int
part1 = foldr ((+) . view gameId ) 0 . filterValidGames

findLowerLimit :: Game -> Draw
findLowerLimit g = foldr (\d acc ->
                         Draw {_red = max (view red d) (view red acc),
                               _green = max (view green d) (view green acc),
                               _blue = max (view blue d) (view blue acc)}
                           ) (Draw {_red = 0, _blue = 0, _green = 0}) (view draws g)

part2 :: [Game] -> Int
part2 = foldr ((+) . power . findLowerLimit) 0
    where power :: Draw -> Int
          power d = view red d * view blue d * view green d

run02 :: String -> IO ()
run02 input = do
    let parsed = runParser parseInput () "" input
    case parsed of
          Left e -> print e
          Right gs -> do
              computationTime $ putStrLn $ "Part1: " ++ show (part1 gs)
              computationTime $ putStrLn $ "Part2: " ++ show (part2 gs)
