{-# LANGUAGE TemplateHaskell #-}
module Y2023.Aoc_2023_13
( run13,
)
where

import Control.Lens (over,set,view)
import Control.Lens.TH (makeLenses)
import Control.Monad (when)
import Control.Monad.Writer
import Data.Bifunctor (bimap)
import qualified Data.Set as S
import qualified Data.Vector as V
import Text.Parsec hiding (Empty, State)

import Utility.BaseUtil (computationTime)
import Utility.VectorUtil

-- | Data structures
data FieldType = Ash | Rock deriving (Show, Eq)
data Field = Field {
    _content :: !(Grid FieldType),
    _rows :: !Int,
    _columns :: !Int
    }
makeLenses ''Field
-- | Parsing
parseInput :: Parsec String u [Field]
parseInput = parseField `sepBy` newline

parseField :: Parsec String u Field
parseField = do
    content <- V.fromList <$> parseLine `endBy` newline
    let rows = V.length content
    let columns = V.length . V.head $ content
    return Field {
        _content = content,
        _rows = rows,
        _columns = columns
        }

parseLine :: Parsec String u (V.Vector FieldType)
parseLine = V.fromList . map charToType <$> many1 (noneOf "\n")

charToType :: Char -> FieldType
charToType c
    | c == '.' = Ash
    | c == '#' = Rock
    | otherwise = error $ "Unexpected character: " ++ show c

-- | Part 1
rowMirror :: Field -> Int -> Int -> Bool
rowMirror field currentColumn candidate =
        let rowsEachSide = min (view rows field - candidate - 1) (candidate +1)
            start = candidate - rowsEachSide + 1
            end = candidate + rowsEachSide
            in all (\currentRow ->
                   let topEntry = view content field !!! (currentRow, currentColumn)
                       bottomRow = (candidate-currentRow)*2+currentRow +1
                       bottomEntry = view content field !!! (bottomRow, currentColumn)
                       in topEntry == bottomEntry
                   ) [start..candidate]

columnMirror :: Field -> Int -> Int -> Bool
columnMirror field currentRow candidate =
        let columnsEachSide = min (view columns field - candidate - 1) (candidate +1)
            start = candidate - columnsEachSide + 1
            end = candidate + columnsEachSide
            in all (\currentColumn ->
                   let topEntry = view content field !!! (currentRow, currentColumn)
                       bottomColumn = (candidate-currentColumn)*2+currentColumn +1
                       bottomEntry = view content field !!! (currentRow, bottomColumn)
                       in topEntry == bottomEntry
                   ) [start..candidate]

findMirrorCandidates :: (Field -> Int -> Int -> Bool) -> Field -> Int -> [Int] -> [Int]
findMirrorCandidates f field current = filter (f field current)

findYSymmetry :: Field -> S.Set Int
findYSymmetry field =
        S.fromList $ foldr (findMirrorCandidates rowMirror field) [0..view rows field - 2] [0..view columns field - 1]

findXSymmetry :: Field -> S.Set Int
findXSymmetry field =
        S.fromList $ foldr (findMirrorCandidates columnMirror field) [0..view columns field - 2] [0..view rows field - 1]

findSymmetries :: Field -> (S.Set Int, S.Set Int)
findSymmetries field =
        let xSymmetries = findXSymmetry field
            ySymmetries = findYSymmetry field
            in (xSymmetries, ySymmetries)

computeSymmetryValues :: (S.Set Int, S.Set Int) -> Int
computeSymmetryValues (xSymmetries, ySymmetries) =
            sum $ map (+1) (S.toList xSymmetries) ++ map ((*) 100 . (+) 1) (S.toList ySymmetries)

runPart1 :: String -> IO ()
runPart1 input = do
    let parsed = runParser parseInput () "" input
    case parsed of
        Left e -> print e
        Right fields -> do
            putStr "Part 1: "
            print . sum . map (computeSymmetryValues . findSymmetries) $ fields

-- | Part 2
toggleFieldEntry :: FieldType -> FieldType
toggleFieldEntry Rock = Ash
toggleFieldEntry Ash = Rock

findDifferentSymmetry :: Field -> Int
findDifferentSymmetry field =
        let (xSymInitial, ySymInitial) = findSymmetries field
            in V.head . V.filter (0 /=) . V.head . V.filter (not . V.null . V.filter (0 /= )) $ V.imap (\rowIdx rows -> V.imap (\colIdx entry ->
              -- | This is so stupid but works performance wise. I was too lazy to use ST Monad and
              -- make this grid mutable.
              let changedField = V.imap (\innerRowIdx row -> V.imap (\innerColIdx entry ->
                                        if innerRowIdx == rowIdx && innerColIdx == colIdx
                                            then toggleFieldEntry entry
                                            else entry) row) (view content field)
                  (newXSym, newYSym) = findSymmetries (set content changedField field)
                  diffX = newXSym `S.difference` xSymInitial
                  diffY = newYSym `S.difference` ySymInitial
                    in if S.null diffX && S.null diffY
                           then 0
                           else computeSymmetryValues (diffX, diffY)

              ) rows) $ view content field

runPart2 :: String -> IO ()
runPart2 input = do
    let parsed = runParser parseInput () "" input
    case parsed of
        Left e -> print e
        Right fields -> do
            putStr "Part 2: "
            print . sum . map findDifferentSymmetry $ fields

-- | Running
run13 :: String -> IO ()
run13 input = do
    computationTime $ runPart1 input
    computationTime $ runPart2 input
