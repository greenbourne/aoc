module Y2023.Aoc_2023_11
    (run11
    )
where

import Control.Monad (when)
import Control.Monad.State
import Control.Monad.Writer
import qualified Data.Set as S
import qualified Data.Vector as V
import Text.Parsec hiding (Empty, State)

import Utility.BaseUtil
import Utility.VectorUtil

-- | Data structures
data Universe = Space | Galaxy deriving (Show, Eq)

-- | Parsing
parseInput :: ParsecT String u (State (S.Set Int)) (Grid Universe)
parseInput = do
    rows <- parseLine `endBy` newline
    if null rows
        then return $ V.fromList []
        else do
            let rowLength = V.length . head $ rows
            galaxyColumns <- get
            put $ S.fromList [0..rowLength-1] `S.difference` galaxyColumns
            return $ V.fromList rows

parseLine :: ParsecT String u (State (S.Set Int)) (V.Vector Universe)
parseLine = do
        universeRow <- map charToUniverse <$> many1 (noneOf "\n")
        galaxyColumns <- get
        let galaxyColumnsCurrent = S.fromList . map fst . filter ((==) Galaxy . snd) . zip [0..] $ universeRow
        put $ galaxyColumns `S.union` galaxyColumnsCurrent
        return $ V.fromList universeRow

charToUniverse :: Char -> Universe
charToUniverse c
    | c == '.' = Space
    | c == '#' = Galaxy
    | otherwise = error $ "Unexpected symbol: " ++ show c

universeToChar :: Universe -> Char
universeToChar u
    | u == Space = '.'
    | otherwise = '#'

prettyPrintUniverseIO :: Grid Universe -> IO ()
prettyPrintUniverseIO = mapM_ putStrLn . prettyPrintUniverse

prettyPrintUniverse :: Grid Universe -> [String]
prettyPrintUniverse = V.toList . V.map (V.toList . V.map universeToChar)

-- | Utility
isEmptySpaceRow :: V.Vector Universe -> Bool
isEmptySpaceRow = V.all (Space ==)

getEmptyRows :: Grid Universe -> S.Set Int
getEmptyRows = S.fromList . map fst . filter (isEmptySpaceRow . snd) . zip [0..] . V.toList

-- | Part 1
expandUniverse :: S.Set Int -> S.Set Int -> Grid Universe -> Grid Universe
expandUniverse emptyCols emptyRows g =
        let expandedCols = V.map (V.fromList . insertElemAt Space emptyCols . V.toList) g
            rowLength = V.length . head $ V.toList expandedCols
            in V.fromList $ insertElemAt (V.fromList . replicate rowLength $ Space) emptyRows (V.toList expandedCols)

getGalaxyPositions :: Grid Universe -> S.Set Position
getGalaxyPositions g = S.fromList . snd . runWriter $ V.imapM_ (\rowIdx row -> V.imapM_ (\colIdx element ->
                           when (element == Galaxy) $
                                   tell [(rowIdx, colIdx)]
                                   ) row) g

sumPathLength :: S.Set Position -> Int
sumPathLength pos =
        let posList = S.toList pos
            doubled = sum [abs (fst p1 - fst p2) + abs (snd p1 - snd p2) | p1 <- posList, p2 <- posList, p1 /= p2]
            in doubled `div` 2

runPart1 :: String -> IO ()
runPart1 input = do
    let (parsed, emptyColumns) = runState (runParserT parseInput () "" input) S.empty
    case parsed of
        Left e -> print e
        Right universe -> do
            let emptyRows = getEmptyRows universe
            let expandedUniverse = expandUniverse emptyColumns emptyRows universe
            let galaxyPos = getGalaxyPositions expandedUniverse
            putStr "Part 1: "
            print (sumPathLength galaxyPos)

-- Part 2
spaceExpansion :: Int
spaceExpansion = 1000000

getExpandedPositions :: S.Set Int -> S.Set Int -> Grid Universe -> S.Set Position
getExpandedPositions emptyCols emptyRows g = S.fromList . snd . runWriter $ V.imapM_ (\rowIdx row -> V.imapM_ (\colIdx element ->
                           when (element == Galaxy) $ do
                                   let expandedRow = S.size . S.filter (rowIdx >) $ emptyRows
                                   let expandedCol = S.size . S.filter (colIdx >) $ emptyCols
                                   tell [(rowIdx + (expandedRow * max 1 (spaceExpansion-1)), colIdx + (expandedCol * max 1 (spaceExpansion-1)))]
                                   ) row) g

runPart2 :: String -> IO ()
runPart2 input = do
    let (parsed, emptyColumns) = runState (runParserT parseInput () "" input) S.empty
    case parsed of
        Left e -> print e
        Right universe -> do
            let emptyRows = getEmptyRows universe
            let galaxyPos = getExpandedPositions emptyColumns emptyRows universe
            putStr "Part 2: "
            print (sumPathLength galaxyPos)

-- Running
run11 :: String -> IO ()
run11 input = do
    computationTime $ runPart1 input
    computationTime $ runPart2 input
