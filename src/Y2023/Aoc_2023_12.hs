{-# LANGUAGE TemplateHaskell #-}
module Y2023.Aoc_2023_12
( run12,
)
where

import Control.Monad.State
import Control.Lens (over,set,view)
import Control.Lens.TH (makeLenses)
import Data.List (intercalate)
import qualified Data.Map as M
import Text.Parsec hiding (Empty, State)

import Utility.BaseUtil (computationTime)
import Utility.ParseUtil (parsePositiveNumber)

-- | Data structure
data Spring = Working | Damaged | Unknown deriving (Show, Eq, Ord)

data SpringRow = SpringRow {
    _springs :: ![Spring],
    _description :: ![Int]
    } deriving (Show, Eq, Ord)
makeLenses ''SpringRow

-- | Parsing
parseInput :: Parsec String u [SpringRow]
parseInput = parseLine `endBy` newline

parseLine :: Parsec String u SpringRow
parseLine = do
    springs <- map charToSpring <$> many1 (noneOf " ")
    space
    description <- parsePositiveNumber `sepBy` char ','
    return SpringRow {
        _springs = springs,
        _description = description
        }

charToSpring :: Char -> Spring
charToSpring c
    | c == '.' = Working
    | c == '#' = Damaged
    | c == '?' = Unknown
    | otherwise = error $ "Unexpected character " ++ show c

-- Part 1
validPaths :: [Spring] -> [Int] -> Int
validPaths [] [] = 1
validPaths [] _  = 0
validPaths springs [] =
    if Damaged `elem` springs
        then 0
        else 1
validPaths springs@(s:ss) damaged@(d:ds)
    | d > length springs = 0
    | s == Working = validPaths ss damaged
    | s == Unknown =
        let workingCase = validPaths ss damaged
            canNextNBeDamaged = (notElem Working . take d) springs
            isNPlus1NotDamaged = (\rest -> null rest || head rest /= Damaged) $ drop d springs
            in if canNextNBeDamaged && isNPlus1NotDamaged
                then workingCase + validPaths (drop (d+1) springs) ds
                else workingCase
    | s == Damaged =
        let canNextNBeDamaged = (notElem Working . take d) springs
            isNPlus1NotDamaged = (\rest -> null rest || head rest /= Damaged) $ drop d springs
            in if canNextNBeDamaged && isNPlus1NotDamaged
                    then validPaths (drop (d+1) springs) ds
                    else 0

sumValidPaths :: [SpringRow] -> Int
sumValidPaths =
        sum . map (\sr -> validPaths (view springs sr) (view description sr))

runPart1 :: String -> IO ()
runPart1 input = do
    let parsed = runParser parseInput () "" input
    case parsed of
        Left e -> print e
        Right rows -> do
            putStr "Part 1: "
            print $ sumValidPaths rows

-- Part 2
sumValidPathsDP :: [SpringRow] -> Int
sumValidPathsDP =
        sum . map (\sr -> evalState (validPathsDP sr) M.empty)

validPathsDP :: SpringRow -> State (M.Map SpringRow Int) Int
validPathsDP (SpringRow [] []) = return 1
validPathsDP (SpringRow [] _)  = return 0
validPathsDP (SpringRow sprgs []) =
    if Damaged `elem` sprgs
        then return 0
        else return 1
validPathsDP sr@(SpringRow sprgs@(s:ss) damaged@(d:ds))
    | d > length sprgs = return 0
    | s == Working = do
        m <- get
        if sr `M.member` m
            then return $ m M.! sr
            else do
                result <- validPathsDP . set description damaged . set springs ss $ sr
                updateDPMap sr result
    | s == Unknown = do
        m <- get
        if sr `M.member` m
            then return $ m M.! sr
            else do
                workingCase <- validPathsDP . set springs ss . set description damaged $ sr
                let canNextNBeDamaged = (notElem Working . take d) sprgs
                let isNPlus1NotDamaged = (\rest -> null rest || head rest /= Damaged) $ drop d sprgs
                if canNextNBeDamaged && isNPlus1NotDamaged
                    then do
                        damagedCase <- validPathsDP . set description ds . over springs (drop (d+1)) $ sr
                        updateDPMap sr (workingCase + damagedCase)
                    else
                        updateDPMap sr workingCase
    | s == Damaged = do
        m <- get
        if sr `M.member` m
            then return $ m M.! sr
            else do
                let canNextNBeDamaged = (notElem Working . take d) sprgs
                let isNPlus1NotDamaged = (\rest -> null rest || head rest /= Damaged) $ drop d sprgs
                    in if canNextNBeDamaged && isNPlus1NotDamaged
                            then do
                                result <- validPathsDP . set description ds . over springs (drop (d+1)) $ sr
                                updateDPMap sr result
                            else updateDPMap sr 0

updateDPMap :: SpringRow -> Int -> State (M.Map SpringRow Int) Int
updateDPMap sr n = do
    m2 <- get
    put $ M.insert sr n m2
    return n

unfoldSprings :: [SpringRow] -> [SpringRow]
unfoldSprings = map (\sr ->
                        let des = view description sr
                            s = view springs sr
                            in SpringRow {
                                _description = concat $ replicate 5 des,
                                _springs = intercalate [Unknown] $ replicate 5 s
                                }
                       )

runPart2 :: String -> IO ()
runPart2 input = do
    let parsed = runParser parseInput () "" input
    case parsed of
        Left e -> print e
        Right rows -> do
            putStr "Part 2: "
            print . sumValidPathsDP . unfoldSprings $ rows

-- | Running
run12 :: String -> IO ()
run12 input = do
    computationTime $ runPart1 input
    computationTime $ runPart2 input
