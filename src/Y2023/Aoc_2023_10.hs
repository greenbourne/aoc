module Y2023.Aoc_2023_10
    (run10
    )
where

import qualified Control.Monad.State as ST
import Data.Bifunctor (bimap)
import Data.Maybe (fromJust, isJust)
import qualified Data.Set as S
import qualified Data.Vector as V
import Text.Parsec hiding (Empty, State)

import Utility.BaseUtil
import Utility.VectorUtil

-- | Data structure
data Pipe = Start | WestEast | SouthNorth | NorthEast | NorthWest | SouthEast | SouthWest | Ground deriving (Eq, Ord, Show)
type Edge = (Position, Position)

-- | Parsing
parseInput :: Parsec String u (Grid Pipe, Position)
parseInput = do
        ls <- parseLine `endBy` newline
        let startPos = head . map (\(x, (_,y)) -> (x,fromJust y)) . filter (isJust . snd . snd) $ zip [0..] ls
        return (V.fromList $ map fst ls, startPos)

parseLine :: Parsec String u (V.Vector Pipe, Maybe Int)
parseLine = do
        pipes <- map charToPipe <$> many1 (noneOf "\n")
        if Start `S.member` S.fromList pipes
            then do
                let y = fst . head . filter ((==) Start . snd) $ zip [0..] pipes
                return (V.fromList pipes, Just y)
            else return (V.fromList pipes, Nothing)

charToPipe :: Char -> Pipe
charToPipe c
    | c == '|' = SouthNorth
    | c == '-' = WestEast
    | c == 'L' = NorthEast
    | c == 'J' = NorthWest
    | c == '7' = SouthWest
    | c == 'F' = SouthEast
    | c == '.' = Ground
    | c == 'S' = Start
    | otherwise = error $ "Element unknown: " ++ show c

-- | Part 1
findLoop :: Position -> Grid Pipe -> ST.State (S.Set Position) [Position]
findLoop current g = do
        visited <- ST.get
        ST.when (null visited) (ST.put (S.insert current visited))
        visited <- ST.get
        let connected = S.filter (\c -> not (c `S.member` visited)) $ getConnectedNeighbours g current
        if null connected
            then return [current]
            else do
                let next = head $ S.toList connected
                ST.put (S.insert next visited)
                (:) current <$> findLoop next g

getConnectedNeighbours :: Grid Pipe -> Position -> S.Set Position
getConnectedNeighbours g current
    | (g !!! current) == Start =
        let neighbours = filter (isJust . (!!?) g) $ map (bimap (fst current +) (snd current +)) [(-1,0),(1,0),(0,1),(0,-1)]
            in S.fromList [n | n <- neighbours, current `S.member` getConnectedNeighbours g n]
    | otherwise = let newPositions = [(fst current + fst delta, snd current + snd delta) | delta <- validDelta (g !!! current)]
                      in S.fromList newPositions

validDelta :: Pipe -> [Position]
validDelta c
    | c == SouthNorth = [(-1,0),(1,0)]
    | c == WestEast = [(0,1),(0,-1)]
    | c == NorthEast = [(-1,0),(0,1)]
    | c == NorthWest = [(-1,0),(0,-1)]
    | c == SouthWest = [(1,0),(0,-1)]
    | c == SouthEast = [(1,0),(0,1)]
    | c == Ground = []
    | otherwise = error $ "Unexpected Element: " ++ show c

-- | Part 2
getNumberOfInsidePoints :: Grid Pipe -> S.Set Position -> [Edge] -> Int
getNumberOfInsidePoints g loopElems edges =
        let insideGrid = V.imap (\rowIdx rows -> V.imap (\colIdx _ -> isPointInside loopElems (rowIdx,colIdx) edges) rows) g
            in V.foldr (\e acc -> (+) acc . length . filter id . V.toList $ e) 0 insideGrid

isPointInside :: S.Set Position -> Position -> [Edge] -> Bool
isPointInside loopElems current edges = 
        if current `S.member` loopElems
            then False
            else odd . length . filter id . map (pointCrossesEdge current) $ edges

-- Raycast algorithm
pointCrossesEdge :: Position -> Edge -> Bool
pointCrossesEdge (xp,yp) ((x1,y1),(x2,y2)) =
        if (yp < y1) /= (yp < y2) &&
            fromIntegral xp < (fromIntegral x1 + ((fromIntegral yp - fromIntegral y1)/(fromIntegral y2- fromIntegral y1))*(fromIntegral x2 - fromIntegral x1))
            then True
            else False

getEdges :: [Position] -> [Edge]
getEdges [] = []
getEdges (p:[]) = []
getEdges (p:ps) = let endEdge = last $ takeWhile (\p' -> fst p' == fst p || snd p' == snd p) ps
                      rest = endEdge : dropWhile (\p' -> fst p' == fst p || snd p' == snd p) ps
                      in (p, endEdge) : getEdges rest

-- | Running
runPart1 :: String -> IO ()
runPart1 input = do
    let parsed = runParser parseInput () "" input
    case parsed of
        Left e -> print e
        Right (g,s) -> do
            let (_,visited) = ST.runState (findLoop s g) S.empty
            putStr "Part 1: "
            print (S.size visited `div` 2)

runPart2 :: String -> IO ()
runPart2 input = do
    let parsed = runParser parseInput () "" input
    case parsed of
        Left e -> print e
        Right (g,s) -> do
            let (loop,visited) = ST.runState (findLoop s g) S.empty
            putStr "Part 2: "
            print $ getNumberOfInsidePoints g visited (getEdges loop)

run10 :: String -> IO ()
run10 input = do
    computationTime $ runPart1 input
    computationTime $ runPart2 input
