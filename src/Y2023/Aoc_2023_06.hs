{-# LANGUAGE TemplateHaskell #-}
module Y2023.Aoc_2023_06
    (run06
    )
where

import Control.Lens (view)
import Control.Lens.TH (makeLenses)
import Text.Parsec hiding (Empty, State)

import Utility.BaseUtil
import Utility.ParseUtil (parseNumberRow)

-- Data structures
data Race = Race {
    _time_max :: !Int,
    _distance  :: !Int
    } deriving (Show)
makeLenses ''Race

-- Parsing
parseInputPart1 :: Parsec String u [Race]
parseInputPart1 = do
    string "Time:"
    times <- parseNumberRow
    string "Distance:"
    zipWith (\t d -> Race {
        _time_max = t,
        _distance = d
        }) times <$> parseNumberRow

parseInput :: Parsec String u Race
parseInput = do
    string "Time:"
    time <- filter (' ' /=) <$> many1 (noneOf "\n")
    newline
    string "Distance:"
    distance <- filter (' ' /=) <$> many1 (noneOf "\n")
    return $ Race {
        _time_max = read time :: Int,
        _distance = read distance :: Int
        }

-- Part 1
getAmountOfValidStarts :: Race -> Int
getAmountOfValidStarts r = length [n |n <- [1..(view time_max r)], (view time_max r-n) * n > view distance r]

-- Part 2
findLowestTime :: Race -> Int -> Int -> Int
findLowestTime race lowerBound upperBound
        | lowerBound == upperBound = lowerBound
        | lowerBound + 1 == upperBound = lowerBound -- This might be wrong
        | otherwise =
            let numberToCheck = (upperBound + lowerBound) `div` 2
                isValid = numberToCheck * (view time_max race - numberToCheck) > view distance race
                in if isValid
                       then findLowestTime race lowerBound numberToCheck
                       else findLowestTime race numberToCheck upperBound

beatRaceAmount :: Race -> Int
beatRaceAmount r =
    let startingNumber = view time_max r `div` 2
        lowestNumber = findLowestTime r 0 startingNumber
        in if even (view time_max r)
               then (startingNumber - lowestNumber) * 2 - 1
               else (startingNumber - lowestNumber) * 2

runPart1 :: String -> IO ()
runPart1 input = do
    let parsed = runParser parseInputPart1 () "" input
    case parsed of
        Left e -> print e
        Right races -> do
            putStr "Part 1: "
            print $ product . map beatRaceAmount $ races

runPart2 :: String -> IO ()
runPart2 input = do
    let parsed = runParser parseInput () "" input
    case parsed of
        Left e -> print e
        Right race -> do
            putStr "Part 2: "
            print $ beatRaceAmount race

run06 :: String -> IO ()
run06 input = do
    computationTime $ runPart1 input
    computationTime $ runPart2 input
