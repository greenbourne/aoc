{-# LANGUAGE TemplateHaskell #-}
module Y2023.Aoc_2023_04
( run04,
)
where

import Control.Lens (over, set, view)
import Control.Lens.TH (makeLenses)
import Data.Functor.Identity
import qualified Data.Set as S
import qualified Data.IntMap as I
import Text.Parsec hiding (Empty, State)

import Utility.BaseUtil
import Utility.ParseUtil (parsePositiveNumber)

-- Data structures
data CardPlayer = CardPlayer {
    _cardId :: !Int,
    _cardNumbers :: !(S.Set Int),
    _playerNumbers :: !(S.Set Int) } deriving (Show)
makeLenses ''CardPlayer

-- Parsing
parseInput :: ParsecT String u Identity [CardPlayer]
parseInput = parseLine `endBy` newline

parseLine :: ParsecT String u Identity CardPlayer
parseLine = do
    string "Card"
    many1 space
    cardId <- parsePositiveNumber
    char ':'
    cardNumbers <- map (read :: String -> Int ) . words <$> many1 (noneOf "|")
    char '|'
    playerNumbers <- map (read :: String -> Int ) . words <$> many1 (noneOf "\n")
    return CardPlayer {
        _cardId = cardId,
        _cardNumbers = S.fromList cardNumbers,
        _playerNumbers = S.fromList playerNumbers }

-- Part 1
getCardScore1 :: CardPlayer -> Int
getCardScore1 cp =
        let inter = view cardNumbers cp `S.intersection` view playerNumbers cp
            in floor $ 2^^(S.size inter - 1)

computeScore1 :: [CardPlayer] -> Int
computeScore1 = sum . map getCardScore1

-- Part 2
getCardScore2 :: CardPlayer -> Int
getCardScore2 cp =
        let inter = view cardNumbers cp `S.intersection` view playerNumbers cp
            in S.size inter

computeScore2 :: [CardPlayer] -> Int
computeScore2 cps = sum . I.elems $ foldr (\cp acc ->
                            let score = getCardScore2 cp
                                id = view cardId cp
                                currentCopies = I.findWithDefault 1 id acc
                                withOriginal = I.insertWith (flip const) id 1 acc
                                newCopies = tail [id..(id+score)]
                                in foldr (\copy acc' -> I.insertWith (\_ oldVal -> currentCopies+oldVal ) copy (currentCopies+1) acc') withOriginal newCopies
                            ) (I.fromList [(1,1)]) cps

-- Run
runPart1 :: [CardPlayer] -> IO ()
runPart1 cardNumbers = do
    putStr "Part 1: "
    print . computeScore1 $ cardNumbers

runPart2 :: [CardPlayer] -> IO ()
runPart2 cardNumbers = do
    putStr "Part 2: "
    print . computeScore1 $ reverse cardNumbers

run04 :: String -> IO ()
run04 input = do
    let parsed = runParser parseInput () "" input
    case parsed of
        Left e -> print e
        Right cardNumbers -> do
            computationTime $ runPart1 cardNumbers
            computationTime $ runPart2 cardNumbers
