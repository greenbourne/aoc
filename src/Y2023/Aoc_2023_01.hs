module Y2023.Aoc_2023_01
    (run01,
    )
where

import Data.List (isPrefixOf)
import qualified Data.List.NonEmpty as NE
import Data.Maybe (catMaybes, mapMaybe)
import Text.Read (readMaybe)

import Utility.BaseUtil (computationTime)

patterns :: [(String, Char)]
patterns = [("0", '0'),
            ("1", '1'),
            ("2", '2'),
            ("3", '3'),
            ("4", '4'),
            ("5", '5'),
            ("6", '6'),
            ("7", '7'),
            ("8", '8'),
            ("9", '9'),
            ("zero",  '0'),
            ("one",   '1'),
            ("two",   '2'),
            ("three", '3'),
            ("four",  '4'),
            ("five",  '5'),
            ("six",   '6'),
            ("seven", '7'),
            ("eight", '8'),
            ("nine",  '9')]

getNumberOfLine :: String -> Maybe Int
getNumberOfLine s =
        let nums = NE.nonEmpty $ findAllNumbers s
            in case nums of
               Nothing -> error $ "No numbers found in line: " ++ s
               Just x -> readMaybe [NE.head x, NE.last x]

findAllNumbers :: String -> String
findAllNumbers = catMaybes . findAllNumbers'

findAllNumbers' :: String -> [Maybe Char]
findAllNumbers' [] = []
findAllNumbers' s@(x:xs) = getNumberPrefix s : findAllNumbers' xs

getNumberPrefix :: String -> Maybe Char
getNumberPrefix s = case [r | (p, r) <- patterns, p `isPrefixOf` s] of
                        [] -> Nothing
                        (x:_) -> Just x

runPart2 :: String -> IO ()
runPart2 input = do
    putStr "Part 2: "
    let s  = sum . mapMaybe getNumberOfLine . filter (not . null) $ lines input
    print s

run01 :: String -> IO ()
run01 input =
    computationTime $ runPart2 input

