module Y2023.Aoc_2023_16
( run16
)
where

import Control.Monad.State
import Data.Array
import qualified Data.Set as S
import qualified Data.Text as T
import Text.Parsec hiding (Empty, State)
import Utility.BaseUtil (computationTime)

-- | Data structures
type Position = (Int,Int)
data Tile = Empty | Pipe | Minus | Slash | BackSlash deriving (Show, Eq)
data Direction = North | West | East | South deriving (Show, Eq, Ord)

-- | Parsing
charToTile :: Char -> Tile
charToTile c
    | c == '.' = Empty
    | c == '-' = Minus
    | c == '|' = Pipe
    | c == '/' = Slash
    | c == '\\' = BackSlash
    | otherwise = error $ "Unexpected character " ++ show c

parseInput :: Parsec T.Text u (Array Position Tile)
parseInput = do
    rows <- parseLine `endBy` newline
    let rowsEndIdx = length rows - 1
    let columnsEndIdx = (length . head $ rows) - 1
    return $ listArray ((0,0),(rowsEndIdx,columnsEndIdx)) (concat rows)

parseLine :: Parsec T.Text u [Tile]
parseLine = map charToTile <$> many1 (noneOf "\n")

-- | Implementation
beamContinuation :: Direction -> Tile -> [Direction]
beamContinuation d t
    | t == Empty = [d]
    | t == Pipe && (d == North || d == South) = [d]
    | t == Pipe && (d == West || d == East) = [South,North]
    | t == Minus && (d == North || d == South) = [West,East]
    | t == Minus && (d == West || d == East) = [d]
    | t == Slash && d == North = [East]
    | t == Slash && d == South = [West]
    | t == Slash && d == East = [North]
    | t == Slash && d == West = [South]
    | t == BackSlash && d == North = [West]
    | t == BackSlash && d == South = [East]
    | t == BackSlash && d == East = [South]
    | t == BackSlash && d == West = [North]
    | otherwise = error $ "Unexpected combination of Direction " ++ show d ++ " and Tile " ++ show t

addDirectionToPos :: Position -> Direction -> Position
addDirectionToPos pos dir
    | dir == North = (fst pos - 1, snd pos)
    | dir == South = (fst pos + 1, snd pos)
    | dir == West = (fst pos, snd pos - 1)
    | dir == East = (fst pos, snd pos + 1)

beamStep :: Array Position Tile -> Direction -> Position -> State (S.Set (Position,Direction)) ()
beamStep arr dir pos =
    if not $ inRange (bounds arr) pos
        then return ()
        else do
            visited <- get
            if (pos,dir) `S.member` visited
                then return ()
                else do
                    put (S.insert (pos,dir) visited)
                    let nextPos = map (\d -> (,) d $ addDirectionToPos pos d) $ beamContinuation dir (arr ! pos)
                    mapM_ (uncurry (beamStep arr)) nextPos

findEnergizedTiles :: Array Position Tile -> Direction -> Position -> Int
findEnergizedTiles arr dir start = S.size . S.map fst $ execState (beamStep arr dir start) S.empty

-- | Part 1
runPart1 :: T.Text -> IO ()
runPart1 input = do
    let parsed = runParser parseInput () "" input
    case parsed of
        Left e -> print e
        Right arr -> do
            putStr "Part 1: "
            let visited = findEnergizedTiles arr East (0,0)
            print visited

-- | Part 2
getAllStartingConfigurations :: Array Position Tile -> [(Direction, Position)]
getAllStartingConfigurations arr =
        let inds = indices arr
            (rowBnd, colBnd) = snd $ bounds arr
            framePoints = [(pRow, pCol) | (pRow,pCol) <- inds, pRow == 0 || pRow == rowBnd || pCol == 0 || pCol == colBnd]
            frameConfigs = map (addDir rowBnd colBnd) framePoints
            cornerConfigs = [(East, (0,0)), (West, (0,colBnd)), (East, (rowBnd,0)), (West, (rowBnd, colBnd))]
            in frameConfigs ++ cornerConfigs
            where addDir :: Int -> Int -> Position -> (Direction, Position)
                  addDir rowBnd colBnd p
                    | fst p == 0 = (South, p)
                    | snd p == 0 = (East, p)
                    | fst p == rowBnd = (North, p)
                    | snd p == colBnd = (West, p)

runPart2 :: T.Text -> IO ()
runPart2 input = do
    let parsed = runParser parseInput () "" input
    case parsed of
        Left e -> print e
        Right arr -> do
            putStr "Part 2: "
            let startConfigs = getAllStartingConfigurations arr
            let maxEnergy = maximum $ map (uncurry (findEnergizedTiles arr)) startConfigs
            print maxEnergy

-- | Running
run16 :: String -> IO ()
run16 input = do
    let txt = T.pack input
    computationTime $ runPart1 txt
    computationTime $ runPart2 txt
