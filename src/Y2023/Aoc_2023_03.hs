module Y2023.Aoc_2023_03
    (run03,
    )
where

import Data.Char (isDigit)
import Data.Functor.Identity
import Data.Maybe (catMaybes)
import qualified Data.Map as M
import qualified Data.Vector as V
import Text.Parsec hiding (Empty, State)

import Utility.BaseUtil
import Utility.VectorUtil (Grid, Position, (!!?))

-- Parsing
parseInput :: ParsecT String u Identity (Grid Char)
parseInput = do
    lines <- parseLine `endBy` newline
    return $ V.fromList lines

parseLine :: ParsecT String u Identity (V.Vector Char)
parseLine = V.fromList <$> many1 (noneOf "\n")

-- Part 1
sumParts :: [(Int,String)] -> Int
sumParts = sum . map fst . filter ((<) 0 . length . snd)

findPartsByNumber :: Grid Char -> [(Int, String)]
findPartsByNumber g = concatMap V.toList $ V.imap (\rIdx row -> V.catMaybes . V.imap (\cIdx elem -> getPartsByNumber g (rIdx,cIdx) elem) $ row) g

getPartsByNumber :: Grid Char -> Position -> Char -> Maybe (Int, String)
getPartsByNumber g p c = case g !!? p of
    Nothing -> Nothing
    Just c -> if isJustNumber(g !!? p) && not (isJustNumber (g !!? (fst p, snd p - 1)))
                  then let (stringNum, parts) = collectPartsForNumber g p -- valid case
                           in Just (read stringNum :: Int, parts)
                  else Nothing -- part of a number, so skip

collectPartsForNumber :: Grid Char -> Position -> (String, String)
collectPartsForNumber g p =
                let collectedBack = filter isPart . catMaybes $ [g !!? (fst p + fst deltaPos, snd p + snd deltaPos) | deltaPos <- [(0,-1), (-1,-1), (1,-1)]]
                    in case g !!? p of
                        Nothing -> ("", collectedBack)
                        Just c -> if not (isDigit c)
                                      then
                                          let collectedEnd = filter isPart . catMaybes $ [g !!? (fst p + fst deltaPos, snd p + snd deltaPos) | deltaPos <- [(0,0), (-1,0), (1,0)]]
                                              in ("", collectedBack ++ collectedEnd)
                                      else let (partNumber, parts) = collectPartsForNumber g (fst p, snd p + 1)
                                              in (c:partNumber, parts ++ collectedBack)

-- Part 2
multGears :: M.Map Position [Int] -> Int
multGears = foldr (\e acc -> acc + product e) 0 . M.elems . M.filter ((==) 2 . length)

findGearsByNumber :: Grid Char -> [(Int, [Position])]
findGearsByNumber g = concatMap V.toList $ V.imap (\rIdx row -> V.catMaybes . V.imap (\cIdx elem -> getGearsByNumber g (rIdx,cIdx) elem) $ row) g

getGearsByNumber :: Grid Char -> Position -> Char -> Maybe (Int, [Position])
getGearsByNumber g p c = case g !!? p of
    Nothing -> Nothing
    Just c -> if isJustNumber(g !!? p) && not (isJustNumber (g !!? (fst p, snd p - 1)))
                  then let (stringNum, parts) = collectGearsForNumber g p -- valid case
                           in Just (read stringNum :: Int, parts)
                  else Nothing -- part of a number, so skip

collectGearsForNumber :: Grid Char -> Position -> (String, [Position])
collectGearsForNumber g p =
                let collectedBack = [ (fst p + fst deltaPos, snd p + snd deltaPos)| deltaPos <- [(0,-1), (-1,-1), (1,-1)], isMult g (fst p + fst deltaPos, snd p + snd deltaPos)]
                    in case g !!? p of
                        Nothing -> ("", collectedBack)
                        Just c -> if not (isDigit c)
                                      then
                                          let collectedEnd = [ (fst p + fst deltaPos, snd p + snd deltaPos) | deltaPos <- [(0,0), (-1,0), (1,0)], isMult g (fst p + fst deltaPos, snd p + snd deltaPos)]
                                              in ("", collectedBack ++ collectedEnd)
                                      else let (partNumber, parts) = collectGearsForNumber g (fst p, snd p + 1)
                                              in (c:partNumber, parts ++ collectedBack)

createGearMap :: [(Int, [Position])] -> M.Map Position [Int]
createGearMap = foldr (\(number,positions) acc -> foldr (\position m -> M.insertWith (++) position [number] m) acc positions) M.empty

-- Utils
isJustNumber :: Maybe Char -> Bool
isJustNumber mc = case mc of
    Just c -> isDigit c
    Nothing -> False -- First column in row is a number

isPart :: Char -> Bool
isPart c
    | c == '.'  = False
    | isDigit c = False
    | otherwise = True

isMult :: Grid Char -> Position -> Bool
isMult g p = case g !!? p of
    Nothing -> False
    Just c -> c == '*'

-- Run
runPart1 :: Grid Char -> IO ()
runPart1 g = do
    putStr "Part 1: "
    print . sumParts . findPartsByNumber $ g

runPart2 :: Grid Char -> IO ()
runPart2 g = do
    putStr "Part 1: "
    print . multGears . createGearMap . findGearsByNumber $ g

run03 :: String -> IO ()
run03 input = do
    let parsed = runParser parseInput () "" input
    case parsed of
        Left e -> print e
        Right g -> do
            computationTime $ runPart1 g
            computationTime $ runPart2 g
