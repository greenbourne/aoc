module Y2023.Aoc_2023_09
    (run09
    )
where

import Text.Parsec hiding (Empty, State)

import Utility.BaseUtil
import Utility.ParseUtil

-- | Parsing
parseInput :: Parsec String u [[Int]]
parseInput = many1 parseNumberRow

-- | Part 1 & 2
triSeqPast :: [Int] -> [Int]
triSeqPast xs
    | (not . any (/= 0)) xs = replicate (1 + length xs) 0
    | otherwise =
        let d = diff xs
            rec = triSeqPast d
            in (head xs - head rec) :xs

triSeqFuture :: [Int] -> [Int]
triSeqFuture xs
    | (not . any (/= 0)) xs = replicate (1 + length xs) 0
    | otherwise =
        let d = diff xs
            rec = triSeqFuture d
            in xs ++ [last xs + last rec]

diff :: [Int] -> [Int]
diff (x:y:zz) = y-x : diff (y:zz)
diff _        = []

-- | Running
runPart1 :: [[Int]] -> IO ()
runPart1 ls= do
    putStr "Part 1: "
    print . sum . map (last . triSeqFuture) $ ls

runPart2 :: [[Int]] -> IO ()
runPart2 ls = do
    putStr "Part 2: "
    print . sum . map (head . triSeqPast) $ ls

run09 :: String -> IO ()
run09 input = do
    let parsed = runParser parseInput () "" input
    case parsed of
        Left e -> print e
        Right ls -> do
            computationTime $ runPart1 ls
            computationTime $ runPart2 ls
