{-# LANGUAGE TemplateHaskell #-}
module Y2023.Aoc_2023_07
    (run07
    )
where

import Control.Lens (view)
import Control.Lens.TH (makeLenses)
import Data.Maybe (mapMaybe)
import Data.List (group, sort, sortBy)
import Text.Parsec hiding (Empty, State)

import Utility.BaseUtil

-- Data structures
data Kind = High | Pair | TwoPair | Drilling | FullHouse | Quadruple | Quintuplet deriving (Show, Eq, Ord)

-- Part 1
-- data Card = Two | Three | Four | Five | Six | Seven | Eight | Nine | Ten | Joker | Queen | King | Ace deriving (Show, Eq, Ord)
-- Part 2
data Card = Joker | Two | Three | Four | Five | Six | Seven | Eight | Nine | Ten | Queen | King | Ace deriving (Show, Eq, Ord)

data Hand = Hand {
    _kind  :: Kind,
    _cards :: [Card],
    _bid   :: Int
    } deriving (Show, Eq)
makeLenses ''Hand

instance Ord Hand where
    compare hand1 hand2
        | view kind hand1 /= view kind hand2 = compare (view kind hand1) (view kind hand2)
        | otherwise = let diff =  filter (EQ /=) $ zipWith compare (view cards hand1) (view cards hand2)
                          in if null diff
                                 then EQ
                                 else head diff

-- Parsing & Part 1 / 2
parseInput :: (String -> Int -> Hand) -> Parsec String u [Hand]
parseInput f = parseLine f `endBy` newline

parseLine :: (String -> Int -> Hand) -> Parsec String u Hand
parseLine f = do
    hand <- many1 alphaNum
    space
    bid <- (read :: String -> Int) <$> many1 alphaNum
    return $ f hand bid

stringToHandWJ :: String -> Int -> Hand
stringToHandWJ s bid =  Hand {
                 _kind = mapToKindWithJoker cards,
                 _cards = cards,
                 _bid = bid }
                 where cards= mapMaybe charToCard s

stringToHandWOJ :: String -> Int -> Hand
stringToHandWOJ s bid =  Hand {
                 _kind = mapToKindWithoutJoker cards,
                 _cards = cards,
                 _bid = bid }
                 where cards= mapMaybe charToCard s

mapToKindWithoutJoker :: [Card] -> Kind
mapToKindWithoutJoker cards =
    let gr = group . sort $ cards
        in mapToKindHelper gr

mapToKindWithJoker :: [Card] -> Kind
mapToKindWithJoker cards =
    let gr = sortBy (flip (\gr1 gr2 -> length gr1 `compare` length gr2)) . group . sort . filter (/= Joker) $ cards
        numberOfJokers = length . filter (== Joker) $ cards
        grWithJokers = if numberOfJokers == 5
                           then [cards]
                           else (head gr ++ replicate numberOfJokers (head (head gr))) : tail gr
        in mapToKindHelper grWithJokers

mapToKindHelper :: [[Card]] -> Kind
mapToKindHelper gr =
        case length gr of
               1 -> Quintuplet
               5 -> High
               4 -> Pair
               2 -> if any ((==) 4 . length) gr
                        then Quadruple
                        else FullHouse
               3 -> if any ((==) 3 . length) gr
                        then Drilling
                        else TwoPair
               _ -> error "Invalid input"

charToCard :: Char -> Maybe Card
charToCard c
    | c == 'A' = Just Ace
    | c == 'K' = Just King
    | c == 'Q' = Just Queen
    | c == 'J' = Just Joker
    | c == 'T' = Just Ten
    | c == '9' = Just Nine
    | c == '8' = Just Eight
    | c == '7' = Just Seven
    | c == '6' = Just Six
    | c == '5' = Just Five
    | c == '4' = Just Four
    | c == '3' = Just Three
    | c == '2' = Just Two
    | otherwise = Nothing

computeRankSum :: [Hand] -> Int
computeRankSum = sum . zipWith (\r h -> r * view bid h) [1..] . sort

-- Running
runPart1 :: String -> IO ()
runPart1 input = do
    let parsed = runParser (parseInput stringToHandWOJ) () "" input
    case parsed of
        Left e -> print e
        Right hands -> do
            putStr "Part 1: "
            print $ computeRankSum hands

runPart2 :: String -> IO ()
runPart2 input = do
    let parsed = runParser (parseInput stringToHandWJ) () "" input
    case parsed of
        Left e -> print e
        Right hands -> do
            putStr "Part 2: "
            print $ computeRankSum hands

run07 :: String -> IO ()
run07 input =
    -- Part1 needs different data type, see above. Joker has to be between Ten and Queen
    -- runPart1 input
    computationTime $ runPart2 input
