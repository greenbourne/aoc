{-# LANGUAGE ScopedTypeVariables #-}
module Y2023.Aoc_2023_14
( run14,
) where

import Control.Monad
import Control.Monad.State
import Control.Monad.ST
import Control.Monad.Primitive
import Data.Array
import Data.Array.ST
import Data.Bifunctor (bimap)
import Data.List (groupBy)
import qualified Data.Map as M
import Text.Parsec hiding (Empty, State)

import Utility.BaseUtil (computationTime)

-- | Data structures
type Position = (Int,Int)
data BoardEntity = Empty | Rock | Boulder deriving (Show, Eq, Ord)
data Direction = North | South | East | West deriving (Show, Eq)

-- | Utility
directionDelta :: Direction -> Position
directionDelta d
    | d == North = (-1,0)
    | d == South = (1,0)
    | d == East  = (0,1)
    | d == West  = (0,-1)

prettyPrintIO :: Array Position BoardEntity -> IO ()
prettyPrintIO arr = do
        let rows = groupBy (\a b -> fst (fst a) == fst (fst b)) $ assocs arr
        mapM_ ( putStrLn . map (entityToChar . snd)) rows

charToEntity :: Char -> BoardEntity
charToEntity c
    | c == 'O' = Boulder
    | c == '#' = Rock
    | c == '.' = Empty
    | otherwise = error $ "Unexpected character " ++ show c

entityToChar :: BoardEntity -> Char
entityToChar c
    | c == Boulder = '0'
    | c == Rock = '#'
    | c == Empty = '.'

countRocks :: Array Position BoardEntity -> Int
countRocks arr =
        let maxValue = (+) 1 . fst . snd . bounds $ arr :: Int
            rows = groupBy (\a b -> fst (fst a) == fst (fst b)) $ assocs arr :: [[(Position, BoardEntity)]]
            in sum $ map (\r ->
                   let nBoulders = length $ filter ((==) Boulder . snd) r :: Int
                       valueRow = maxValue - fst (fst (head r)) :: Int
                       in valueRow * nBoulders
                       ) rows

-- | Parsing
parseInput :: Parsec String u (Array Position BoardEntity)
parseInput = do
    rows <- parseLine `endBy` newline
    let rowsEndIdx = length rows - 1
    let columnsEndIdx = (length . head $ rows) - 1
    return $ listArray ((0,0),(rowsEndIdx,columnsEndIdx)) (concat rows)

parseLine :: Parsec String u [BoardEntity]
parseLine = map charToEntity <$> many1 (noneOf "\n")

-- | Part 1
moveBoulderTo :: Direction -> STArray s Position BoardEntity -> Position -> ST s ()
moveBoulderTo dir arr pos = do
    value <- readArray arr pos
    when (value == Boulder) $ do
        -- Find destination for boulder
        let delta = directionDelta dir
        b <- getBounds arr
        let allPossiblePositions = takeWhile (inRange b) . map (\x -> bimap (fst pos +) (snd pos +) . bimap (x *) (x *) $ delta) $ [1..]
        allValid <- takeWhile snd . zip allPossiblePositions <$> mapM ((return . (==) Empty) <=< readArray arr) allPossiblePositions
        unless (null allValid) $ do
            let newBoulderPos = fst $ last allValid
            writeArray arr newBoulderPos Boulder
            writeArray arr pos Empty

moveAllBoulderTo :: Direction -> STArray s Position BoardEntity -> ST s ()
moveAllBoulderTo dir arr = do
        current :: Array Position BoardEntity <- freeze arr
        let inds = indices current
        let indsOrder = if dir == North || dir == West
                               then inds
                               else reverse inds
        mapM_ (moveBoulderTo dir arr) indsOrder

moveToNorth :: Array Position BoardEntity -> Array Position BoardEntity
moveToNorth arr = runSTArray $ do
    mArr <- thaw arr
    moveAllBoulderTo North mArr
    return mArr

runPart1 :: String -> IO ()
runPart1 input = do
    let parsed = runParser parseInput () "" input
    case parsed of
        Left e -> print e
        Right board -> do
            putStr "Part 1: "
            print . countRocks . moveToNorth $ board

-- | Part 2
doCycle :: STArray s Position BoardEntity -> ST s ()
doCycle arr = do
    moveAllBoulderTo North arr
    moveAllBoulderTo West arr
    moveAllBoulderTo South arr
    moveAllBoulderTo East arr

findRepetition  :: forall s. STArray (PrimState (ST s)) Position BoardEntity -> Int -> StateT (M.Map (Array Position BoardEntity) Int) (ST s) (Int, Int)
findRepetition arr current = do
        before :: Array Position BoardEntity <- lift $ freeze arr
        m <- get
        if before `M.member` m
            then return (m M.! before, current)
            else do
                put $ M.insert before current m
                lift $ doCycle arr
                findRepetition arr (current + 1)

runNCycles :: forall s. STArray (PrimState (ST s)) Position BoardEntity -> Int -> StateT (M.Map (Array Position BoardEntity) Int) (ST s) ()
runNCycles arr 0 = return ()
runNCycles arr n = do
        lift $ doCycle arr
        runNCycles arr (n-1)

simulateNCycles :: Int -> Array Position BoardEntity -> Array Position BoardEntity
simulateNCycles amount arr = runST $ do
    mArr :: STArray (PrimState (ST s)) Position BoardEntity <- thaw arr
    (start, end) <- evalStateT (findRepetition mArr 0) M.empty
    newMArr :: STArray (PrimState (ST s)) Position BoardEntity <- thaw arr
    let offset = (amount-start) `mod` (end-start)
    evalStateT (runNCycles newMArr (offset+start)) M.empty
    freeze newMArr

targetCycles :: Int
targetCycles = 1000000000

runPart2 :: String -> IO ()
runPart2 input = do
    let parsed = runParser parseInput () "" input
    case parsed of
        Left e -> print e
        Right board -> do
            putStr "Part 2: "
            print . countRocks . simulateNCycles targetCycles $ board

-- | Run all
run14 :: String -> IO ()
run14 input = do
    computationTime $ runPart1 input
    computationTime $ runPart2 input
