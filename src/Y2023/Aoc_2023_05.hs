{-# LANGUAGE TemplateHaskell #-}
module Y2023.Aoc_2023_05
    (run05
    )
where

import Control.Lens (view)
import Control.Lens.TH (makeLenses)
import Data.Range
import Data.Maybe (mapMaybe)
import Text.Parsec hiding (Empty, State)
import Text.Read (readMaybe)

import Utility.BaseUtil
import Utility.ParseUtil (parsePositiveNumber)

-- Data structures
data ResourceMapping = ResourceMapping {
    _dest_range_start :: !(Range Int),
    _src_range_start  :: !(Range Int),
    _diff :: !Int
    } deriving (Show)
makeLenses ''ResourceMapping

data Almanac = Almanac {
    _seeds             :: ![Int],
    _seedsRange        :: ![Range Int],
    _seed_to_soil      :: ![ResourceMapping],
    _soil_to_fert      :: ![ResourceMapping],
    _fert_to_water     :: ![ResourceMapping],
    _water_to_light    :: ![ResourceMapping],
    _light_to_temp     :: ![ResourceMapping],
    _temp_to_humid     :: ![ResourceMapping],
    _humid_to_location :: ![ResourceMapping]
    } deriving (Show)
makeLenses ''Almanac

-- Parsing
parseInput :: Parsec String u Almanac
parseInput = do
        seeds <- parseSeeds
        newline
        seed_to_soil <- parseResourceMappings
        newline
        soil_to_fert <- parseResourceMappings
        newline
        fert_to_water <- parseResourceMappings
        newline
        water_to_light <- parseResourceMappings
        newline
        light_to_temp <- parseResourceMappings
        newline
        temp_to_humid <- parseResourceMappings
        newline
        humid_to_location <- parseResourceMappings
        return Almanac {
            _seeds             = seeds,
            _seedsRange        = seedsToRange seeds,
            _seed_to_soil      = seed_to_soil,
            _soil_to_fert      = soil_to_fert,
            _fert_to_water     = fert_to_water,
            _water_to_light    = water_to_light,
            _light_to_temp     = light_to_temp,
            _temp_to_humid     = temp_to_humid,
            _humid_to_location = humid_to_location
            }

seedsToRange :: [Int] -> [Range Int]
seedsToRange [] = []
seedsToRange (x:y:zz) = x +=+ (x + y - 1) : seedsToRange zz
seedsToRange _ = error "odd number of seeds are not specified"

parseSeeds :: Parsec String u [Int]
parseSeeds = do
    string "seeds: "
    mapMaybe (readMaybe :: String -> Maybe Int) <$> (spaces *> many1 digit `sepBy` many1 (char ' ')) <* newline

parseResourceMappings :: Parsec String u [ResourceMapping]
parseResourceMappings = do
    many1 (noneOf "\n")
    newline
    concat <$> many1 (parseResourceMapping `endBy1` newline)

parseResourceMapping :: Parsec String u ResourceMapping
parseResourceMapping = do
    a <- parsePositiveNumber
    space
    b <- parsePositiveNumber
    space
    c <- parsePositiveNumber
    return ResourceMapping {
        _dest_range_start = a +=+ (a + c - 1),
        _src_range_start = b +=+ (b + c - 1),
        _diff = a - b
        }

-- Logic for Part 1 & 2
moveRange :: Int -> Range Int -> Range Int
moveRange diff (SingletonRange s) = SingletonRange (s+diff)
moveRange diff (SpanRange a b) = SpanRange (moveBound diff a) (moveBound diff b)
moveRange diff (LowerBoundRange a) = LowerBoundRange (moveBound diff a)
moveRange diff (UpperBoundRange a) = UpperBoundRange (moveBound diff a)
moveRange _ r = r

moveBound :: Int -> Bound Int -> Bound Int
moveBound diff b = Bound (boundValue b + diff) (boundType b)

rangeMapping :: [ResourceMapping] -> Range Int -> [Range Int]
rangeMapping rms range =
        let matchesSrc = concatMap (\rm -> [view src_range_start rm] `intersection` [range]) rms
            matchesDest = concatMap (\rm ->
                                    let match = [view src_range_start rm] `intersection` [range]
                                        in if null match
                                               then []
                                               else map (moveRange (view diff rm)) match
                                        ) rms
            differenceSrc = [range] `difference` matchesSrc
            in matchesDest++differenceSrc

rangeMappings :: [ResourceMapping] -> [Range Int] -> [Range Int]
rangeMappings rms = concatMap (rangeMapping rms)

seedToLocation :: Almanac -> [Range Int] -> [Range Int]
seedToLocation alm =
        rangeMappings (view humid_to_location alm)
            . rangeMappings (view temp_to_humid alm)
            . rangeMappings (view light_to_temp alm)
            . rangeMappings (view water_to_light alm)
            . rangeMappings (view fert_to_water alm)
            . rangeMappings (view soil_to_fert alm)
            . rangeMappings (view seed_to_soil alm)

getMinimum :: [Range Int] -> Int
getMinimum = (\xs -> if null xs
                        then error "No ranges provided"
                        else minimum xs) . mapMaybe getLowerBound

getLowerBound :: Range Int -> Maybe Int
getLowerBound (SingletonRange s) = Just s
getLowerBound (SpanRange a b)
    | boundType  a == Inclusive = Just $ boundValue a
    | boundValue a == boundValue b && boundType b == Inclusive = Just $ boundValue a
    | boundValue a + 1  == boundValue b && boundType b == Exclusive && boundType a == Exclusive = Nothing
    | otherwise = Just (boundValue a + 1)
getLowerBound _ = error "something broke because we are dealing with infinity somehow"

-- Run
runPart1 :: Almanac -> IO ()
runPart1 almanac = do
    putStr "Part 1: "
    print $ getMinimum (seedToLocation almanac (map SingletonRange (view seeds almanac)))

runPart2 :: Almanac -> IO ()
runPart2 almanac = do
    putStr "Part 2: "
    print $ getMinimum (seedToLocation almanac (view seedsRange almanac))

run05 :: String -> IO ()
run05 input = do
    let parsed = runParser parseInput () "" input
    case parsed of
        Left e -> print e
        Right almanac -> do
            computationTime $ runPart1 almanac
            computationTime $ runPart2 almanac

