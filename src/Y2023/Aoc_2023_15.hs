{-# LANGUAGE OverloadedStrings, TemplateHaskell #-}
module Y2023.Aoc_2023_15
( run15
)
where

import Control.Lens (over,set,view)
import Control.Lens.TH (makeLenses)
import Control.Monad.State
import Data.Char (ord)
import qualified Data.Text as T
import qualified Data.Vector as V
import Text.Parsec hiding (Empty, State, label)

import Utility.BaseUtil (computationTime)
import Utility.ParseUtil (parsePositiveNumber)

-- | Data structures
data RealLens = RealLens {
    _label :: T.Text,
    _focal :: Int
   } deriving (Show, Eq)
makeLenses ''RealLens

newtype Box = Box {
    _lenses :: [RealLens]
   } deriving (Show, Eq)
makeLenses ''Box

data Operation = Minus T.Text | Equals T.Text Int deriving (Eq, Show)

-- | Parsing
parseInput :: Parsec T.Text u [Operation]
parseInput =
    parseOperation `sepBy` char ','

parseOperation :: Parsec T.Text u Operation
parseOperation = try parseMinus <|> parseEquals

parseMinus :: Parsec T.Text u Operation
parseMinus = do
    label <- many1 letter
    char '-'
    return $ Minus (T.pack label)

parseEquals :: Parsec T.Text u Operation
parseEquals = do
    label <- many1 letter
    char '='
    Equals (T.pack label) <$> parsePositiveNumber

-- | Part 1
toHash :: T.Text -> Int
toHash = T.foldl helper 0
        where helper :: Int -> Char -> Int
              helper acc c = ((acc + ord c) * 17) `mod` 256

runPart1 :: T.Text -> IO ()
runPart1 input = do
        let tokens = T.splitOn (T.pack ",") . T.dropEnd 1 $ input
        let result = sum $ map toHash tokens
        putStr "Part 1: "
        print result

-- | Part 2
applyOperations :: [Operation] -> State (V.Vector Box) ()
applyOperations = mapM_ applyOperation

applyOperation :: Operation -> State (V.Vector Box) ()
applyOperation (Minus label) = do
        boxes <- get
        let boxIdx = toHash label
        let targetBox = boxes V.! boxIdx
        let ls = view lenses targetBox
        if null ls
            then return ()
            else do
                let boxUpdates = over lenses (removeLens label) targetBox
                put $ boxes V.// [(boxIdx, boxUpdates)]
applyOperation (Equals label focal) = do
        boxes <- get
        let boxIdx = toHash label
        let targetBox = boxes V.! boxIdx
        let boxUpdates =  over lenses (insertLens (RealLens {_label = label, _focal = focal })) targetBox
        put $ boxes V.// [(boxIdx, boxUpdates)]

removeLens :: T.Text -> [RealLens] -> [RealLens]
removeLens targetLabel [] = []
removeLens targetLabel (l:ls)
    | view label l == targetLabel = ls
    | otherwise = l: removeLens targetLabel ls

insertLens :: RealLens -> [RealLens] -> [RealLens]
insertLens newLens [] = [newLens]
insertLens newLens (l:ls)
    | view label l == view label newLens = newLens:ls
    | otherwise = l : insertLens newLens ls

sumBoxes :: V.Vector Box -> Int
sumBoxes = V.sum . V.imap sumBox

sumBox :: Int -> Box -> Int
sumBox idx box = sum . map (\(slot,realLens) -> (1+idx) * slot * view focal realLens) $ zip [1..] (view lenses box)

runPart2 :: T.Text -> IO ()
runPart2 input = do
    let parsed = runParser parseInput () "" input
    case parsed of
        Left e -> print e
        Right operations -> do
            putStr "Part 2: "
            let startingBoxes = V.fromList . replicate 256 $ Box {_lenses = []}
            let results = sumBoxes . execState (applyOperations operations) $ startingBoxes
            print results

-- | Running
run15 :: String -> IO ()
run15 input = do
    let txt = T.pack input
    computationTime $ runPart1 txt
    computationTime $ runPart2 txt
