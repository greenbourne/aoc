module Y2023.Aoc_2023_08
    (run08
    )
where

import Data.Array ((!), bounds)
import Data.List (sort)
import Data.Maybe (fromJust, mapMaybe, fromMaybe)
import qualified Data.Map as M
import qualified Data.Set as S
import Text.Parsec hiding (Empty, State)

import Utility.BaseUtil
import Utility.GraphUtil

data Direction = LeftDir | RightDir deriving (Show, Eq)

-- | Parsing
parseInput :: Parsec String u (Graph Direction, M.Map String Int, [Direction])
parseInput = do
    directions <- map toDirection <$> many1 alphaNum
    newline -- end of first line
    newline -- empty line
    edges <- concat <$> parseOutgoing `endBy` newline
    let nameMap = M.fromList . flip zip [0..] . S.toList . S.fromList . map (\(src,_,_) -> src) $ edges
    let fooEdges = map (\(src,v,dest) -> (fromJust (M.lookup src nameMap),v,fromJust (M.lookup dest nameMap))) edges
    let g = buildG (0,M.size nameMap -1) fooEdges
    return (g, nameMap, directions)

parseOutgoing :: Parsec String u [(String, Direction, String)]
parseOutgoing = do
    src <- manyTill anyChar space
    char '='
    space
    string "("
    dest1 <- manyTill anyChar (char ',')
    space
    dest2 <- manyTill anyChar (string ")")
    return [(src, LeftDir, dest1), (src, RightDir, dest2)]

toDirection :: Char -> Direction
toDirection c
    | c == 'R' = RightDir
    | c == 'L' = LeftDir
    | otherwise = error "Undefined input"

-- | Part 1
part01 :: Vertex -> Graph Direction -> [Direction] -> Vertex -> Int
part01 end g dirs cur
    | cur == end = 0
    | otherwise  = 1 + part01 end g (tail dirs) (step g cur (head dirs))

step :: Graph Direction -> Vertex -> Direction -> Vertex
step g v d = snd . head . filter ((==) d . fst) $ g!v

runPart1 :: String -> IO ()
runPart1 input = do
    let parsed = runParser parseInput () "" input
    case parsed of
        Left e -> print e
        Right (g, m, dirs)  -> do
            let start = fromJust $ "AAA" `M.lookup` m
            let end = fromJust $ "ZZZ" `M.lookup` m
            putStr "Part1: "
            print $ part01 end g (concat . repeat $ dirs) start

-- Part2
stepsUntilEnd :: S.Set Vertex -> Graph Direction -> [Direction] -> Vertex -> Int
stepsUntilEnd endpoints g dirs cur
    | cur `S.member` endpoints = 0
    | otherwise = 1 + stepsUntilEnd endpoints g (tail dirs) (step g cur (head dirs))

stepUntilEndAll :: [Vertex] -> S.Set Vertex -> Graph Direction -> [Direction] -> M.Map Vertex Int
stepUntilEndAll startPoints endPoints g dirs = M.fromList . map (\s -> (,) s $ stepsUntilEnd endPoints g dirs s) $ startPoints

-- | This only works in O(1) and with the help of LCM because of how the
-- input is constructed.
-- The poor writing of the description does not enforce this kind of
-- input at all.
runPart2 :: String -> IO ()
runPart2 input = do
    let parsed = runParser parseInput () "" input
    case parsed of
        Left e -> print e
        Right (g, m, dirs)  -> do
            let startPoints = map snd . filter (\(k,_) -> last k == 'A') $ M.assocs m
            let endPoints = S.fromList . map snd . filter (\(k,_) -> last k == 'Z') $ M.assocs m
            let sequenceRepeated = concat . repeat $ dirs
            let end = foldr lcm 1. M.elems $ stepUntilEndAll startPoints endPoints g sequenceRepeated
            putStr "Part2: "
            print end

-- | Run all
run08 :: String -> IO ()
run08 input = do
   computationTime $ runPart1 input
   computationTime $ runPart2 input
