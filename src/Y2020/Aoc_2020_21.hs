{-# LANGUAGE TupleSections #-}

module Y2020.Aoc_2020_21
  ( run21,
  )
where

import Data.Bifunctor
import Data.Functor.Identity
import Data.List (intercalate, sortBy)
import Data.List.Split (splitOneOf)
import qualified Data.Map as M
import qualified Data.Set as S
import Text.Parsec

------ parsing -----

data FoodInfo = FoodInfo {
   ingredients :: S.Set String,
   allegens :: S.Set String
} deriving (Show, Eq)

parseLine :: ParsecT String u Identity FoodInfo
parseLine = do
  ingr <- many1 (satisfy (/= '('))
  allegen <-between (char '(') (char ')') (many (satisfy (/= ')')))
  return $ FoodInfo (S.fromList (words ingr)) (S.fromList (filter (\a -> a/= "contains" && a /= "") $ splitOneOf ", " allegen))

parseInput :: ParsecT String u Identity [FoodInfo]
parseInput = parseLine `sepBy` endOfLine <* eof

---- part 1 ----
findPossibleAllegen2Ingr :: Foldable t => t FoodInfo -> M.Map String (S.Set String)
findPossibleAllegen2Ingr fs =
                   let asMap = concatMap (\f -> map (,ingredients f) (S.toList $ allegens f)) fs
                       allIngr = foldr (\f acc -> S.union acc (ingredients f)) S.empty fs
                       allAll = foldr (\f acc -> S.union acc (allegens f)) S.empty fs
                       allPossibilities = M.fromList (map (,allIngr ) (S.toList allAll))
                   in foldr (\(a,ingrs) m -> M.update (Just . S.intersection ingrs) a m) allPossibilities asMap
---- part 2 ----
findUniqueMapping :: M.Map String String -> M.Map String (S.Set String) -> M.Map String String
findUniqueMapping solvedMapping oldMap =
  let solvedIngrs    = S.fromList $ M.elems solvedMapping
      updated        = M.map (\v -> S.difference v (S.intersection v solvedIngrs)) oldMap
      nextMapping    = findNextMapping solvedMapping updated
      in if nextMapping == solvedMapping
       then nextMapping
       else findUniqueMapping nextMapping updated

findNextMapping :: M.Map String String -> M.Map String (S.Set String) -> M.Map String String
findNextMapping solved m = let uniques = map (second (head . S.toList)). filter (\p -> S.size (snd p) == 1) . M.assocs $ m
                               in M.fromList $ M.toList solved ++ uniques

---- execute ----
run21 :: String -> IO ()
run21 input = do
  let foods =  runParser parseInput () "" input
  case foods of
    Right fs -> do
                   let all2PossibleIngr = findPossibleAllegen2Ingr fs
                   let ingrWithAllegens = foldr S.union S.empty . M.elems $ all2PossibleIngr
                   let allIngrWithoutAllegens = foldr (\f acc -> acc + S.size (S.difference (ingredients f) (S.intersection ingrWithAllegens (ingredients f)))) 0 fs
                   print $ "Part 1: " ++ show allIngrWithoutAllegens
                   let allegenIngrMapping = M.toList $ findUniqueMapping M.empty all2PossibleIngr
                   let ingredientList = intercalate "," . map snd $ sortBy (\(a1,_) (a2,_) -> compare a1 a2) allegenIngrMapping
                   print $ "Part 2: " ++ ingredientList
    Left e  -> print e