{-# LANGUAGE TypeApplications #-}

module Y2020.Aoc_2020_13 (run13) where

import Data.List.Split
import Data.List (sortBy)
import Data.Bifunctor (second)

run13 :: String -> IO ()
run13 input = do
  let [arr, busses] = take 2 $ lines input
  let arrival = read arr :: Int
  let roundtrips = map (read @Int) . filter (/= "x") $ splitOn "," busses
  let waitingTimes =  map ((\rs -> second (fst rs -) rs) . (\r -> (r,arrival `mod` r))) roundtrips
  let (time, bus) = head $ sortTuples snd waitingTimes
  print $ "Task1: " ++ show (time*bus)
  print $ "Task2: " ++ show (solveTask2 busses)

type Rest = Int
type Coefficient = Int

solveTask2 :: String -> Integer
solveTask2 s = let input2Mod = findModsOfStartPoint . mapToIndices $ s
                   n = product . map snd $ input2Mod
                   coeffs = map (\(a,b,c) -> (toInteger a, toInteger b, toInteger c)) $ calculateCoef n input2Mod
                   in solveSimulCong coeffs `mod` toInteger n


sortTuples :: ((Int, Int) -> Int) -> [(Int, Int)] -> [(Int, Int)]
sortTuples f = sortBy (\a b -> compare (f a) (f b))

mapToIndices :: (Num a, Enum a) => [Char] -> [(a, Int)]
mapToIndices s = map (second (read @Int)) . filter (\e -> snd e /= "x") $ zip [0..] (splitOn "," s)

findModsOfStartPoint :: [(Int, Int)] -> [(Rest, Int)]
findModsOfStartPoint = map (\(idx, v) -> ((v-idx) `mod` v, v))

solveSimulCong :: [(Integer, Integer, Integer)] -> Integer
solveSimulCong = sum . map (\ (c, r, v) ->  extendedEuclid c v * r * c)

calculateCoef :: Int -> [(Rest, Int)] -> [(Coefficient, Rest, Int)]
calculateCoef prod xs = do
              x <- xs
              let coeff = div prod (snd x)
              return (coeff, fst x, snd x)

extendedEuclid :: Integral b => b -> b -> b
extendedEuclid a b = let (r,t,_) = head $ dropWhile (\(rs,_,_) -> snd rs /= 0) euclid
                     in fst r * fst t
                      where euclid = iterate (\((old_r, r), (old_s, s), (old_t, t)) ->
                                  let quotient = old_r `div` r
                                    in ((r,old_r - quotient * r),(s, old_s - quotient * s), (t,old_t - quotient * t)))
                                           ((a,b), (1,0), (0,1))
