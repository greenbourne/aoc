module Y2020.Aoc_2020_25
  ( run25,
  )
where

----- part1 -----
determineLoopSize :: Int -> Int -> Int -> Int
determineLoopSize public subject value
        | public == value = 0
        | otherwise       =  1 + determineLoopSize public subject (step subject value)

loopNTimes :: Int -> Int -> Int -> Int
loopNTimes n subject value
        | n == 0    = value
        | otherwise = loopNTimes (n - 1) subject (step subject value)

step :: Int -> Int -> Int
step subject value = (subject * value) `mod` 20201227

run25 :: String -> IO ()
run25 input = do
        let numbers = map (read :: String -> Int) $ lines input
        let publicKey1 = numbers!!0
        let publicKey2 = numbers!!1
        let loopSize1 =  determineLoopSize publicKey1 7 1
        let loopSize2 = determineLoopSize publicKey2 7 1
        putStrLn $ "Part 1: " ++ (show $ loopNTimes loopSize2 publicKey1 1)
