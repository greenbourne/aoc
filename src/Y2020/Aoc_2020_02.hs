module Y2020.Aoc_2020_02 (run02) where

import Text.ParserCombinators.ReadP
import Data.Char (isDigit, isAlpha)

data PasswortInput = PasswortInput (Int,Int) Char String deriving (Show)
run02 :: String -> IO ()
run02 input = do
  let parsedInput = fst . head . filter ((== "") . snd) $ readP_to_S parseInput input -- format: 'a-b n: password'
  let validPasswords = length . filter (== True) . map validatePasswordPart1 $ parsedInput
  putStrLn $ "Part 1: Number of valid passwords: " ++ show validPasswords
  let validPasswords2 = length . filter (== True) . map validatePasswordPart2 $ parsedInput
  putStrLn $ "Part 2: Number of valid passwords: " ++ show validPasswords2


parseInput  :: ReadP [PasswortInput]
parseInput = parseLine `sepBy` endOfLine

parseLine :: ReadP PasswortInput
parseLine = do
  (a,b) <- range
  _ <- many1 whitespace
  n <- get
  _ <- string ":"
  _ <- many1 whitespace
  password <- many1 (satisfy isAlpha)
  return $ PasswortInput (a,b) n password


validatePasswordPart1 :: PasswortInput -> Bool
validatePasswordPart1 (PasswortInput r c pw)  = occurences r $ filter (== c) pw
          where occurences :: (Int, Int) -> String -> Bool
                occurences (a,b) s = (length s >= a) && (length s <= b)

validatePasswordPart2 :: PasswortInput -> Bool
validatePasswordPart2 (PasswortInput (a,b) c p)  =
    let entryA = checkEntry p (a-1) c
        entryB = checkEntry p (b-1) c
    in entryA `xor` entryB
         where
         checkEntry :: String -> Int -> Char -> Bool
         checkEntry pw index ch
            | length pw <= index = False
            | pw!!index == ch = True
            | otherwise = False

range :: ReadP (Int, Int)
range = do
  a <- decimal
  _ <- string "-"
  b <- decimal
  return (a,b)

decimal :: ReadP Int
decimal = read <$> many1 (satisfy isDigit)

whitespace :: ReadP Char
whitespace = char ' '

endOfLine :: ReadP Char
endOfLine = char '\n'

infixr 2 `xor`  -- same precedence & associativity as (||)
xor :: Bool -> Bool -> Bool
xor True p = not p
xor False p = p
