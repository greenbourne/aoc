module Y2020.Aoc_2020_23
  ( run23,
  findCupsDestination,
  )
where

import Control.Monad.ST
import Control.Monad.Primitive
import Data.Char (digitToInt)
import qualified Data.List as L
import qualified Data.Set as S
import qualified Data.Vector as V
import qualified Data.Vector.Mutable as MV

---- part 1 ----

moveNCupsV :: Int -> V.Vector Int -> [V.Vector Int]
moveNCupsV n = iterate (moveNCupsV' n)

moveNCupsV' :: Int -> V.Vector Int -> V.Vector Int
moveNCupsV' n cups = let current = cups V.! 0
                         movingCups = V.take n $ V.drop 1 cups
                         remaining  = V.drop n $ V.drop 1 cups
                         (i, _)   = findMatching current remaining
                            in  V.take (i+1) remaining <> movingCups <> V.drop (i + 1) remaining <> V.singleton current
                            where
                              findMatching :: Int -> V.Vector Int -> (Int, Int)
                              findMatching c xs =
                                         let assocs  = V.zip (V.fromList [0..]) xs
                                             smallerNumbers =  V.filter (c >) xs
                                             in if null smallerNumbers then
                                                 L.maximumBy (\a b -> compare (snd a) (snd b)) assocs
                                                 else let searchedNumber = maximum smallerNumbers
                                                          in V.head $ V.filter (\x -> snd x == searchedNumber) assocs

-- part 2
type CrabState s = MV.MVector s

initialise :: [Int] -> [Int]
initialise xs = let wAncestor = zip xs $ tail xs
                    idxSorted       = L.sortBy (\a b -> compare (fst a) (fst b)) wAncestor
                    in map (\(_,y) -> y-1) idxSorted

runCrabGame :: V.Vector Int -> Int -> Int -> V.Vector Int
runCrabGame v n start = runST $ do
  cs <- V.thaw v
  go cs n start
  V.freeze cs
  where go state n' current
          | n' == 0 = pure ()
          | otherwise = do
              next <- step state current
              go state (n'-1) next


step :: (PrimMonad m) => CrabState (PrimState m) Int -> Int -> m Int
step cs currentIdx = do
  mc1 <- MV.read cs currentIdx
  mc2 <- MV.read cs mc1
  mc3 <- MV.read cs mc2
  afterMCs <-  MV.read cs mc3
  let destCup = findCupsDestination currentIdx (MV.length cs) (S.fromList [mc1, mc2, mc3])
  MV.write cs currentIdx afterMCs
  afterDestCup <- MV.read cs destCup
  MV.write cs destCup mc1
  MV.write cs mc3 afterDestCup
  pure afterMCs

findCupsDestination :: Int -> Int -> S.Set Int -> Int
findCupsDestination c amount movingCups =
           let possibleDests =  S.map (\x -> if c - x < 0 then amount + (c - x) else c - x) $ S.fromList [1..4]
               validDests = S.filter (`S.notMember` movingCups) possibleDests
               smallerDests = S.filter (< c) validDests
               in if null smallerDests then  maximum . S.toList $ validDests
                  else maximum . S.toList $ smallerDests

---- execute ----
cupAmount :: Int
cupAmount = 1000000

numberOfRounds :: Int
numberOfRounds = 10000000

run23 :: String -> IO ()
run23 input = do
  let parseResult = map digitToInt input :: [Int]

  -- part 1
  let hundredRoundsV = moveNCupsV 3 (V.fromList parseResult) !! 100
  let labelV = concatMap show $ V.tail (V.dropWhile (/= 1) hundredRoundsV) <> V.takeWhile (/= 1) hundredRoundsV
  print $ "Part 1: " ++ labelV

  -- part 2
  let initialVector = V.fromList $ initialise (parseResult ++ [1 + length parseResult]) ++ [(1 + length parseResult)..(cupAmount-1)] ++ [head parseResult - 1]
  let finalResult = runCrabGame initialVector numberOfRounds (head parseResult - 1)
  let firstNumber =  (finalResult V.! 0) + 1
  let secondNumber = (finalResult V.! (firstNumber-1)) + 1
  print $ "First number: " ++ show firstNumber ++ "; second number: " ++ show secondNumber
  print $ "Part 2: " ++ show (firstNumber*secondNumber)
