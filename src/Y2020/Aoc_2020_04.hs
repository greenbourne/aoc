module Y2020.Aoc_2020_04 where

import Text.ParserCombinators.ReadP
import Data.Char (isDigit, isSpace, isHexDigit)
import Data.List (nub)
import Control.Monad

run04 :: String -> IO ()
run04 input = do
  let parsedAll = readP_to_S inputP input
  let parsedInput = fst . head . filter ((== "") . snd) $ parsedAll
  let validPassports = length . filter (True ==) . map hasRequiredFields $ parsedInput
  print $ "Exercise1: Valid Passports: " ++ show validPassports
  let validPassports2 = length . filter (True == ) $ map (\pass -> hasRequiredFields pass && validateFieldContent pass) parsedInput
  print $ "Exercise2: Valid Passports: " ++ show validPassports2

hasRequiredFields :: Passport -> Bool
hasRequiredFields p = validateCid (nub p)
    where
      validateCid :: Passport -> Bool
      validateCid p_no_dup
        | length p_no_dup == 8 = True
        | length p_no_dup == 7 = not $ containsCID p_no_dup
        | otherwise            = False

validateFieldContent :: Passport -> Bool
validateFieldContent ((BYR byr):ps)
  | byr >= 1920 && byr <= 2002 = validateFieldContent ps
  | otherwise = False
validateFieldContent ((IYR iyr):ps)
  | iyr >= 2010 && iyr <= 2020 = validateFieldContent ps
  | otherwise = False
validateFieldContent ((EYR eyr):ps)
  | eyr >= 2020 && eyr <= 2030 = validateFieldContent ps
  | otherwise = False
validateFieldContent (h@(HGT _):ps) = validateHeight h && validateFieldContent ps
validateFieldContent (h@(HCL _):ps) = validateHairColor h && validateFieldContent ps
validateFieldContent (e@(ECL _):ps) = validateEyeColor e && validateFieldContent ps
validateFieldContent (p@(PID _):ps) = validatePid p && validateFieldContent ps
validateFieldContent ((CID _):ps) = validateFieldContent ps
validateFieldContent [] = True

validatePid :: PassportEntry -> Bool
validatePid (PID p) = any ((== "") . snd ) $ readP_to_S parseValidatePid p
    where parseValidatePid :: ReadP ()
          parseValidatePid = do
            count 9 (satisfy isDigit)
            return ()
validatePid _ = False

validateEyeColor :: PassportEntry -> Bool
validateEyeColor (ECL ecl) = any ((== "") . snd ) $ readP_to_S parseValidateEyeColor ecl
    where parseValidateEyeColor :: ReadP ()
          parseValidateEyeColor = do
             string "amb" +++ string "blu" +++ string "brn" +++ string "gry" +++ string "grn" +++ string "hzl" +++ string "oth"
             return ()
validateEyeColor _ = False

validateHairColor :: PassportEntry -> Bool
validateHairColor (HCL hcl) = any ((== "") . snd ) $ readP_to_S parseValidateHaircolor hcl
    where parseValidateHaircolor = do
              char '#'
              count 6 (satisfy isHexDigit)
              return ()
validateHairColor _ = False

validateHeight :: PassportEntry -> Bool
validateHeight (HGT hgt) = any ((== "") . snd ) $ readP_to_S parseValidateHeight hgt
  where parseValidateHeight = do
          number <- decimal
          unit <- string "cm" +++ string "in"
          unless ((unit == "cm" && number >= 150 && number <= 198) || (unit == "in" && number >= 59 && number <= 76)) pfail
validateHeight _ = False


containsCID :: Passport -> Bool
containsCID [] = False
containsCID ((CID _):_) = True
containsCID (_:rs) = containsCID rs

type Passport = [PassportEntry]

data PassportEntry
  = BYR Int
  | IYR Int
  | EYR Int
  | HGT String
  | HCL String
  | ECL String
  | PID String
  | CID String
  deriving (Eq, Show)

--- parsing
inputP :: ReadP [[PassportEntry]]
inputP = entriesP `sepBy` endOfLine

entriesP :: ReadP [PassportEntry]
entriesP =  (byrP +++ iyrP +++ eyrP +++ hclP +++ hgtP +++ eclP +++ cidP +++ pidP ) `sepBy` (whitespace +++ endOfLine) <* endOfLine

byrP :: ReadP PassportEntry
byrP = string "byr:" *> (BYR <$> decimal)

iyrP :: ReadP PassportEntry
iyrP = string "iyr:" *> (IYR <$> decimal)

eyrP :: ReadP PassportEntry
eyrP = string "eyr:" *> (EYR <$> decimal)

hgtP :: ReadP PassportEntry
hgtP = string "hgt:" *> (HGT <$> letters)

hclP :: ReadP PassportEntry
hclP = string "hcl:" *> (HCL <$> letters)

eclP :: ReadP PassportEntry
eclP = string "ecl:" *> (ECL <$> letters)

pidP :: ReadP PassportEntry
pidP = string "pid:" *> (PID <$> letters)

cidP :: ReadP PassportEntry
cidP = string "cid:" *> (CID <$> letters)

decimal :: ReadP Int
decimal = read <$> many1 (satisfy isDigit)

letters :: ReadP String
letters = many1 (satisfy (not . isSpace))

endOfLine :: ReadP Char
endOfLine = char '\n'

whitespace :: ReadP Char
whitespace = char ' '
