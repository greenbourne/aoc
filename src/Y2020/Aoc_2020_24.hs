module Y2020.Aoc_2020_24
  ( run24,
  )
where

import Data.Functor.Identity
import Data.List (foldl',intercalate)
import qualified Data.Map.Strict as M
import qualified Data.Set as S
import Text.Parsec hiding (Empty)

----- parsing -----
parseTilePlan :: ParsecT String u Identity [Direction]
parseTilePlan = do
        many1 ( choice [
                       string "w" >> return W,
                       string "e" >> return E,
                       try (string "se" >> return SE),
                       try (string "ne" >> return NE),
                       try (string "nw" >> return NW),
                       try (string "sw" >> return SW)
                       ]
              )

parseTilePlans :: ParsecT String u Identity [[Direction]]
parseTilePlans = parseTilePlan `endBy` newline

----- types -----
data Direction = SE | SW | NE | NW | E | W deriving (Show, Eq, Enum, Bounded)

---- part 1 ----
dirToFactor :: Direction -> (Int, Int)
dirToFactor SE = (1, -2)
dirToFactor SW = (-1, -2)
dirToFactor NE = (1, 2)
dirToFactor NW = (-1, 2)
dirToFactor E  = (2, 0)
dirToFactor W  = (-2, 0)

planToTilePos :: [Direction] -> (Int, Int)
planToTilePos dirs = foldl' (\acc dir -> (fst acc + fst dir, snd acc + snd dir)) (0,0) (map dirToFactor dirs)

buildMap :: [(Int, Int)] -> M.Map (Int, Int) Int
buildMap positions = let initial = M.empty
                         in foldr (\pos m -> M.insertWith (\_ old -> old+1) pos 1 m) initial positions

getNumberBlackTiles :: M.Map (Int, Int) Int -> Int
getNumberBlackTiles = M.size . M.filter (odd)

---- part 2 ----
getBlackTiles :: M.Map (Int, Int) Int -> S.Set (Int, Int)
getBlackTiles = S.fromList . M.keys . M.filter (odd)

iteration :: Int -> S.Set (Int, Int) -> S.Set (Int, Int)
iteration 0 blackTiles = blackTiles
iteration days blackTiles =
        let nextDay = foldr (\blackTile acc -> acc `S.union` (flip4BlackTileAdj blackTile blackTiles)) S.empty blackTiles
            in iteration (days-1) nextDay

flip4BlackTileAdj :: (Int, Int) -> S.Set (Int, Int) -> S.Set (Int,Int)
flip4BlackTileAdj cur m = let adjs = S.fromList $ getAdjs cur
                              blackAdjs = adjs `S.intersection` m
                              whiteAdjs = adjs `S.difference` blackAdjs
                              numberBlackAdjs = S.size blackAdjs
                              flipCur = if numberBlackAdjs == 1 || numberBlackAdjs == 2 then False else True
                              white2Flip = filter (flipWhite m) (S.toList whiteAdjs)
                              in if flipCur then S.fromList white2Flip else S.fromList $ cur:white2Flip

flipWhite:: S.Set (Int, Int) -> (Int, Int) -> Bool
flipWhite m white = let adjs = S.fromList $ getAdjs white
                        blackAdjs = adjs `S.intersection` m
                        in if S.size blackAdjs == 2 then True else False

getAdjs :: (Int, Int) -> [(Int, Int)]
getAdjs cur = map (\d -> (fst cur + fst d, snd cur + snd d)) $ map dirToFactor [SE .. W]

run24 :: String -> IO ()
run24 input = do
        let plans = runParser parseTilePlans () "" input
        case plans of
            Left e -> print e
            Right parsedInput -> do
                let markedTiles = buildMap . map planToTilePos $ parsedInput
                -- part 1
                let numberBlackTiles = getNumberBlackTiles markedTiles
                putStrLn $ "Part 1: " ++ show numberBlackTiles
                --- part 2
                let blackTiles = getBlackTiles markedTiles
                let hundredDays = iteration 100 blackTiles
                putStrLn $ "Part 2: " ++ show ( S.size hundredDays)
