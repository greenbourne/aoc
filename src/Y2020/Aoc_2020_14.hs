module Y2020.Aoc_2020_14 (run14) where

import Control.Monad.State.Lazy
import qualified Data.Bits as B
import Data.Char (isAlphaNum, isDigit)
import qualified Data.IntMap as I
import Text.ParserCombinators.ReadP hiding (get)

newline :: ReadP Char
newline = char '\n'

decimal :: ReadP Int
decimal = read <$> many1 (satisfy isDigit)

asciis :: ReadP String
asciis = many (satisfy isAlphaNum)

parseMask :: ReadP Mask
parseMask = do
  string "mask = "
  m <- asciis
  let maskAll = zip [0 ..] $ reverse m
  let zeros = map fst . filter (('0' ==) . snd) $ maskAll
  let ones = map fst . filter (('1' ==) . snd) $ maskAll
  let floatings = map fst . filter (('X' ==) . snd) $ maskAll
  return $ Mask zeros ones floatings

parseMemoryAccess :: ReadP MemoryAccess
parseMemoryAccess = do
  string "mem["
  dest <- decimal
  string "] = "
  val <- decimal
  return (dest, val)

parseBlock :: ReadP Block
parseBlock = do
  m <- parseMask <* newline
  memory <- parseMemoryAccess `endBy` newline
  return (m, memory)

parseInput :: ReadP [Block]
parseInput = many1 parseBlock


getAllAddresses :: Int -> Mask -> [Int]
getAllAddresses v mask = do
  let static = manipulateOnes v (ones mask)
  combination <- perm (length $ floating mask)
  let comb2Floating = zip (floating mask) combination
  let zerosFloating = map fst $ filter ((==) 0 . snd) comb2Floating
  let onesFloating = map fst $ filter ((==) 1 . snd) comb2Floating
  return (flip manipulateZeros (zerosFloating) $ manipulateOnes static (onesFloating))

getIndices :: [Int] -> ([Int], [Int])
getIndices values =
  let v2i = valuesToIndex values
      zeros = filter ((==) 0 . snd) v2i
      ones = filter ((==) 1 . snd) v2i
   in (map fst zeros, map fst ones)

valuesToIndex :: [Int] -> [(Int, Int)]
valuesToIndex = zip [0 ..]

perm :: Int -> [[Int]]
perm 0 = []
perm 1 = [[0], [1]]
perm n = do
  sublist <- perm (n -1)
  [0 : sublist, 1 : sublist]

manipulateOnes :: Int -> [Int] -> Int
manipulateOnes = foldr $ flip B.setBit

manipulateZeros :: Int -> [Int] -> Int
manipulateZeros = foldr $ flip B.clearBit

executeBlock :: Block -> State (I.IntMap Int) ()
executeBlock (mask, mems) = do
  mapM_
    ( \(dest, val) ->
        do
          let valWith0 = foldr (flip B.clearBit) val (zeros mask)
          let valWith0And1 =
                foldr (flip B.setBit) valWith0 (ones mask)
          currentMap <- get
          put $ I.insert dest valWith0And1 currentMap
    )
    mems


executeBlock2 :: Block -> State (I.IntMap Int) ()
executeBlock2 (mask, mems) = do
   mapM_
     ( \(dest, val) ->
         do
           let addresses = getAllAddresses dest mask
           mapM_ (\addr -> do
                          currentMap <- get
                          put $ I.insert addr val currentMap
                          ) addresses
     )
     mems

runBlocks :: [Block] -> I.IntMap Int
runBlocks blocks = execState (mapM_ executeBlock blocks) I.empty

runBlocks2 :: [Block] -> I.IntMap Int
runBlocks2 blocks = execState (mapM_ executeBlock2 blocks) I.empty

task1 :: [Block] -> Int
task1 = sum . I.elems . runBlocks

task2 :: [Block] -> Int
task2 = sum . I.elems . runBlocks2

solveTask :: ([Block] -> I.IntMap Int) -> [Block] -> Int
solveTask f = sum . I.elems . f

type Block = (Mask, [MemoryAccess])

data Mask = Mask {zeros :: [Int], ones :: [Int], floating :: [Int]} deriving (Show)

type MemoryAccess = (Int, Int)

run14 :: String -> IO ()
run14 input = do
  let blocks = fst . head $ filter ((== "") . snd) $ readP_to_S parseInput input
  print $ show (solveTask runBlocks blocks)
  print $ show (solveTask runBlocks2 blocks)
  return ()
