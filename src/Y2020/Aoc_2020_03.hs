module Y2020.Aoc_2020_03 where

import Data.Array

run03 :: String -> IO ()
run03 input = do
  let arr = stringsToGrid . lines $ input
  let steps = [(1,1),(1,3),(1,5),(1,7),(2,1)]
  let trees = map (encounteredTrees arr (snd . bounds $ arr) (0,0)) steps
  print $ "Exercise 2: Number of Trees on routes: " ++ show trees
  print $ product trees

stringsToGrid :: [String] -> Array (Int, Int) Surface
stringsToGrid input = listArray ((0, 0), (length input -1 ,(length . head $ input) - 1)) . map toSurface . concat $ input

encounteredTrees :: Array (Int, Int) Surface -> Coordinate -> Coordinate -> Step -> Int
encounteredTrees a m current step = length . filter (== Tree) $ countTreesOnRoute a (drop 1 $ generateRoute step m current)

countTreesOnRoute :: Array (Int, Int) Surface -> Route -> [Surface]
countTreesOnRoute a (r:rs) = a ! r : countTreesOnRoute a rs
countTreesOnRoute _ [] = []

generateRoute :: Step -> Coordinate -> Coordinate -> Route
generateRoute step@(s_row,s_column) m@(max_row, max_column) current@(current_row, current_column)
  | current_row > max_row = []
  | otherwise = current : generateRoute step m (current_row+s_row, (current_column+s_column) `mod` (max_column +1))

toSurface :: Char -> Surface
toSurface c
  | c == '#' = Tree
  | otherwise = Plain

type Route = [(Int, Int)]
type Step = (Int, Int)
type Coordinate = (Int, Int)

data Surface = Tree | Plain deriving (Eq)

instance Show Surface where
  show Plain = "."
  show Tree = "#"

