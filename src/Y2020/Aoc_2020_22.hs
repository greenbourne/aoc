module Y2020.Aoc_2020_22
  ( run22,
  )
where

-- This part is highly influenced by this solution: https://github.com/mstksg/advent-of-code-2020/blob/master/src/AOC/Challenge/Day22.hs
-- My solution, even with State monads etc was way to slow. On my way through debugging and finding the
-- error the above mentioned solution had the missing link. I forgot to lookup the maximum in the decks.
-- Anyway, his solution was so nice,that I rewrote it for learning purposes.

import Control.Monad (guard)
import Data.Functor.Identity
import qualified Data.IntMap as IM
import Data.Sequence as SE
import qualified Data.Set as S
import Data.Hashable

import Text.Parsec hiding (Empty)

------ parsing -----
parseInput :: ParsecT String u Identity ([Int], [Int])
parseInput = do
  string "Player 1:"
  newline
  deck1 <- parseDeck
  newline
  string "Player 2:"
  newline
  deck2 <- parseDeck
  return (deck1, deck2)

parseDeck :: ParsecT String u Identity [Int]
parseDeck = parseNumber `endBy` newline

parseNumber :: ParsecT String u Identity Int
parseNumber = read <$> many1 digit

---- part 2 ----

data Player = P1 | P2 deriving (Eq, Show)

type Deck = Seq Int

seq2List :: Seq a -> [a]
seq2List = foldr (:) []

hashHand :: (Deck,Deck) -> Int
hashHand (xs,ys) = hash (seq2List (SE.take 2 xs), seq2List $ SE.take 2 ys, SE.length xs)

game2 :: (Deck, Deck) -> (Player, Deck)
game2 = playRecursiveCombat IM.empty (\(topP1 :<| tailP1,topP2 :<| tailP2) -> do
                  xs <- takeExactly topP1 tailP1
                  ys <- takeExactly topP2 tailP2
                  let maxP1 = maximum xs
                      maxP2 = maximum ys
                  if maxP1 > maxP2 then
                    return P1
                  else return . fst $ game2 (xs, ys)
              )

takeExactly :: Int -> Deck -> Maybe Deck
takeExactly n xs = SE.take n xs <$ guard (SE.length xs >= n)

playRecursiveCombat :: IM.IntMap (S.Set (Deck, Deck)) -> ((Deck, Deck) -> Maybe Player) -> (Deck, Deck) -> (Player, Deck)
playRecursiveCombat _ _ (p1, Empty) = (P1, p1)
playRecursiveCombat _ _ (Empty, p2) = (P2, p2)
playRecursiveCombat seen handle decks@(topP1 :<| tailP1, topP2 :<| tailP2)
      | collision = (P1, fst decks)
      | otherwise = case handle decks of
        Nothing -> if topP1 > topP2 then playRecursiveCombat seen' handle (tailP1 :|> topP1 :|> topP2,tailP2)
                    else playRecursiveCombat seen' handle (tailP1, tailP2:|> topP2 :|> topP1)
        Just p -> case p of
          P1 -> playRecursiveCombat seen' handle (tailP1 :|> topP1 :|> topP2,tailP2)
          _->  playRecursiveCombat seen' handle (tailP1, tailP2 :|>topP2 :|> topP1)
      where collision = case IM.lookup (hashHand decks) seen of
              Nothing -> False
              Just s  -> decks `S.member` s
            seen' = IM.insertWith (<>) (hashHand decks) (S.singleton decks) seen

---- execute ----
run22 :: String -> IO ()
run22 input = do
  let parseResult =  runParser parseInput () "" input
  case parseResult of
    Left e -> print e
    Right (p1, p2) -> do
          print $ "Deck 1: " ++ show p1
          print $ "Deck 2: " ++ show p2
          let finalDeck = snd $ game2 (fromList p1,fromList p2)
          let resultPart2 = foldr (\(mul, v) acc -> acc + mul * v) 0 $ Prelude.zip [1..] (Prelude.reverse . seq2List $ finalDeck)
          print $ "Part2: Deck value: " ++ show resultPart2
