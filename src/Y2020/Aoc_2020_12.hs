{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Y2020.Aoc_2020_12 (run12) where

import Prelude hiding (Left, Right)
import Control.Monad.State.Lazy
import Data.Char (isDigit)

import Text.ParserCombinators.ReadP hiding (get)

data Instruction = Direction CardinalDirection Int | Left Degree | Right Degree | Forward Int deriving (Eq, Show)

data CardinalDirection = North | East | South | West deriving (Eq, Show, Enum, Bounded)
data Degree = Ninety | HundredEighty | TwoHundretSecenty | ThreeSixty deriving (Eq, Show)

type NorthSouth = Int
type EastWest = Int

run12 :: String -> IO ()
run12 input = do
  let instructions = fst . head $ filter ((== "") . snd) $ readP_to_S parseInput input
  print $ show instructions
  let (_,shipLocation) = runInstructions instructions
  print $ "Task2: " ++ show (computeManhattenDistance shipLocation)
  return ()

computeManhattenDistance :: (NorthSouth, EastWest) -> Int
computeManhattenDistance (ns, ew) = abs ns + abs ew

runInstructions :: [Instruction] -> ((NorthSouth, EastWest), (NorthSouth, EastWest))
runInstructions instrs = execState (followAllInstructions instrs) ((1, 10), (0,0))

followAllInstructions :: [Instruction] -> State ((NorthSouth, EastWest), (NorthSouth, EastWest)) ()
followAllInstructions = mapM_ followInstruction

followInstruction :: Instruction -> State ((NorthSouth, EastWest), (NorthSouth, EastWest)) ()
followInstruction instr = do
      (currentPosWaypoint@(wns,wew), currentPosShip@(sns, sew)) <- get
      case instr of
        Direction d n -> do
          put (moveShipCardinal currentPosWaypoint d n, currentPosShip)
        Forward v -> do
          put (currentPosWaypoint, (v*wns + sns, v*wew+sew) )
        Left d ->
          put (rotateWayPointLeft currentPosWaypoint d, currentPosShip)
        Right d ->
          put (rotateWayPointRight currentPosWaypoint d, currentPosShip)
          

rotateWayPointRight :: (NorthSouth, EastWest) -> Degree -> (NorthSouth, EastWest)
rotateWayPointRight waypoint d
    | d == Ninety = rotateWayPointLeft waypoint TwoHundretSecenty
    | d == HundredEighty = rotateWayPointLeft waypoint HundredEighty
    | d == TwoHundretSecenty = rotateWayPointLeft waypoint Ninety
    | otherwise = waypoint


rotateWayPointLeft :: (NorthSouth, EastWest) -> Degree -> (NorthSouth, EastWest)
rotateWayPointLeft waypoint d
    | d == Ninety = rotateWayPointLeftOnce waypoint
    | d == HundredEighty = iterate rotateWayPointLeftOnce waypoint !! 2
    | d == TwoHundretSecenty = iterate rotateWayPointLeftOnce waypoint !! 3
    | otherwise = waypoint

rotateWayPointLeftOnce ::  (NorthSouth, EastWest) -> (NorthSouth, EastWest)
rotateWayPointLeftOnce (wns, wew)
    | wns >= 0 &&  wew >= 0 = (wew, (-1) * wns) --- 1,12 -> 12, -1
    | wns >= 0 &&  wew <= 0 = (wew, (-1) * wns) --- 12, -1 -> -1, -12
    | wns <= 0 &&  wew <= 0 = (wew, (-1) * wns) ---- -1,-12 -> -12, 1
    | wns <= 0 &&  wew >= 0 = (wew,  (-1) * wns)

rotateShipLeft :: CardinalDirection -> Degree -> CardinalDirection
rotateShipLeft cd d
    | d == ThreeSixty = cd
    | d == Ninety = previous cd
    | d == HundredEighty = previous (previous cd)
    | otherwise = previous (previous (previous cd))

rotateShipRight :: CardinalDirection -> Degree -> CardinalDirection
rotateShipRight cd d
    | d == ThreeSixty = cd
    | d == Ninety = next cd
    | d == HundredEighty = next (next cd)
    | otherwise = next (next (next cd))

moveShipCardinal :: (NorthSouth, EastWest) -> CardinalDirection -> Int -> (NorthSouth, EastWest)
moveShipCardinal (x,y) cd value = case cd of
      North -> (x+value, y)
      South -> (x-value, y)
      East -> (x, y+value)
      West -> (x, y-value)

parseInput :: ReadP [Instruction]
parseInput = parseInstruction `endBy` (char '\n')

parseInstruction :: ReadP Instruction
parseInstruction =  parseSouth +++ parseNorth +++ parseWest +++ parseEast +++ parseLeft +++ parseRight +++ parseForward

parseSouth = char 'S' *> (Direction South <$> decimal)
parseNorth = char 'N' *> (Direction North <$> decimal)
parseWest = char 'W' *> (Direction West <$> decimal)
parseEast = char 'E' *> (Direction East <$> decimal)
parseLeft = char 'L' *> (Left <$> parseDegree)
parseRight = char 'R' *> (Right <$> parseDegree)
parseForward = char 'F' *> (Forward <$> decimal)

parseDegree :: ReadP Degree
parseDegree = do
  degree <- decimal
  case degree of
    90 -> return Ninety
    180 -> return HundredEighty
    270 -> return TwoHundretSecenty
    360 -> return ThreeSixty
    _ -> pfail


decimal :: ReadP Int
decimal = read <$> many1 (satisfy isDigit)

previous :: forall a . (Enum a, Bounded a) => a -> a
previous x | from x > from minBound = pred x
           | otherwise              = maxBound where from :: a -> Int; from = fromEnum

next :: forall a . (Enum a, Bounded a) => a -> a
next x | from x < from maxBound = succ x
       | otherwise              = minBound where from :: a -> Int; from = fromEnum
