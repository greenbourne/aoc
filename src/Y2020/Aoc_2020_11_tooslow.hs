{-# LANGUAGE FlexibleContexts #-}

module Y2020.Aoc_2020_11_tooslow (run11) where

import Control.Monad.ST
import Control.Monad
import Data.Array
import Data.Array.ST
import Data.Functor (($>))
import Data.List.Split (chunksOf)
import Debug.Trace
import Text.ParserCombinators.ReadP

run11 :: String -> IO ()
run11 input = do
  let initialRoom = fst . head $ filter ((== "") . snd) $ readP_to_S parseRoom input

  --print $ "adj for (0,0)" ++ show (getAdjSeats (0,0) arr)
  ----print $ show arr
  ----print "\n\n\n"
  --putStr (prettyPrint (iteration arr))
  --let finalRoom = findSameRoom 0 (generateInfiniteRooms arr)
  let finalRoom = runArray ((0,0), (length (head initialRoom) - 1 , length initialRoom - 1)) (concat initialRoom)
  putStr (prettyPrint finalRoom)
  let occSeats = length $ filter (UsedSeat ==) (elems finalRoom)
  putStrLn $ "Task 1: " ++ show occSeats
  return ()

data RoomEntity = EmptySeat | UsedSeat | Floor deriving (Eq)

instance Show (RoomEntity) where
  show p
        | p == UsedSeat = "#"
        | p ==  EmptySeat = "L"
        | otherwise = "."

prettyPrint :: Array (Int, Int) RoomEntity -> String
prettyPrint arr = unlines $ chunksOf (1 + (snd . snd. bounds $ arr)) (concat . map show $ elems arr)

findSameRoom :: Int -> [Array (Int, Int) RoomEntity] -> Array (Int, Int) RoomEntity
findSameRoom c (x:ys@(y:_))
        | x == y = x
        | otherwise = trace ("Count: " ++ show c) $ findSameRoom (c+1) ys
        
generateInfiniteRooms :: Array (Int, Int) RoomEntity -> [Array (Int, Int) RoomEntity]
generateInfiniteRooms = iterate iteration

iteration :: Array (Int, Int) RoomEntity -> Array (Int, Int) RoomEntity
iteration arr = listArray (bounds arr) nextRoom
       where nextRoom = iteration' (indices arr) arr

iteration' :: [(Int, Int)] -> Array (Int, Int) RoomEntity -> [RoomEntity]
iteration' [] _ = []
iteration' (p:ps) arr = nextSeat p arr : iteration' ps arr
      where nextSeat :: (Int, Int) -> Array (Int, Int) RoomEntity -> RoomEntity
            nextSeat ix arr
                  | arr ! ix == Floor = Floor
                  | arr ! ix == EmptySeat =  if seatStaysEmpty p arr then EmptySeat else UsedSeat
                  | otherwise = if seatStaysOccupied p arr then UsedSeat else EmptySeat

seatStaysEmpty :: (Int, Int) -> Array (Int, Int) RoomEntity -> Bool
seatStaysEmpty (x,y) arr = any ((==) UsedSeat . (arr !)) (getAdjSeats (x,y) arr)

seatStaysOccupied :: (Int, Int) -> Array (Int, Int) RoomEntity -> Bool
seatStaysOccupied (x,y) arr =  numberOfAdjOcc <= 3
       where numberOfAdjOcc = length $ filter ((==) UsedSeat . (arr !)) (getAdjSeats (x,y) arr)

getAdjSeats :: (Int, Int) -> Array (Int, Int) RoomEntity -> [(Int, Int)]
getAdjSeats z@(x,y) arr =
     let (b_start, b_end) = bounds arr
         in filter (\e ->
                (fst b_start <= fst e) && (snd b_start <= snd e)
                 && (fst b_end >= fst e) && (snd b_end >= snd e) && (e /= z) )
                 [(x+mx, y+my) | mx <- [-1, 0, 1], my <- [-1,0,1]]

parseRoom :: ReadP [[RoomEntity]]
parseRoom = parseRoomRow `endBy` newline

parseRoomRow :: ReadP [RoomEntity]
parseRoomRow = many (parseEmptySeat +++ parseUsedSeat +++ parseFloor)

parseUsedSeat :: ReadP RoomEntity
parseUsedSeat = char '#' $> UsedSeat

parseFloor :: ReadP RoomEntity
parseFloor = char '.' $> Floor

parseEmptySeat :: ReadP RoomEntity
parseEmptySeat = char 'L' $> EmptySeat

newline :: ReadP Char
newline = char '\n'


----- Mutable approach

runArray :: ((Int, Int), (Int, Int)) -> [RoomEntity] -> Array (Int, Int) RoomEntity
runArray bnds values = runSTArray $
  do arr <- newListArray bnds values
     idxs <- fmap fst <$> getAssocs arr
     iterationM 0 idxs arr
     return arr


iterationM :: Int -> [(Int, Int)] -> STArray s (Int, Int) RoomEntity -> ST s ()
iterationM rounds pos arr = do
  es <- getElems arr
  updatePosition pos arr
  es' <- getElems arr
  trace ("round nr" ++ show rounds) (return ())
  unless (es == es') (iterationM (rounds+1) pos arr)


updatePosition :: [(Int, Int)] -> STArray s (Int, Int) RoomEntity -> ST s ()
updatePosition [] _ = return ()
updatePosition positions a =  do
  newValues <- mapM (\pos ->  do
    readArray a pos >>= \e -> nextSeat e pos a >>= (\newSeat -> return (pos,newSeat))
   ) positions
  mapM_ (uncurry (writeArray a)) newValues
      where nextSeat :: RoomEntity -> (Int, Int) -> STArray s (Int, Int) RoomEntity -> ST s RoomEntity
            nextSeat re pos arr
                  | re == Floor = return Floor
                  | re == EmptySeat = do
                     keepEmpty <- seatStaysEmptyM pos arr
                     if keepEmpty then return EmptySeat else return UsedSeat
                  | otherwise = do
                      occupied <- seatStaysOccupiedM pos arr
                      if occupied then return UsedSeat else return EmptySeat


seatStaysEmptyM :: (Int, Int) -> STArray s (Int, Int) RoomEntity -> ST s Bool
seatStaysEmptyM (x,y) arr = do
          adjs <- getAdjSeatsM (x,y) arr
          (return . or) =<< mapM (fmap (UsedSeat ==) . readArray arr) adjs


seatStaysOccupiedM :: (Int, Int) -> STArray s (Int, Int) RoomEntity -> ST s Bool
seatStaysOccupiedM (x,y) arr = do
       adjs <- getAdjSeatsM (x,y) arr
       usedSeats <- filterM (readArray arr >=> (return . (==) UsedSeat)) adjs
       return $ length usedSeats <= 3

getAdjSeatsM :: (Int, Int) -> STArray s (Int, Int) RoomEntity -> ST s [(Int, Int)]
getAdjSeatsM z@(x,y) arr = do
      (b_start, b_end) <- getBounds arr
      return $ filter (\e ->
                 (fst b_start <= fst e) && (snd b_start <= snd e)
                  && (fst b_end >= fst e) && (snd b_end >= snd e) && (e /= z) )
                  [(x+mx, y+my) | mx <- [-1, 0, 1], my <- [-1,0,1]]

