{-# LANGUAGE TemplateHaskell #-}
module Y2020.Aoc_2020_20
  ( run20,
  )
where

import Control.Lens (over, set, view)
import Control.Lens.TH (makeLenses)
import Control.Monad.State
import Control.Monad
import Control.Monad.ST
import Control.Monad.Primitive
import Data.Bifunctor (second)
import Data.Char
import qualified Data.IntMap as I
import Data.Maybe
import qualified Data.Vector as V
import qualified Data.Vector.Mutable as MV
import qualified Data.Set as S
import Utility.VectorUtil (Grid, (!!!), (!!?))

import Text.ParserCombinators.ReadP hiding (get)

-- | Represent rotation
data D8 = Flip
    | Flip90
    | Flip180
    | Flip270
    | Id90
    | Id180
    | Id270
    | Id deriving (Show, Eq)

-- | State
data Puzzle = Puzzle {
    _unused :: !(I.IntMap (Grid Bool)),
    _allPieces :: !(I.IntMap (Grid Bool)),
    _lastInsertedPiece :: !(Grid Bool),
    _lastPosition :: (Int, Int),
    _finalPuzzle :: !(Grid (D8,Int)),
    _candidates :: !(S.Set Int)
    }
makeLenses ''Puzzle

------ parsing -----
newline :: ReadP Char
newline = char '\n'

decimal :: ReadP Int
decimal = read <$> many1 (satisfy isDigit)

parseTileName :: ReadP Int
parseTileName = do
  string "Tile "
  number <- decimal
  char ':'
  newline
  return number

parseTileLine :: ReadP (V.Vector Bool)
parseTileLine = V.fromList . map ('#' ==) <$> many (char '.' +++ char '#')

parseTile :: ReadP (Int, Grid Bool)
parseTile = do
  number <- parseTileName
  g <- V.fromList <$> parseTileLine `endBy` newline
  return (number, g)

parseInput :: ReadP [(Int, Grid Bool)]
parseInput = parseTile `sepBy` newline

--- Data representation and utility functions
writeValue :: V.MVector s [a] -> (Int, Int) -> a -> ST s ()
writeValue m (c,r) v = do
   col <- MV.read m c
   let (x,_:ys) = splitAt r col
   MV.write m c (x++v:ys)

rotateLeft :: Grid Bool -> Grid Bool
rotateLeft g = V.imap (\rowIdx col -> V.imap (\colIdx _ -> g !!! (colIdx, rows - rowIdx - 1)) col) g
  where
    rows = V.length g

flipAround :: Grid Bool -> Grid Bool
flipAround g = V.imap (\rowIdx col -> V.imap (\colIdx _ -> g !!! (rowIdx, rows - colIdx - 1)) col) g
  where
    rows = V.length g

getAllSides :: Grid Bool -> [(Grid Bool, D8)]
getAllSides g = [
  (g,Id),
  (rotateLeft g, Id90),
  (rotateLeft . rotateLeft $ g, Id180),
  (rotateLeft . rotateLeft . rotateLeft $ g, Id270),
  (flipAround g, Flip),
  (flipAround . rotateLeft $ g, Flip90),
  (flipAround . rotateLeft . rotateLeft $ g, Flip180),
  (flipAround . rotateLeft . rotateLeft . rotateLeft $ g, Flip270)
  ]

-- part 1
edgeMatch :: Grid Bool -> Grid Bool -> Bool
edgeMatch g1 g2 = or [a == b | a <- getAllEdges g1, b <- getAllEdges g2]

getAllEdges :: Grid Bool -> [V.Vector Bool]
getAllEdges g =
  let firstCol = V.fromList $ map (\r -> g !!! (r, 0)) [0 .. (V.length g -1)]
      lastCol = V.fromList $ map (\r -> g !!! (r, V.length g - 1)) [0 .. (V.length g -1)]
   in V.head g : firstCol : lastCol : [V.last g]

rotateFlip :: Grid Bool -> D8 -> Grid Bool
rotateFlip g dir =
    case dir of
        Id -> g
        Id90 -> rotateLeft g
        Id180 -> rotateLeft . rotateLeft $ g
        Id270 -> rotateLeft . rotateLeft . rotateLeft $ g
        Flip  -> flipAround g
        Flip90 -> flipAround . rotateLeft $ g
        Flip180 -> flipAround . rotateLeft . rotateLeft $ g
        Flip270 -> flipAround . rotateLeft . rotateLeft . rotateLeft $ g

-- | hasMatch a b: if b can be rotated/flipped in a way to
-- | match a border of a, this D8 is returned
hasMatch :: Grid Bool -> Grid Bool -> Maybe D8
hasMatch g1 g2 =
  let g2Sides = getAllSides g2
   in listToMaybe [ snd g2' | g2' <- g2Sides,
                              edgeMatch g1 (fst g2')]

findNeighbours :: [(Int, Grid Bool)] -> [(Int, [(Int, D8)])]
findNeighbours nr2Grids = map ( \(nr, g) -> (nr, [(nr', fromJust $ hasMatch g g') | (nr', g') <- nr2Grids, g /= g', isJust $ hasMatch g g'] )) nr2Grids

-- part2
findTilePositions :: [(Int, [Int])] -> V.Vector [Int]
findTilePositions neighbours =
  runST $ do
         let borders = filter ((<=) 3 . length . snd) neighbours
         let tilesMap = I.fromList neighbours
         let emptyPuzzle = V.fromList (replicate (length neighbours) (replicate (length neighbours) defaultValue))
         mutPuzzle <- V.thaw emptyPuzzle
         writeValue mutPuzzle (0,0) (fst . head $ borders)
         V.freeze mutPuzzle

choosePuzzlePiece :: (Int, Int) -> S.Set Int -> I.IntMap [Int] -> Grid Int -> Int
choosePuzzlePiece current borders tMap tiles =
  let adjPieces = getAdjPieces current tiles
      matches   = findMatchingPieces tMap adjPieces
      usedPieces = S.fromList $ filter (/= defaultValue) $ catMaybes [tiles !!? (i,j) | i <- [0.. (length tiles)], j <- [0.. (length tiles)]]
  in if isBorderPiece current tiles
   then
      head $ dropWhile (`S.member` usedPieces) $ S.toList (S.intersection (S.fromList matches) borders)
   else
      head $ dropWhile (`S.member` usedPieces) matches

isBorderPiece :: (Int, Int) -> Grid Int -> Bool
isBorderPiece (ax, ay) puzzle
  | ax == 0 || ay == 0 = True
  | ax == length puzzle - 1 || ay == length puzzle -1 = True
  | otherwise = False

findMatchingPieces :: Ord a => I.IntMap [a] -> [a] -> [I.Key]
findMatchingPieces tMap adjPieces =
  let keyValue = map (second S.fromList) $ I.assocs tMap
      adjSet = S.fromList adjPieces
  in map fst $ filter (\(_, neighbours) -> adjSet `S.isSubsetOf` neighbours) keyValue

defaultValue :: Int
defaultValue = -1

getAdjPieces :: p -> Grid Int -> [Int]
getAdjPieces coord puzzle = filter (/= defaultValue) . mapMaybe (puzzle !!?) $ [(0,-1), (-1, 0), (0, 1), (1,0)]

-- | New approach
--
--
buildPuzzle :: State Puzzle ()
buildPuzzle = do
    initPuzzle <- get
    first <- findFirstPiece
    put $ set unused (I.delete first (view unused initPuzzle)) initPuzzle
    go
    where go :: State Puzzle ()
          go = do
              p <- get
              if null (view unused p)
                  then return ()
                  else do
                      insertNextPiece
                      go

findFirstPiece :: State Puzzle Int
findFirstPiece = undefined

insertNextPiece :: State Puzzle ()
insertNextPiece = undefined

findCandidatePieces :: State Puzzle ()
findCandidatePieces = do
    s <- get
    let uPieces = I.toList . view unused $ s
    let idToNeighbours = findNeighbours uPieces
    put $ set candidates (S.fromList . map fst . filter ((>=) 2 . length . snd) $ idToNeighbours) s

setPiece :: State Puzzle ()
setPiece = do
    s <- get
    let lastPieceO = view lastInsertedPiece s
    let ca = view candidates s
    return ()

getNextPiecePos :: State Puzzle (Int, Int)
getNextPiecePos = do
    puzzle <- get
    let lastPiece = view finalPuzzle puzzle !!! view lastPosition puzzle :: (D8, Int)
    return (0,0)

run20 :: String -> IO ()
run20 input = do
  let name2Grids = fst . head $ filter ((== "") . snd) $ readP_to_S parseInput input
  let name2Neighbours = findNeighbours name2Grids
  print . show $ name2Neighbours
