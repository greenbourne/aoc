module Utility.VectorUtil
  ( (!!!),
    (!!?),
    (///),
    addVec,
    subVec,
    vectorLength,
    Grid(..),
    Position,
    getNeighbours,
    getNeighboursI
    )
where

import Data.Bifunctor (first,bimap,second)
import Data.List (groupBy)
import Data.Maybe (isJust, fromJust, mapMaybe)
import qualified Data.Vector as V

--- Helper Types ---
type Grid a = V.Vector (V.Vector a)
type Position = (Int, Int)

--- Utility functions ---
-- Access grids entry on a given position
(!!!) :: Grid a -> (Int, Int) -> a
(!!!) g (c, r) = let col = g V.! c in col V.! r

-- Access grids entry on a given position but safely
(!!?) :: Grid a -> Position -> Maybe a
(!!?) g (c, r) = do
    col <- g V.!? c
    col V.!? r

-- Update Grids positions with given values
(///) :: Grid a -> [(Position, a)] -> Grid a
(///) g positions2Up =
    let grouped = filter (not . null) $ groupBy (\a b -> fst (fst a) == fst (fst b)) positions2Up
        in foldr (\e a -> let idx = fst (fst (head e))
                              upRow = case a V.!? idx of
                                          Nothing -> V.empty -- this case seems wrong
                                          Just row -> row V.// map (first snd) e
                              in if idx >= V.length a
                                     then a
                                     else a `V.update` V.fromList [(idx, upRow)]
                 ) g grouped

getNeighbours :: Grid a -> Position -> [a]
getNeighbours grid p =
        let ms = [(1,0),(0,1),(-1,0),(0,-1)]
            coords = map (\m -> bimap (fst m +) (snd m +) p) ms
            in mapMaybe (grid !!?) coords

getNeighboursI :: Grid a -> Position -> [(Position, a)]
getNeighboursI grid p =
        let ms = [(1,0),(0,1),(-1,0),(0,-1)]
            coords = map (\m -> bimap (fst m +) (snd m +) p) ms
            in map (second fromJust). filter (isJust . snd) $ map (\c -> (c, grid !!? c)) coords

addVec :: V.Vector Int -> V.Vector Int -> V.Vector Int
addVec v1 v2 = V.fromList $ zipWith (+) (V.toList v1) (V.toList v2)

subVec :: V.Vector Int -> V.Vector Int -> V.Vector Int
subVec v1 v2 = V.fromList $ zipWith (-) (V.toList v1) (V.toList v2)

vectorLength :: V.Vector Int -> Float
vectorLength = sqrt . sum . map (\foo -> fromIntegral foo^^2) . V.toList
