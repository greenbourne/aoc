module Utility.GraphUtil (
    Vertex,
    Table,
    Graph,
    Bounds,
    Edge,
    Labeling,
    LabGraph,
    buildG,
    vertices,
    labels,
    reverseE,
    showGraphViz,
    edges
    )
where

import Data.Array

-- | See https://wiki.haskell.org/The_Monad.Reader/Issue5/Practical_Graph_Handling
-- | for more information
type Vertex = Int
type Table a = Array Vertex a
type Graph e = Table [(e, Vertex)]
type Bounds  = (Vertex, Vertex)
type Edge e = (Vertex, e, Vertex)

type Labeling a = Vertex -> a
data LabGraph n e = LabGraph (Graph e) (Labeling n)

vertices :: LabGraph n e -> [Utility.GraphUtil.Vertex]
vertices (LabGraph gr _) = indices gr

labels :: LabGraph b e -> [b]
labels (LabGraph gr l) = map l (indices gr)

edges :: Graph e -> [Edge e]
edges g = [ (v, l, w) | v <- indices g, (l, w) <- g!v ]

buildG :: Bounds -> [Edge e] -> Graph e
buildG bounds0 edges0 = accumArray (flip (:)) [] bounds0 [(v, (l,w)) | (v,l,w) <- edges0]

-- | The graph obtained by reversing all edges.
transposeG  :: Graph e -> Graph e
transposeG g = buildG (bounds g) (reverseE g)

reverseE    :: Graph e -> [Edge e]
reverseE g   = [ (w, l, v) | (v, l, w) <- edges g ]

-- Get showGraphViz format
showGraphViz :: (Show a, Show n) => LabGraph n a -> String
showGraphViz (LabGraph gr lab) =
    "digraph name {\n" ++
    "rankdir=LR;\n" ++
    (concatMap showNode $ indices gr) ++
    (concatMap showEdge $ edges gr) ++
    "}\n"
    where showEdge (from, t, to) = show from ++ " -> " ++ show to ++
               " [label = \"" ++ show t ++ "\"];\n"
          showNode v = show v ++ " [label = " ++ (show $ lab v) ++ "];\n"

