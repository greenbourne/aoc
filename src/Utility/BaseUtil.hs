module Utility.BaseUtil
    (
    computationTime,
    insertElemAt
    )
    where

import qualified Data.Set as S
import System.CPUTime (getCPUTime)
import Text.Printf (printf)

insertElemAt :: a -> S.Set Int -> [a] -> [a]
insertElemAt x indices =
        helper x indices 0
        where helper :: a -> S.Set Int -> Int -> [a] -> [a]
              helper y indices curIdx []
                | curIdx `S.member` indices = [y]
                | otherwise = []
              helper y indicies curIdx (z:zs)
                | curIdx `S.member` indices = y : z : helper y indices (curIdx+1) zs
                | otherwise = z : helper y indices (curIdx+1) zs


computationTime :: IO t -> IO t
computationTime a = do
    start <- getCPUTime
    v <- a
    end   <- getCPUTime
    let diff = fromIntegral (end - start) / (10^12)
    printf "Computation time: %0.3f sec\n" (diff :: Double)
    return v
