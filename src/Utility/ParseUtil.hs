{-# LANGUAGE FlexibleContexts #-}

module Utility.ParseUtil
  ( parseNegativeNumber,
    parsePositiveNumber,
    parseNumberRow,
    parseAnyNumber
    )
where

import Data.Functor.Identity
import qualified Data.Text as T
import Text.Parsec hiding (Empty, State)
import Data.Maybe (mapMaybe)
import Text.Read (readMaybe)

parseNegativeNumber :: Stream s m Char => ParsecT s u m Int
parseNegativeNumber = do
        try $ char '-'
        ((*) (-1) . read :: String -> Int) <$> many1 digit

parsePositiveNumber :: Stream s m Char => ParsecT s u m Int
parsePositiveNumber = (read :: String -> Int) <$> many1 digit

parseAnyNumber :: Stream s m Char => ParsecT s u m Int
parseAnyNumber = parseNegativeNumber <|> parsePositiveNumber

parseNumberRow :: Stream s m Char => ParsecT s u m [Int]
parseNumberRow =
    (spaces *> (parseNegativeNumber <|> parsePositiveNumber) `sepBy` many1 (char ' ')) <* newline

-- Parsing functions useful for debugging:
-- parseTest (parserTraced "label:" parseInput) input
