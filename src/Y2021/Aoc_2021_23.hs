{-# LANGUAGE TemplateHaskell #-}
module Y2021.Aoc_2021_23
  ( run23 )
where

import Control.Monad (unless)
import Control.Monad.State
import Control.Lens (over, set, view)
import Control.Lens.TH (makeLenses)
import Data.Bifunctor (bimap)
import Data.Functor.Identity
import Data.List (maximumBy)
import qualified Data.Map as M
import Data.Maybe (catMaybes)
import Data.Monoid ((<>))
import qualified Data.Set as S
import qualified Data.Vector as V
import Utility.VectorUtil ((!!?))
import Text.Parsec hiding (Empty, State)

--- Helper Types ---
type Grid a = V.Vector (V.Vector a)
type Position = (Int, Int)

-- Data structures
type Board = Grid Char
data PieceType = Amber | Bronze | Copper | Desert deriving (Show, Eq)
instance Ord PieceType where
        compare a b
            | a == b = EQ
            | a == Amber = GT
            | b == Amber = LT
            | a == Bronze = GT
            | a == Copper && b == Desert = GT
            | otherwise = LT

data Piece = Piece {
    _position :: !Position,
    _piecetype :: !PieceType,
    _weight :: Int
    } deriving (Show, Eq)
makeLenses ''Piece

instance Ord Piece where
    compare a b = compare (view piecetype a) (view piecetype b) <> compare (view position a) (view position b) <> compare (view weight a) (view weight b)

data GameState = GameState {
    _pieces :: S.Set Piece,
    _visited :: M.Map (S.Set Piece) Int,
    _cost :: Int,
    _path :: String,
    _bestValue :: Int
    } deriving (Show, Eq)
makeLenses ''GameState

-- Parsing
parseInput :: ParsecT String u Identity GameState
parseInput = createGameState <$> parseRawBoard

parseRawBoard :: ParsecT String u Identity Board
parseRawBoard = V.fromList <$> parseLine `endBy` newline

parseLine :: ParsecT String u Identity (V.Vector Char)
parseLine = V.fromList <$> many1 (noneOf "\n")

convert2Piece :: Board -> Position -> Maybe Piece
convert2Piece grid pos = do
        c <- grid !!? pos
        t <- convert2PieceType c
        return Piece { _position = pos, _piecetype = t, _weight = pieceWeight t}

convert2PieceType :: Char -> Maybe PieceType
convert2PieceType c
    | c == 'A' = Just Amber
    | c == 'B' = Just Bronze
    | c == 'C' = Just Copper
    | c == 'D' = Just Desert
    | otherwise = Nothing

pieceWeight :: PieceType -> Int
pieceWeight pt
    | pt == Amber = 1
    | pt == Bronze = 10
    | pt == Copper = 100
    | otherwise = 1000

createGameState :: Board -> GameState
createGameState g =
        let maxHeight = V.length g - 1
            maxWidth = V.length (g V.! 0) - 1
            pieces = S.fromList . catMaybes $ [convert2Piece g (x,y) | x <- [0..maxHeight], y <- [0..maxWidth]]
            in GameState { _pieces = pieces, _visited = M.empty, _cost = 0, _path = "", _bestValue = 1000000}

-- Given Facts Part2
homeA = S.fromList [(2,3),(3,3),(4,3),(5,3)]
homeB = S.fromList [(2,5),(3,5),(4,5),(5,5)]
homeC = S.fromList [(2,7),(3,7),(4,7),(5,7)]
homeD = S.fromList [(2,9),(3,9),(4,9),(5,9)]
lastRow = 5

-- Given Facts Part1
--homeA = S.fromList [(2,3),(3,3)]
--homeB = S.fromList [(2,5),(3,5)]
--homeC = S.fromList [(2,7),(3,7)]
--homeD = S.fromList [(2,9),(3,9)]
--lastRow = 3

allHomes = homeA `S.union` homeB `S.union` homeC `S.union` homeD
invalidStopPositions = S.fromList [(1,3),(1,5),(1,7),(1,9)]
validStopPositions = S.fromList [(1,1),(1,2),(1,4),(1,6),(1,8),(1,10),(1,11)]
validWalkPositions = validStopPositions `S.union` invalidStopPositions `S.union` homeA `S.union` homeB `S.union` homeC `S.union` homeD

-- Implementation
isHomeFinished :: PieceType -> State GameState Bool
isHomeFinished pt = do
        gs <- get
        let as = filter ((==) pt . view piecetype) (S.toList $ view pieces gs)
        return $ all ((`S.member` homeForPieceType pt) . view position) as

canMoveToHome :: PieceType -> State GameState Bool
canMoveToHome pt = do
        foreignPiecePositions <- gets (map (view position) . filter ((/=) pt . view piecetype) . S.toList . view pieces)
        return (S.null $ S.fromList foreignPiecePositions `S.intersection` homeForPieceType pt)

isHomeEmpty :: PieceType -> State GameState Bool
isHomeEmpty pt = do
        allPiecePositions <- gets (map (view position) . S.toList . view pieces)
        return (S.null $ S.fromList allPiecePositions `S.intersection` homeForPieceType pt)

homeForPieceType :: PieceType -> S.Set Position
homeForPieceType Amber = homeA
homeForPieceType Bronze = homeB
homeForPieceType Copper = homeC
homeForPieceType Desert = homeD

allDestinations :: Piece -> State GameState (M.Map Position Int)
allDestinations p = do
        blockedPositions <- gets (S.fromList . map (view position) . S.toList . view pieces)
        M.fromList <$> allDestination' (view piecetype p) (view position p) blockedPositions
        where allDestination' :: PieceType -> Position -> S.Set Position -> State GameState [(Position,Int)]
              allDestination' pt current blocked = do
                  let allPos = S.fromList [ bimap (fst current +) (snd current +) diff |
                                diff <- [(0,1),(1,0),(-1,0),(0,-1)]]
                      nextRealPos = S.toList (((validWalkPositions `S.union` homeForPieceType pt)`S.intersection` allPos) `S.difference` blocked)
                  isCurrentValidStop <- isValidDestination pt current blocked
                  if null nextRealPos
                      then if isCurrentValidStop then return [(current,0)] else return []
                      else do
                          if isCurrentValidStop
                             then map (\(p, c) -> (p,c+1)) . (:) (current,-1) . concat <$> forM nextRealPos (\n -> allDestination' pt n (current `S.insert` blocked))
                             else map (\(p, c) -> (p,c+1)) . concat <$> forM nextRealPos (\n -> allDestination' pt n (current `S.insert` blocked))

isValidDestination :: PieceType -> Position -> S.Set Position -> State GameState Bool
isValidDestination pt pos blocked = do
        toHome <- canMoveToHome pt
        homeEmpty <- isHomeEmpty pt
        if toHome && homeEmpty
            then return $ pos `S.member` ((S.filter ((==) lastRow . fst) (homeForPieceType pt) `S.union` validStopPositions) `S.difference` blocked)
            else if toHome
                     then do
                         ptHomePositions <- gets (map (view position) . filter ((==) pt . view piecetype) . S.toList . view pieces)
                         let emptyHomes = homeForPieceType pt `S.difference` S.fromList ptHomePositions
                         let validStopEmptyHome = maximumBy (\a b -> compare (fst a) (fst b)) $ S.toList emptyHomes
                         return $ pos `S.member` (( validStopEmptyHome `S.insert` validStopPositions) `S.difference` blocked)
                     else return $ pos `S.member` (validStopPositions `S.difference` blocked)

possibleMoves4Piece :: Piece -> State GameState (M.Map Position Int)
possibleMoves4Piece p = do
            allDests <- allDestinations p
            if S.null $ allHomes `S.intersection` S.fromList (M.keys allDests)
                then if view position p `S.member` validStopPositions
                         then return M.empty
                         else return allDests
                else return $ M.filterWithKey (\k a -> k `S.member` allHomes) allDests

pieceCandidates :: State GameState [Piece]
pieceCandidates = do
        ps <- gets (S.toList . view pieces)
        filterM isPieceMovable ps

-- TODO: This function is completely messed up for some odd reason.
-- Removing the comparison of the lastRow leads to zero results, using the
-- last row on Copper und Desert leads to zero results. Just WTF. This
-- function works, has no obvious flaws. Maybe some monadic side effect
-- screwing up everything? Possibly the 'continue' function?
isPieceMovable :: Piece -> State GameState Bool
isPieceMovable p
    | view piecetype p == Amber = isHomeFinished Amber >>= (\a -> return $ not (a || view position p == (lastRow,3)))
    | view piecetype p == Bronze = isHomeFinished Bronze >>= (\a -> return $ not (a || view position p == (lastRow,5)))
    | view piecetype p == Copper = isHomeFinished Copper >>= (\a -> return $ not (a || view position p == (3,7)))
    | view piecetype p == Desert = isHomeFinished Desert >>= (\a -> return $ not (a || view position p == (3,9)))

continue :: State GameState Bool
continue = do
        -- 1. Check if this state has been visited already to prevent loops
        gs <- get
        let currentCost = view cost gs
        if view bestValue gs < currentCost
            -- Costs are already to high
            then return False
            else do
                let hasBeenVisited = view visited gs M.!? view pieces gs
                case hasBeenVisited of
                    Nothing -> do
                        put $ over visited (M.insert (view pieces gs) currentCost) gs
                        return True
                    Just oldCost -> do
                        if currentCost < oldCost
                            then do
                                    put $ over visited (M.insertWithKey (\k newV oldV -> newV) (view pieces gs) currentCost) gs
                                    return True
                            else return False

step :: State GameState [Int]
step = do
        -- 1. Check if this state has been visited already to prevent loops
        doContinue <- continue
        gs <- get
        let currentCost = view cost gs
        if not doContinue
            then return []
            else do
                done <- and <$> forM [Amber, Bronze, Copper, Desert] isHomeFinished
                if done
                    -- 2. All pieces are in final position
                    then if view bestValue gs < currentCost
                             then return []
                             else do
                                 put $ set bestValue currentCost gs
                                 return [0]
                    else do
                        candidates <- pieceCandidates
                        vMperC <- concat <$> forM candidates (\c -> do
                                map (\(m,s) -> (c,m,s)) . M.toList <$> possibleMoves4Piece c)
                        let movesToHome = filter (\(_,m,_) -> m `S.member` allHomes) vMperC
                        let validNextMoves = if null movesToHome
                                                 then vMperC
                                                 else take 1 movesToHome
                        concat <$> forM validNextMoves (\(c, move,nSteps) -> do
                            currentGameState <- get
                            let costForMove = nSteps * view weight c
                            let newPiecePosition = set position move c
                            let updatedPieces = view pieces $ over pieces (S.insert newPiecePosition . S.delete c) gs
                            let updatedCostGS = set cost (costForMove + currentCost) gs
                            let updatedPiecesGS = set pieces updatedPieces updatedCostGS
                            let updatedVisitedGS = set visited (view visited currentGameState) updatedPiecesGS
                            let updatedCompleteGS = set path (view path gs ++ " -> " ++ show (view piecetype c) ++ " " ++ show move) updatedVisitedGS
                            put (over bestValue (\v -> min v (view bestValue currentGameState)) updatedCompleteGS)
                            map (costForMove +) <$> step
                            )

-- Main
run23 :: String -> IO ()
run23 input = do
        let parsed = runParser parseInput () "" input
        case parsed of
            Left e -> print e
            Right gs -> do
                let allResults = evalState step gs
                print $ minimum allResults
