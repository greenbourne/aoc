module Y2021.Aoc_2021_06
  ( run06,
  )
where

import Data.List.Split (splitOn)
import qualified Data.Vector as V
-- part 1 & 2
--
computeFishs :: Int -> V.Vector Int -> Int
computeFishs 0 v = V.sum v
computeFishs n v = computeFishs (n-1) $ step v

step :: V.Vector Int -> V.Vector Int
step v = let clones = v V.! 0
             next   = V.tail v V.++ V.fromList [0]
             in next V.// [(6,(next V.! 6) + clones), (8,(next V.! 8) + clones)]

-- main
run06 :: String -> IO ()
run06 input = do
        let initFish = map (read :: String -> Int) $ splitOn "," input
        print initFish
        let preparedList = map (\n -> length . filter (== n) $ initFish) [0..9]
        print (computeFishs 256 $ V.fromList preparedList)
