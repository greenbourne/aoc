module Y2021.Aoc_2021_22
  ( run22,
  )
where

import Control.Monad (guard, forM, forM_, foldM, foldM_ )
import qualified Control.Monad.State as ST
import Data.Bifunctor (bimap)
import Data.Range
import Data.Functor.Identity
import Data.Maybe (catMaybes, mapMaybe)
import Data.Monoid ((<>))
import qualified Data.Set as S
import Text.Parsec hiding (Empty, State)

import Debug.Trace

-- Data structures
data Instruction = Instruction {
        xRange :: Range Int,
        yRange :: Range Int,
        zRange :: Range Int,
        isOn :: Bool
        } deriving (Show, Eq)

data Cube = Cube {
        x:: Range Int,
        y:: Range Int,
        z:: Range Int
        } deriving (Show, Eq)

data Intersection = Intersection {
        cube :: Cube,
        weight :: Int
        } deriving (Show, Eq)

data CubeIntersectionState = CubeIntersectionState {
        intersections :: [Intersection],
        cubes :: [Cube]
        } deriving (Show, Eq)

-- Parsing
parseInput :: ParsecT String u Identity [Instruction]
parseInput = parseLine `endBy` newline

parseLine :: ParsecT String u Identity Instruction
parseLine = do
    char 'o'
    isOn <- (char 'n' >> return True) <|> (string "ff" >> return False)
    string " x="
    startX <- parseNegativeNumber <|> parsePositiveNumber
    string ".."
    endX <- parseNegativeNumber <|> parsePositiveNumber
    string ",y="
    startY <- parseNegativeNumber <|> parsePositiveNumber
    string ".."
    endY <- parseNegativeNumber <|> parsePositiveNumber
    string ",z="
    startZ <- parseNegativeNumber <|> parsePositiveNumber
    string ".."
    endZ <- parseNegativeNumber <|> parsePositiveNumber
    return $ Instruction {
                         xRange = startX +=+ endX,
                         yRange = startY +=+ endY,
                         zRange = startZ +=+ endZ,
                         isOn = isOn
                         }

parsePositiveNumber :: ParsecT String u Identity Int
parsePositiveNumber = (read :: String -> Int) <$> many1 digit

parseNegativeNumber :: ParsecT String u Identity Int
parseNegativeNumber = do
        try $ char '-'
        ((*) (-1) . read :: String -> Int) <$> many1 digit

-- Part2
findIntersection :: Instruction -> Intersection -> Maybe Intersection
findIntersection instr inter1 =
        let interX = intersection [xRange instr] [x (cube inter1)]
            interY = intersection [yRange instr] [y (cube inter1)]
            interZ = intersection [zRange instr] [z (cube inter1)]
            in if not (null interX) && not (null interY) && not (null interZ)
                   then Just $ Intersection {
                                cube = Cube {
                                        x = head interX,
                                        y = head interY,
                                        z = head interZ
                                        },
                                weight = weight inter1 * (-1)
                                }
                   else Nothing

findIntersectionCs :: Instruction -> Cube -> Maybe Intersection
findIntersectionCs instr cube2 =
        let interX = intersection [xRange instr] [x cube2]
            interY = intersection [yRange instr] [y cube2]
            interZ = intersection [zRange instr] [z cube2]
            in if not (null interX) && not (null interY) && not (null interZ)
                   then Just $ Intersection {
                                cube = Cube {
                                        x = head interX,
                                        y = head interY,
                                        z = head interZ
                                        },
                                weight = -1
                                }
                   else Nothing

step :: Instruction -> ST.State CubeIntersectionState ()
step instr = do
        currentS <- ST.get
        let newCubeIntersections = mapMaybe (findIntersectionCs instr) (cubes currentS)
        let newIntersectionIntersections = mapMaybe (findIntersection instr) (intersections currentS)
        let newCubes = if isOn instr
                           then Cube { x = xRange instr, y = yRange instr, z = zRange instr } : cubes currentS
                           else cubes currentS
        let newIntersections = (newCubeIntersections ++ newIntersectionIntersections) ++ intersections currentS
        ST.put CubeIntersectionState {intersections = newIntersections , cubes = newCubes}

volumeCube :: Cube -> Int
volumeCube c = (rangeToDifference (x c) + 1) * (rangeToDifference (y c) + 1) * (rangeToDifference (z c) + 1)

volumeIntersection :: Intersection -> Int
volumeIntersection inter = volumeCube (cube inter) * weight inter

rangeToDifference :: (Num a, Show a) => Range a -> a
rangeToDifference (SpanRange start end) = boundValue end - boundValue start
rangeToDifference (SingletonRange a) = 0

part2 :: [Instruction] -> ST.State CubeIntersectionState Int
part2 instrs = do
        forM_ instrs step
        final <- ST.get
        let sumCubes = sum $ map volumeCube (cubes final)
        let sumIntersection = sum $ map volumeIntersection (intersections final)
        return $ sumCubes + sumIntersection

run22 :: String -> IO ()
run22 input = do
    let parsed = runParser parseInput () "" input
    case parsed of
        Left e -> print e
        Right instr -> do
            let finalScore = ST.evalState (part2 instr) CubeIntersectionState {intersections=[] , cubes=[]}
            print $ "Final Score: " ++ show finalScore
