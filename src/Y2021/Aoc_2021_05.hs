module Y2021.Aoc_2021_05
  ( run05,
  )
where

import Data.Functor.Identity
import Data.List (nub, partition)
import qualified Data.IntMap as I
import qualified Data.Set as S
import Text.Parsec hiding (Empty, Line)

data Line = Line {
                getStart :: (Int, Int),
                getEnd :: (Int, Int)
                } deriving (Show, Eq)

parseInput :: ParsecT String u Identity [Line]
parseInput = parseLine `sepEndBy` newline

parseLine :: ParsecT String u Identity Line
parseLine = do
        x1 <- (read :: String -> Int) <$> many1 digit
        char ','
        y1 <- (read :: String -> Int) <$> many1 digit
        spaces
        string "->"
        spaces
        x2 <- (read :: String -> Int) <$> many1 digit
        char ','
        y2 <- (read :: String -> Int) <$> many1 digit
        if x1  < x2
            then return $ Line (x1, y1) (x2,y2)
            else return $ Line (x2, y2) (x1,y1)

-- utilities
onXCoord :: Int -> Line -> Bool
onXCoord n l = let (x1,y1) = getStart l
                   (x2,y2) = getEnd l
                   in n <= x2 && n >= x1

getLineStartsFromX :: Int -> [Line] -> [Line]
getLineStartsFromX n = filter (\l ->
                             let l_x1 = fst . getStart $ l
                                 in l_x1 >= n
                             )

getSmallestXStart :: [Line] -> [Line]
getSmallestXStart ls = filter (\l -> smallestX == (fst . getStart $ l)) ls
                where smallestX = foldr (\l acc ->
                                  let l_x1 = fst . getStart $ l
                                    in if l_x1 < acc
                                         then l_x1
                                         else acc
                                  ) (fst . getStart . head $ ls) ls

getNextPositionWithCrossings :: Int -> [Line] -> Maybe Int
getNextPositionWithCrossings n ls
            | length (filter (onXCoord n) ls) > 1 = Just n
            | otherwise = let smallest = getSmallestXStart . getLineStartsFromX n $ ls
                                        in if null smallest
                                                then Nothing
                                                else Just (fst . getStart . head $ smallest)

filterOutDiagonal :: [Line] -> [Line]
filterOutDiagonal = filter isNotDiagonal

isNotDiagonal :: Line -> Bool
isNotDiagonal l = let (x1,y1) = getStart l
                      (x2,y2) = getEnd l
                      in (x1 == x2 || y1 == y2)

-- part 1 & 2
sweep :: Int -> [Line] -> Int
sweep current ls = let linesOnCoord = filter (onXCoord current) ls
                       crits = if length linesOnCoord == 1
                                   then 0
                                   else criticalPoints current linesOnCoord
                        in case getNextPositionWithCrossings (current + 1) ls of
                               Nothing -> crits
                               Just nextPos -> crits + sweep nextPos ls

criticalPoints :: Int -> [Line] -> Int
criticalPoints n ls = let
                        (hv, dia) = partition  isNotDiagonal ls
                        imHV = criticalPointsHV I.empty hv
                        imHVD = criticalPointsD n imHV dia
                        in length . filter (\x -> snd x >= 2) $ I.assocs imHVD

criticalPointsHV :: I.IntMap Int -> [Line] -> I.IntMap Int
criticalPointsHV = foldl (\im l -> foldr (\y im' -> I.insertWith (\_ old -> old + 1) y 1 im') im (getRange (snd (getStart l)) (snd (getEnd l))))
                    where getRange start end
                            | start < end = [start..end]
                            | otherwise = [end..start]

criticalPointsD :: Int -> I.IntMap Int -> [Line] -> I.IntMap Int
criticalPointsD n = foldl (\im' l ->
                       let (x1,y1) = getStart l
                           (x2,y2) = getEnd l
                           diff   = n - x1
                             in if y1 > y2
                                    then I.insertWith (\_ old -> old + 1) (y1 - diff) 1 im'
                                    else I.insertWith (\_ old -> old + 1) (y1 + diff) 1 im'
                         )

-- main
run05 :: String -> IO ()
run05 input = do
    -- parse
    let parsed = runParser parseInput () "" input
    case parsed of
        Left e -> print e
        Right lines -> do
            print $ sweep (fst . getStart . head $ getSmallestXStart lines) lines
