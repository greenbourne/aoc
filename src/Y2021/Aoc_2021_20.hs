module Y2021.Aoc_2021_20
  ( run20,
  )
where

import Control.Monad.State.Strict
import Control.Monad (forM, (=<<))
import Data.Maybe (fromMaybe)
import Data.List.Split (splitOn)
import Data.Functor.Identity
import Data.Bifunctor (bimap)
import qualified Data.Vector as V

--- Helper Types ---
type Grid a = V.Vector (V.Vector a)
type Point = (Int, Int)
type World a = ([Bool], (Bool, Grid a))


--- Utility functions ---
(!!?) :: Grid a -> Point -> Maybe a
(!!?) g (c, r) = do
  col <- g V.!? c
  col V.!? r

prettyPrint :: [[Bool]] -> String
prettyPrint = unlines . map (concatMap (\l -> if l then "#" else "."))

countLight :: Grid Bool -> Int
countLight = V.sum . V.map (V.length . V.filter id)

--- Part1&2 ----
iter :: Int -> State (World Bool) ()
iter 0 = return ()
iter n = nextGrid >> iter (n - 1)

nextGrid :: State (World Bool) ()
nextGrid = do
        (alg, (outer, g)) <- get
        let algEnds = if head alg then (False, True) else (False, False)
        next <- oneStep
        if outer == fst algEnds
            then put (alg, (snd algEnds, next))
            else put (alg, (fst algEnds, next))

createGrid :: Bool -> [[Bool]] -> Grid Bool
createGrid outer lights =
        let bordersTopDown = replicate (2 + length (head lights)) outer
            rowsPlusBorder = map (\l -> outer:(l++[outer])) lights
            together       = bordersTopDown:(rowsPlusBorder ++ [bordersTopDown])
            in V.fromList . map V.fromList $ together

oneStep :: State (World Bool) (Grid Bool)
oneStep = do
            (_, (outer, g)) <- get
            let cols = V.length g
                rows = V.length (g V.! 0)
                coords = [(a,b) | a <- [0..(cols -1)], b <- [0..(rows - 1)]]
            createGrid (not outer) <$> forM [0..(cols-1)] (\c ->
                     forM [0..(rows - 1)] (\r ->
                              enhancePoint (c,r)
                              )
                          )

enhancePoint :: Point -> State (World Bool) Bool
enhancePoint p = do
        bits <- getNeighbours p
        computeNextValue bits

getNeighbours :: Point -> State (World Bool) [Bool]
getNeighbours p = do
        (_, (outerWorld , grid)) <- get
        let ms = [(a,b) | a <- [-1,0,1], b <- [-1,0,1]]
            coords = map (\m -> bimap (fst m +) (snd m +) p) ms
        return $ map (fromMaybe outerWorld . (!!?) grid ) coords

computeNextValue :: [Bool] -> State (World Bool) Bool
computeNextValue bits = do
        (algorithm, _) <- get
        let v = snd $ foldr (\b (e, c) -> if b then (2*e, c+e) else (2*e, c)) (1, 0) bits
        return (algorithm !! v)

--- main ---
part1Rounds = 2
part2Rounds = 50

run20 :: String -> IO ()
run20 input = do
    -- parse input
    let alg = map ('#' ==) . concat . takeWhile (not . null) $ lines input
    let initialLights = createGrid False . map (map ('#' == )) . tail . dropWhile (not . null) $ lines input

    -- execute tasks
    let (_, (_, finalGridPart1)) = execState (iter part1Rounds) (alg, (False, initialLights))
    let (_, (_, finalGridPart2)) = execState (iter part2Rounds) (alg, (False, initialLights))

    -- print results
    putStrLn $ "Lights after " ++ show part1Rounds ++ " rounds: " ++ show (countLight finalGridPart1)
    putStrLn $ "Lights after " ++ show part2Rounds ++ " rounds: " ++ show (countLight finalGridPart2)


