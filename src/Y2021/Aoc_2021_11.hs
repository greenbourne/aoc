module Y2021.Aoc_2021_11
( run11,
)
where

import Control.Monad.State.Lazy
import Control.Monad.Loops
import Control.Monad.ST
import Control.Monad (replicateM)
import Data.Bifunctor (bimap, second, first)
import Data.Char (digitToInt)
import Data.Maybe (mapMaybe)
import Data.Array.Base
import Data.Array.ST


type Point = (Int, Int)

runPart2 :: [[Int]] -> Int
runPart2 values = runST $ do
        result <- runStateT (runIterations values) 0
        return $ snd result

runIterations :: [[Int]] -> StateT Int (ST s) Int
runIterations values =
  do arr <- createArray (0,0) ((length . head $ values) -1, length values -1) (concat values)
     let numberElements = foldr (\e acc -> acc + length e) 0 values
     iterateUntil (numberElements ==) (step arr)

createArray :: Point -> Point -> [Int] -> StateT Int (ST s) (STUArray s Point Int)
createArray begin end initValues =
  lift $ newListArray (begin,end) initValues

step :: STUArray s Point Int -> StateT Int (ST s) Int
step arr = do
        n <- get
        put (n + 1)
        assoc <- lift $ getAssocs arr
        let cascade = filter ((==) 9 . snd) assoc
        lift $ mapM_ (uncurry (writeArray arr) . second (\e ->  if e == 9 then 0 else e +1)) assoc
        (+) (length cascade) . sum <$> mapM (energyBumpNeighbours arr . fst) cascade


energyBumpNeighbours :: STUArray s Point Int -> Point -> StateT Int (ST s) Int
energyBumpNeighbours arr p = do
        bs <- lift $ getBounds arr
        let ms = filter ((0,0) /=) [(a,b) | a <- [-1,0,1], b <- [-1,0,1]]
            coords = filter (\(x,y) -> x >= 0 && y >= 0 && x <= fst (snd bs) && y <= snd (snd bs)) $ map (\m -> bimap (fst m +) (snd m +) p) ms
        n2Values <- lift $ mapM (\c -> (,) c <$> readArray arr c) coords
        let noCascade = filter (\p -> snd p < 9 && snd p > 0) n2Values
        let cascade = filter ((==) 9 . snd) n2Values
        lift $ mapM_ (uncurry (writeArray arr) . second (1 +)) noCascade
        lift $ mapM_ (uncurry (writeArray arr) . second (const 0)) cascade
        (+) (length cascade) . sum <$> mapM (energyBumpNeighbours arr . fst) cascade

-- main
run11 :: String -> IO ()
run11 input = do
        let parsed = map (map digitToInt) $ lines input
            a = runPart2 parsed
        print a
