module Y2021.Aoc_2021_13
( run13,
)
where

import Control.Monad.State
import Data.Functor.Identity
import Data.List (nub)
import qualified Data.Set as S
import Text.Parsec hiding (State)

-- Helper types
data Fold = X Int | Y Int deriving (Show)
type Point = (Int, Int)

-- Parsing
parseInput :: ParsecT String u Identity ([Point], [Fold])
parseInput = do
        points <- parsePoint `sepEndBy` newline
        newline
        folds <- parseFold `sepEndBy` newline
        return (points, folds)

parsePoint :: ParsecT String u Identity Point
parsePoint = do
        x <- (read :: String -> Int) <$> many1 digit
        char ','
        y <- (read :: String -> Int) <$> many1 digit
        return (x,y)

parseFold :: ParsecT String u Identity Fold
parseFold = do
        string "fold along "
        axis <- letter
        char '='
        n <- (read :: String -> Int) <$> many1 digit
        if axis == 'y'
            then return $ Y n
            else return $ X n

-- Part1
iter :: [Fold] -> State [Point] ()
iter [] = return ()
iter (f:fs) = do
        oneRound f
        iter fs

oneRound :: Fold -> State [Point] ()
oneRound f = do
        ps <- get
        put . nub $ map (foldPoint f) ps
        return ()

foldPoint :: Fold -> Point -> Point
foldPoint (X n) (px,py) =
        if px > n
          then (n - (px -n), py)
          else (px,py)
foldPoint (Y n) (px,py) =
        if py > n
          then (px, n - (py - n))
          else (px,py)

prettyPrint :: [Point] -> IO ()
prettyPrint ps = do
        let height = maximum . map snd $ ps
            width = maximum . map fst $ ps
            pSet = S.fromList ps
            in forM_ [0..height] (\y -> do
                    forM_ [0..width] (\x ->
                        if (x,y) `S.member` pSet
                            then putStr "#"
                            else putStr " "
                            )
                    putStr "\n"
                            )

run13 :: String -> IO ()
run13 input = do
    case runParser parseInput () "" input of
        Left e ->  print e
        Right (points, folds) -> do
            let (_, s) = runState (iter folds) points
            print s
            prettyPrint s
