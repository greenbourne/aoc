module Y2021.Aoc_2021_12
( run12,
)
where

import Control.Monad.State
import Data.Char (isUpper)
import Data.List (nub)
import Data.List.Split (splitOn)
import Data.Maybe (fromJust)
import qualified Data.Map as M
import qualified Data.Set as S

import qualified Data.Vector as V
--- Helper Types ---
type Grid a = V.Vector (V.Vector a)
type Direction = (Int, Int)

--- Utility functions ---
(!!?) :: Grid a -> Direction -> Maybe a
(!!?) g (c, r) = do
    col <- g V.!? c
    col V.!? r

--- Parse Input ---
parseDirection :: String -> [(String,String)]
parseDirection = f . splitOn "-"
        where f s = [(head s, s!!1), (s!!1, head s)]

getVerticeIndices :: [(String, String)] -> M.Map String Int
getVerticeIndices directions =
        let names = nub . filter (\d -> d /= "start" && d /= "end") . concatMap (\d -> [fst d, snd d]) $ directions
            in M.fromList $ [("start", 0),("end",1)] ++ zip names [2..]

mapIndices2Name :: M.Map String Int -> M.Map Int String
mapIndices2Name = M.fromList . map (\p -> (snd p, fst p)) . M.toList

createAdjMatrix :: Int -> [(Int, Int)] -> V.Vector (V.Vector Bool)
createAdjMatrix n ds = let dirFromVert = map (\i -> S.fromList . map snd $ filter (\d -> fst d == i) ds) [0..n] :: [S.Set Int]
                           in V.fromList (map (V.fromList . transform2Row) dirFromVert)
                           where transform2Row :: S.Set Int -> [Bool]
                                 transform2Row targets = map (`S.member` targets) [0..n]

isMultipleVisit :: String -> Bool
isMultipleVisit = isUpper . head

-- part 1 & 2
getDistinctPaths1 :: V.Vector (V.Vector Bool) -> M.Map Int String -> State [Int] [[Int]]
getDistinctPaths1 adjMatrix m = do
        currentPath <- get
        -- Get all possibile successors
        let currentVert = head currentPath
            successors = map snd . filter fst $ zip (V.toList $ adjMatrix V.! currentVert) [0..]

        -- Filter for non-visited or can be visited again
            allowedSuccessors = filter (\s -> isMultipleVisit (fromJust (M.lookup s m)) || s `notElem` currentPath)successors
        if currentVert == 1
            then return [currentPath]
            else concat <$> mapM (\s -> do
             put (s:currentPath)
             getDistinctPaths1 adjMatrix m
             ) allowedSuccessors


getDistinctPaths2 :: V.Vector (V.Vector Bool) -> M.Map Int String -> State (Bool,[Int]) [[Int]]
getDistinctPaths2 adjMatrix m = do
        (smallTwice, currentPath) <- get
        -- Get all possibile successors
        let currentVert = head currentPath
            successors = map snd . filter fst $ zip (tail . V.toList $ adjMatrix V.! currentVert) [1..]

        -- Filter for non-visited or can be visited again
            allowedSuccessors = filter (\s -> isMultipleVisit (fromJust (M.lookup s m)) || s `notElem` currentPath) successors
        if currentVert == 1
            then return [currentPath]
            else concat <$> mapM (\s -> do
             if s `elem` allowedSuccessors
                 then do
                     put (smallTwice, s:currentPath)
                     getDistinctPaths2 adjMatrix m
                 else do
                     -- Check if one small cave has been visited twice already
                     if smallTwice
                         then return []
                         else do
                             put (True, s:currentPath)
                             getDistinctPaths2 adjMatrix m
             ) successors

-- main
run12 :: String -> IO ()
run12 input = do
        let directionsString = concatMap parseDirection $ lines input
            vertMap = getVerticeIndices directionsString
            numberVerts = M.size vertMap - 1
            directions = map (\d -> (fromJust $ M.lookup (fst d) vertMap, fromJust $ M.lookup (snd d) vertMap)) directionsString :: [(Int,Int)]
            adjMatrix = createAdjMatrix numberVerts directions
            (paths1, _) = runState (getDistinctPaths1 adjMatrix (mapIndices2Name vertMap)) [0]
            (paths2, _) = runState (getDistinctPaths2 adjMatrix (mapIndices2Name vertMap)) (False, [0])
        putStrLn $ "Part1: " ++ show (length paths1)
        putStrLn $ "Part2: " ++ show (length paths2)
