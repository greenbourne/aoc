module Y2021.Aoc_2021_14
  ( run14,
  )
where
import Data.Functor.Identity
import Data.List (maximumBy, minimumBy)
import Data.Function (on)
import qualified Data.Map.Strict as M
import qualified Data.Set as S
import Text.Parsec hiding (Empty)

---- parsing ----
parseInput :: ParsecT String u Identity (String, M.Map String String)
parseInput = do
       input <- many1 letter
       newline
       newline
       m <- parseRules
       return (input, m)

parseRules :: ParsecT String u Identity (M.Map String String)
parseRules = do rules <- parseRule `endBy` newline
                return $ M.fromList rules

parseRule :: ParsecT String u Identity (String, String)
parseRule = do
        from <- many1 letter
        string " -> "
        to <- many1 letter
        return (from, to)

---- part 1/2 -----
initMap :: String -> M.Map String Int
initMap input = foldr (\k m -> M.insertWith (\_ old -> old + 1) k 1 m) M.empty (initialPairs 0)
                            where initialPairs :: Int -> [String]
                                  initialPairs n
                                           | n == (length input) - 1 = []
                                           | otherwise = ((input !! n) : (input !! (n + 1)): []) : initialPairs (n+1)

iterationUntil :: Int -> M.Map String String -> M.Map String Int -> M.Map String Int
iterationUntil n rules current
                | n == 0 = current
                | otherwise = iterationUntil (n-1) rules (iteration rules current)

iteration :: M.Map String String -> M.Map String Int -> M.Map String Int
iteration rules current = foldr (\(polyPair, count) acc -> 
                                      let newPolyPairs = applyRule polyPair
                                          in foldr (\newPolyPair acc' -> M.insertWith (\new old -> new + old) newPolyPair count acc') acc newPolyPairs
                                      ) M.empty (M.assocs current)
                    where applyRule :: String -> [String]
                          applyRule s@(x:y:[]) = case s `M.lookup` rules of
                                            Nothing -> error "omg this should not happen"
                                            Just v  -> [x:v, v++[y]]

countEntries :: S.Set Char -> M.Map String Int -> M.Map Char Int
countEntries s pairMap = let entries = M.assocs pairMap
                             in M.fromList $ map (\polymer -> (polymer , foldr (\(pair, count) acc ->
                                    if (pair == polymer:polymer:[])
                                        then acc + 2*count
                                        else acc + count
                                        ) 0 $ filterRelevantPairs polymer entries)) (S.toList s)
                             where filterRelevantPairs :: Char -> [(String, Int)] -> [(String, Int)]
                                   filterRelevantPairs c entries = filter (\(pp, _) -> c `elem` pp) entries

getAllPolymers :: M.Map String String -> S.Set Char
getAllPolymers m = let keys = M.keys m
                       values = M.elems m
                       in S.fromList $ concat keys ++ concat values

run14 :: String -> IO ()
run14 input = do
        let parsed = runParser parseInput () "" input
        case parsed of
            Left e -> print e
            Right (start, rules) -> do
                let initial = initMap start
                    result10 = iterationUntil 40 rules initial
                    counts = countEntries (getAllPolymers rules) result10
                    fixEndings = M.map (\c -> if even c then c `div` 2 else (c+1) `div` 2) counts
                    max = maximumBy (compare `on` snd) (M.assocs fixEndings)
                    min = minimumBy (compare `on` snd) (M.assocs fixEndings)

                putStrLn $ "Solution Part2: " ++ show (snd max - snd min)
