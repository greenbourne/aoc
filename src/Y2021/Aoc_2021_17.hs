module Y2021.Aoc_2021_17
  ( run17,
  )
where

import Data.Functor.Identity
import Data.Range
import Text.Parsec hiding (Empty)
import Data.List (maximumBy)

--- parsing ----
parseInput :: ParsecT String u Identity (Range Int, Range Int)
parseInput = do
       string "target area: x="
       startX <- parseNegativeNumber <|> parsePositiveNumber
       string ".."
       endX <- parseNegativeNumber <|> parsePositiveNumber
       string ", y="
       startY <- parseNegativeNumber <|> parsePositiveNumber
       string ".."
       endY <- parseNegativeNumber <|> parsePositiveNumber
       return (startX +=+ endX, startY +=+ endY)

parsePositiveNumber :: ParsecT String u Identity Int
parsePositiveNumber = (read :: String -> Int) <$> many1 digit

parseNegativeNumber :: ParsecT String u Identity Int
parseNegativeNumber = do
        try $ char '-'
        ((*) (-1) . read :: String -> Int) <$> many1 digit

--- part 1 ---
getHighestY :: Range Int -> Range Int -> Int
getHighestY rangeX@(SpanRange startX endX) rangeY@(SpanRange startY endY) =
        let deltaX = boundValue startX
            maxY = abs $ deltaX `div` 2
            maxX = abs $ deltaX `div` 2
            throws = [createRow (boundValue endX) (boundValue endY) mx my (0,0)| mx <- [1..maxX], my <- [1..maxY]]
            hitPond = filter (any (\p -> rangeX `inRange` fst p && rangeY `inRange` snd p)) throws
            in maximum $ map (snd . maximumBy (\this other ->
                   if snd this > snd other
                       then GT
                       else if snd this < snd other
                                then LT
                                else EQ
                   )) hitPond
getHighestY _ _ = undefined

--- part 2 ---
getAllThrows :: Range Int -> Range Int -> Int
getAllThrows rangeX@(SpanRange startX endX) rangeY@(SpanRange startY endY) =
        let deltaX = boundValue startX
            maxY = abs $ deltaX `div` 2
            throws = [((mx,my),createRow (boundValue endX) (boundValue startY) mx my (0,0))| mx <- [1..(boundValue endX)], my <- [(boundValue startY)..maxY]]
            hitPond = filter (any (\p -> rangeX `inRange` fst p && rangeY `inRange` snd p) . snd) throws
            in length hitPond
getAllThrows _ _ = undefined

createRow :: Int -> Int -> Int -> Int -> (Int, Int) -> [(Int, Int)]
createRow maxX maxY x y prev
       | fst prev > maxX = []
       | snd prev < maxY = []
       | otherwise = let nextX = if x == 0 then 0 else x - 1
                         nextPoint = (fst prev+x, snd prev+y)
           in nextPoint : createRow maxX maxY nextX (y-1) nextPoint


run17 :: String -> IO ()
run17 input = do
    let parsed = runParser parseInput () "" input
    case parsed of
        Left e -> print e
        Right (rX, rY) -> do
            putStrLn $ "Part 1: " ++ show (getHighestY rX rY)
            putStrLn $ "Part 2: " ++ show (getAllThrows rX rY)
