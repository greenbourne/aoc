module Y2021.Aoc_2021_18
  ( run18,
  )
where

import Data.Functor.Identity
import Text.Parsec hiding (Empty)
import Text.Parsec.Token (GenTokenParser(symbol))

--- data types ---
data BinTree a = Leaf a | Branch (BinTree a) (BinTree a) deriving (Show, Eq)

--- parsing ----
parseInput :: ParsecT String u Identity [BinTree Int]
parseInput = parseBinTree `sepEndBy` newline

parseBinTree :: ParsecT String u Identity (BinTree Int)
parseBinTree = (Leaf <$> parseLeaf) <|> between (char '[') (char ']') parseBranch

parseLeaf :: ParsecT String u Identity Int
parseLeaf = (read :: String -> Int) <$> many1 digit

parseBranch :: ParsecT String u Identity (BinTree Int)
parseBranch = do
        (l:r:_) <- parseBinTree `sepBy1` char ','
        return $ Branch l r

--- part 1 ---
canSplit :: BinTree Int -> Bool
canSplit (Leaf a) = a >= 10
canSplit (Branch l r) = canSplit l || canSplit r

split :: BinTree Int -> (Bool, BinTree Int)
split (Leaf a)
    | a >= 10 && even a = (True, Branch (Leaf (a `div` 2)) (Leaf (a `div` 2)))
    | a >= 10 = (True, Branch (Leaf (a `div` 2)) (Leaf ((a + 1) `div` 2)))
    | otherwise = (False, Leaf a)
split (Branch l r) = let (lB, lT) = split l
                         in if lB
                                then (True, Branch lT r)
                                else let (rB, rT) = split r
                                         in (rB, Branch lT rT)

canExplode :: BinTree Int -> Bool
canExplode = (<=) 4 . getDepth
        where getDepth :: BinTree Int -> Int
              getDepth (Leaf _) = 0
              getDepth (Branch (Leaf _) (Leaf _)) = 0
              getDepth (Branch lB@(Branch _ _) (Leaf _)) = 1 + getDepth lB
              getDepth (Branch (Leaf _) rB@(Branch _ _)) = 1 + getDepth rB
              getDepth (Branch lB@(Branch _ _) rB@(Branch _ _)) = 1 + max (getDepth lB) (getDepth rB)

explode :: Int -> BinTree Int -> (Bool, Maybe Int, Maybe Int, BinTree Int)
explode currentDepth l@(Leaf _) = (False, Nothing, Nothing, l)
explode currentDepth br@(Branch (Leaf a) (Leaf b))
        | currentDepth >= 4 = (True, Just a, Just b, Leaf 0)
        | otherwise = (False, Nothing, Nothing, br)
explode currentDepth br@(Branch lB@(Branch _ _) (Leaf b)) =
        let (exploded, explodeValueL, explodeValueR, rLB) = explode (currentDepth + 1) lB
            in if exploded
                   then
                       case explodeValueR of
                           Nothing -> (True, explodeValueL, explodeValueR, Branch rLB (Leaf b))
                           Just value -> (True, explodeValueL, Nothing, Branch rLB (addToPathMostRight value (Leaf b)))
                   else (False, explodeValueL, explodeValueR, br)
explode currentDepth br@(Branch (Leaf a) rB@(Branch _ _)) =
        let (exploded, explodeValueL, explodeValueR, rRB) = explode (currentDepth + 1) rB
            in if exploded
                   then
                       case explodeValueL of
                           Nothing -> (True, explodeValueL, explodeValueR, Branch (Leaf a) rRB)
                           Just value -> (True, Nothing, explodeValueR, Branch (addToPathMostLeft value (Leaf a)) rRB)
                   else (False, explodeValueL, explodeValueR, br)
explode currentDepth br@(Branch lB@(Branch _ _) rB@(Branch _ _)) =
        let (explodedLeft, explodeValueLLeft, explodeValueRLeft, rLB) = explode (currentDepth + 1) lB
            in if explodedLeft
                   then case explodeValueRLeft of
                       Nothing -> (True, explodeValueLLeft, explodeValueRLeft, Branch rLB rB)
                       Just value -> (True, explodeValueLLeft, Nothing, Branch rLB (addToPathMostLeft value rB))
                   else let (explodedRight, explodeValueLRight, explodeValueRRight, rRB) = explode (currentDepth + 1) rB
                            in if explodedRight
                                then case explodeValueLRight of
                                   Nothing -> (True, explodeValueLRight, explodeValueRRight, Branch lB rRB)
                                   Just value -> (True, Nothing, explodeValueRRight, Branch (addToPathMostRight value lB) rRB)
                                else (False, Nothing, Nothing, br)

addToPathMostRight :: Int -> BinTree Int -> BinTree Int
addToPathMostRight n (Leaf a) = Leaf (a + n)
addToPathMostRight n (Branch lB rB) = Branch lB (addToPathMostRight n rB)

addToPathMostLeft :: Int -> BinTree Int -> BinTree Int
addToPathMostLeft n (Leaf a) = Leaf (a + n)
addToPathMostLeft n (Branch lB rB) = Branch (addToPathMostLeft n lB) rB

addition :: BinTree Int -> BinTree Int -> BinTree Int
addition l r = explodeSplit (Branch l r)

explodeSplit :: BinTree Int -> BinTree Int
explodeSplit b
    | canExplode b = let (_, _, _, t) = explode 0 b
                         in explodeSplit t
    | canSplit b = let (_,s)  =  split b
                         in explodeSplit s
    | otherwise = b


magnitude :: BinTree Int -> Int
magnitude (Leaf a) = a
magnitude (Branch l r) = 3 * magnitude l + 2 * magnitude r

--- part 2 ---
biggestNumber :: [BinTree Int] -> Int
biggestNumber ts = maximum [ magnitude $ addition a b | a <- ts, b <- ts, a /= b]

--- run ---
run18 :: String -> IO ()
run18 input = do
    let parsed = runParser parseInput () "" input
    case parsed of
        Left e -> print e
        Right trees -> do
            let result1 = foldl1 addition trees
            putStrLn $ "Part 1: " ++ show (magnitude result1)
            putStrLn $ "Part 2: " ++ show (biggestNumber trees)

