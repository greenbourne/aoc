module Y2021.Aoc_2021_02
  ( run02,
  )
where

import Data.Functor.Identity
import Text.Parsec hiding (Empty)

-- parsing 
parseInput :: ParsecT String u Identity [Direction]
parseInput = parseDirection `sepEndBy` newline

parseDirection :: ParsecT String u Identity Direction
parseDirection = parseForward <|> parseDown <|> parseUp

parseForward :: ParsecT String u Identity Direction
parseForward = do
        string "forward "
        Forward . read <$> many1 digit

parseDown :: ParsecT String u Identity Direction
parseDown = do
        string "down "
        Down . read <$> many1 digit

parseUp :: ParsecT String u Identity Direction
parseUp = do
        string "up "
        Up . read <$> many1 digit

-- type
data Direction = Forward Int | Down Int | Up Int deriving (Show, Eq)

-- part 1
deepDive :: Int -> (Int, Int) -> [Direction] -> (Int, Int)
deepDive aim (x,y) (Forward f:ds) = deepDive aim (x+f, y+(aim*f)) ds
deepDive aim (x,y) (Down d:ds) = deepDive (aim+d) (x, y) ds
deepDive aim (x,y) (Up u:ds)   = deepDive (aim-u) (x, y) ds
deepDive _ (x,y) [] = (x,y)

-- main
run02 :: String -> IO ()
run02 input = do
    -- parse input
    let parsed = runParser parseInput () "" input
    case parsed of
        Right directions -> do
            let (end_x, end_y) = deepDive 0 (0,0) directions
            print $ end_x * end_y
        Left e -> print e
