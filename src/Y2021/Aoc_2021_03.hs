module Y2021.Aoc_2021_03
  ( run03,
  )
where

data Criteria = Oxygen | Scrubber deriving (Eq)
-- part 1
parseByCols :: [String] -> [[Bool]]
parseByCols input
    | null $ head input = []
    | otherwise =
        let col = map ((==) '1' . head) input
            in col: parseByCols (map tail input)

epsilonGamma :: [[Bool]] -> (Int, Int)
epsilonGamma cols = let l = length . head $ cols
                        commonBits = map ((<) (l `div` 2) . length . filter id ) cols
                        in (bitsToNumber commonBits, bitsToNumber $ map not commonBits)

bitsToNumber :: [Bool] -> Int
bitsToNumber = snd . foldr (\b (e, c) -> if b then (2*e, c+e) else (2*e, c)) (1, 0)

-- part 2
bitCriteria :: Criteria -> Int -> [[Bool]] -> Bool
bitCriteria c idx xs = let l = length xs
                           half = (fromIntegral l :: Double) / 2.0
                           numberTs = length . filter id . map ((==) True . flip (!!) idx) $ xs
                       in case c of
                           Oxygen -> (fromIntegral numberTs :: Double) >= half
                           Scrubber -> (fromIntegral numberTs :: Double)  < half

oxygenScrubber :: Criteria -> Int -> [[Bool]] -> Int
oxygenScrubber _ _ [a] = bitsToNumber a
oxygenScrubber c idx xs = let bit = bitCriteria c idx xs
                          in oxygenScrubber c (idx + 1) (filter ((==) bit . flip (!!) idx) xs)

-- main
run03 :: String -> IO ()
run03 input = do
    -- Part1
    let parsedCols = parseByCols $ lines input
        (gamma, epsilon) = epsilonGamma parsedCols
    print $ "Part 1: " ++ show (gamma * epsilon)

    -- Part 2
    let parsedRows = map (map ('1' ==)) $ lines input
    let oxygen = oxygenScrubber Oxygen 0 parsedRows
    let scrubber = oxygenScrubber Scrubber 0 parsedRows
    print $ "Part 1: " ++ show (scrubber * oxygen)

