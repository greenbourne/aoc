module Y2021.Aoc_2021_07
  ( run07,
  )
where

import Data.List.Split (splitOn)
import Data.List (sort)
-- part 1 & 2

step :: (Int,Int) -> [Int] -> (Int -> [Int] -> Int) -> Int
step (start,end) positions fuelF
    | start == end = fuelF start positions
    | start == (end - 1) = minimum [fuelF start positions, fuelF end positions]
    | otherwise =
        let fuelStart = fuelF start positions
            fuelEnd = fuelF end positions
            mid  = (end + start) `div` 2
            in if fuelStart < fuelEnd
                   then minimum $ step (start, mid) positions fuelF:[fuelStart, fuelEnd]
                   else minimum $ step (mid, end) positions fuelF:[fuelStart, fuelEnd]

fuel1 :: Int -> [Int] -> Int
fuel1 pos = foldr (\n acc -> if n < pos then acc + (pos - n) else acc + (n - pos)) 0

fuel2 :: Int -> [Int] -> Int
fuel2 pos = foldr (\n acc -> if n < pos then acc + sum [0 .. (pos - n)] else acc + sum [0..(n - pos)]) 0

-- main
run07 :: String -> IO ()
run07 input = do
        let positions = map (read :: String -> Int) $ splitOn "," input
            sorted = sort positions
        print $ "Part1 :" ++ show (step (head sorted, last sorted) sorted fuel1)
        print $ "Part2 :" ++ show (step (head sorted, last sorted) sorted fuel2)
