module Y2021.Aoc_2021_04
  ( run04,
  )
where

import Data.Functor.Identity
import qualified Data.Set as S
import Text.Parsec hiding (Empty)
import Text.Parsec.Char (space)

newtype Bingo = Bingo {
                getSets :: [S.Set Int]
                } deriving (Show, Eq)

parseInput :: ParsecT String u Identity ([Int], [Bingo])
parseInput = do
       numbers <- map (read :: String -> Int) <$> many1 digit `sepBy` char ','
       newline
       newline
       bingos <- parseBingo `sepBy` newline
       return (numbers, bingos)

parseBingo :: ParsecT String u Identity Bingo
parseBingo = do
        bingoLines <- count 5 $ map (read :: String -> Int) <$> (spaces *> many1 digit `sepBy` many1 (char ' ')) <* newline
        return $ Bingo (map S.fromList (generateBingoCombinations bingoLines))

generateBingoCombinations :: [[Int]] -> [[Int]]
generateBingoCombinations xs = xs ++ map (\idx -> map (!! idx) xs) [0..4]

-- part 1

bingoGame :: [Int] -> [Bingo] -> Maybe (Int, Bingo)
bingoGame [] _ = Nothing
bingoGame (n:ns) bs =
        let nextBingos = checkNumber n bs
            bingosThisRound = map (\b -> (b, hasBingo b)) nextBingos
            noHitsYet = map fst . filter (not . snd) $ bingosThisRound
            in if null noHitsYet
                   then Just (n, fst $ head bingosThisRound)
                   else bingoGame ns  noHitsYet


checkNumber :: Int -> [Bingo] -> [Bingo]
checkNumber n = map (Bingo . map (S.delete n) . getSets)

hasBingo :: Bingo -> Bool
hasBingo = any S.null . getSets

sumNumbers :: Bingo -> Int
sumNumbers = sum . S.toList . S.unions . getSets

-- main
run04 :: String -> IO ()
run04 input = do
    -- parse
    let parsed = runParser parseInput () "" input
    case parsed of
        Left e -> print e
        Right (numbers,bingos) -> do
            case bingoGame numbers bingos of
                Nothing -> print "Got nothing Bro"
                Just (lastNumber, hit) -> print $ lastNumber * sumNumbers hit
