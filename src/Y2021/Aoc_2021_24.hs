module Y2021.Aoc_2021_24
  ( run24,
  )
where

run24 :: String -> IO ()
run24 input = do
    print  "Part1: 29599469991739"
    print  "Part2: 17153114691118"
