{-# LANGUAGE TemplateHaskell #-}
module Y2021.Aoc_2021_25
  ( run25,
  )
where

import Control.Lens (over, set, view)
import Control.Lens.TH (makeLenses)
import Control.Monad.State
import Control.Monad.ST
import Control.Monad.Primitive
import Data.Functor.Identity
import Data.Maybe (catMaybes)
import qualified Data.Vector as V
import qualified Data.Vector.Mutable as MV
import Text.Parsec hiding (Empty, State)

data Element = EAST | SOUTH | EMPTY deriving (Enum, Show, Eq)

data GridState = GridState {
    _columns :: !Int,
    _rows :: !Int }
makeLenses ''GridState

parseInput :: ParsecT String u Identity (V.Vector Element, GridState)
parseInput = do
        lines <- parseLine `endBy` newline
        return ( V.concat lines,
               GridState { _columns = V.length $ head lines,
                         _rows = length lines })

parseLine :: ParsecT String u Identity (V.Vector Element)
parseLine = V.fromList . map toElement <$> many1 (noneOf "\n")

toElement :: Char -> Element
toElement c
    | c == '>' = EAST
    | c == 'v' = SOUTH
    | otherwise = EMPTY

-- Setter & Getter
accessEntry :: MV.MVector (PrimState (ST s)) Element -> (Int, Int) -> StateT GridState (ST s) Element
accessEntry mv (row, column) = do
    gs <- get
    MV.read mv (view columns gs * row + column)

setEntry :: MV.MVector (PrimState (ST s)) Element -> Element -> (Int, Int) -> StateT GridState (ST s) ()
setEntry mv e (row, column) = do
    gs <- get
    let index = view columns gs * row + column
    MV.write mv index e

-- Part 1
iter :: MV.MVector (PrimState (ST s)) Element -> StateT GridState (ST s) Int
iter mv = do
    gs <- get
    eastTargets <- canMoveTo mv EAST
    forM_ eastTargets (moveTo mv EAST)
    southTargets <- canMoveTo mv SOUTH
    forM_ southTargets (moveTo mv SOUTH)
    gs' <- get
    if null eastTargets && null southTargets
        then return 1
        else (1 +) <$> iter mv

moveTo :: MV.MVector (PrimState (ST s)) Element -> Element -> (Int, Int) -> StateT GridState (ST s) ()
moveTo mv direction pos@(r,c)
    | direction == EAST = do
        gs <- get
        setEntry mv EMPTY pos
        setEntry mv EAST (r, (c+1) `mod` view columns gs)
    | direction == SOUTH = do
        gs <- get
        setEntry mv EMPTY pos
        setEntry mv SOUTH ((r+1) `mod` view rows gs, c)
    | otherwise = return ()

canMoveTo :: MV.MVector (PrimState (ST s)) Element -> Element -> StateT GridState (ST s) [(Int,Int)]
canMoveTo _ EMPTY = return []
canMoveTo mv direction = do
        gs <- get
        catMaybes <$> forM [(r,c) | r <- [0..view rows gs - 1], c <- [0.. view columns gs -1]] (\(r,c) -> do
            elem <- accessEntry mv (r,c)
            if elem == direction
                then do
                    neighbourEast <- if elem == EAST
                                         then accessEntry mv (r, (c + 1) `mod` view columns gs)
                                         else accessEntry mv ((r+1) `mod` view rows gs, c)
                    if neighbourEast == EMPTY
                        then return $ Just (r,c)
                        else return Nothing
                else return Nothing
            )

runPart1:: V.Vector Element -> GridState -> Int
runPart1 v gs = runST $ do
        mv <- V.thaw v
        evalStateT (iter mv) gs

run25 :: String -> IO ()
run25 input = do
        let parsed = runParser parseInput () "" input
        case parsed of
            Left e -> print e
            Right (v, gs) -> do
                print (runPart1 v gs)
