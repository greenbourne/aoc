module Y2021.Aoc_2021_08
( run08,
)
where

import Control.Monad.State.Lazy

import qualified Data.Map as M
import qualified Data.Set as S
import Data.List.Split (splitOn)

import Data.Maybe (catMaybes)

data Signal = Signal {
            getSignalPatterns :: [String],
            getOutput :: [String]
            } deriving (Show)

parseSignal :: String -> Signal
parseSignal s =
        let a = splitOn "|" s
            signalPatterns = words (head a)
            output = words (last a)
            in Signal signalPatterns output

-- part 2
output :: Signal -> Int
output s = let signalMapping = deduce (getSignalPatterns s)
               outputNumbers = map (flip M.lookup signalMapping . S.fromList)(getOutput s)
               in (read :: String -> Int) (concat (catMaybes outputNumbers))


deduce :: [String] -> M.Map (S.Set Char) String
deduce s = let one = S.fromList . head $ filter ((==) 2 . length) s
               four =  S.fromList . head $ filter ((==) 4 . length) s
               seven =  S.fromList . head $ filter ((==) 3 . length) s
               eight =  S.fromList . head $ filter ((==) 7 . length) s
               sixNineZero = map S.fromList $ filter ((==) 6 . length) s
               six = head $ filter (\e -> e `S.intersection` one /= one ) sixNineZero
               nineZero = filter (\e -> e `S.intersection` one == one ) sixNineZero
               zero = head $ filter (\e -> length (e `S.intersection` four) == 3 ) nineZero
               nine = head $ filter (\e -> length (e `S.intersection` four) == 4 ) nineZero
               twoThreeFive = map S.fromList $ filter ((==) 5 . length) s
               five = head $ filter (\e -> length (e `S.intersection` six) == 5 ) twoThreeFive
               twoThree =  filter (/= five) twoThreeFive
               three = head $ filter (\e -> length (e `S.intersection` nine) == 5 ) twoThree
               two = head $ filter (\e -> length (e `S.intersection` nine) == 4 ) twoThree
               in M.fromList[
                            (zero, "0"),
                            (one, "1"),
                            (two, "2"),
                            (three, "3"),
                            (four, "4"),
                            (five, "5"),
                            (six, "6"),
                            (seven, "7"),
                            (eight, "8"),
                            (nine, "9")]

-- main
run08 :: String -> IO ()
run08 input = do
        let signals = map parseSignal $ lines input
            outputSum = sum $ map output signals
        print outputSum
