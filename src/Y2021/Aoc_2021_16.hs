module Y2021.Aoc_2021_16
  ( run16,
  )
where

import Data.List
import Data.Maybe
import Control.Monad.State.Strict

--- Helpers
bitChar :: Char -> Int
bitChar ch = fromMaybe (error $ "illegal char " ++ [ch]) $
    elemIndex ch "01"

bitsToInt :: String -> Int
bitsToInt = foldl' f 0 where
        f n c = 2*n + bitChar c

hexToBits :: Char -> String
hexToBits c
  | c == '0'  = "0000"
  | c == '1'  = "0001"
  | c == '2'  = "0010"
  | c == '3'  = "0011"
  | c == '4'  = "0100"
  | c == '5'  = "0101"
  | c == '6'  = "0110"
  | c == '7'  = "0111"
  | c == '8'  = "1000"
  | c == '9'  = "1001"
  | c == 'A'  = "1010"
  | c == 'B'  = "1011"
  | c == 'C'  = "1100"
  | c == 'D'  = "1101"
  | c == 'E'  = "1110"
  | otherwise = "1111"

rstrip = reverse . dropWhile (== '\n') . reverse

parseVersion :: String -> Int
parseVersion = bitsToInt . take 3

parseType :: String -> Int
parseType = bitsToInt . drop 3 . take 6

--- Types
type Version = Int
type Type    = Int

data Package = Operator Version Type [Package] | Literal Version Type Int deriving (Show, Eq)

type PacketState = State String

---- Literal parsing ----
parseLiteralPayload :: PacketState Int
parseLiteralPayload = bitsToInt <$> parseLiteralPayload'

parseLiteralPayload' :: PacketState String
parseLiteralPayload' = do
                s <- get
                if head s == '0'
                    then do
                         put (drop 5 s)
                         return (take 4 . drop 1 $ s)
                    else do
                         put (drop 5 s)
                         (\l -> (take 4 . drop 1 $ s) ++ l) <$> parseLiteralPayload'

---- Operator parsing ----
parseOperatorPayload :: PacketState [Package]
parseOperatorPayload = do
        s <- get
        let lengthTypeId = head s
        case lengthTypeId of
             '0' -> do
                 let lengthOfPackages = bitsToInt . drop 1 . take 16 $ s
                 let subPackageBegin = drop 16 s
                 put (take lengthOfPackages subPackageBegin)
                 subs <- parseSubPackageArray
                 put (drop lengthOfPackages subPackageBegin)
                 return subs

             _ -> do
                 put (drop 12 s)
                 let numberOfPackages = bitsToInt . drop 1 . take 12 $ s
                 forM [1..numberOfPackages] (const parsePackage)

----- Parse Package -----
parsePackage :: PacketState Package
parsePackage = do
        s <- get
        let version = parseVersion s
        let packetType = parseType s
        put (drop 6 s)
        if packetType == 4
            then Literal version packetType <$> parseLiteralPayload
            else Operator version packetType <$> parseOperatorPayload

parseSubPackageArray :: PacketState [Package]
parseSubPackageArray = do
        s <- get 
        if s == ""
            then return []
            else do
                p <- parsePackage
                sp <- parseSubPackageArray
                return $ p:sp

---- Part 1 ----
sumVersions :: Package -> Int
sumVersions (Literal v _ _) = v
sumVersions (Operator v _ ps) = v + sum (map sumVersions ps)

---- Part 2 ----
evalPackage :: Package -> Int
evalPackage (Literal _ _ v) = v
evalPackage (Operator _ 0 ps) = sum     $ map evalPackage ps
evalPackage (Operator _ 1 ps) = product $ map evalPackage ps
evalPackage (Operator _ 2 ps) = minimum $ map evalPackage ps
evalPackage (Operator _ 3 ps) = maximum $ map evalPackage ps
evalPackage (Operator _ 5 [p,q]) = if evalPackage p > evalPackage q then 1 else 0
evalPackage (Operator _ 6 [p,q]) = if evalPackage p < evalPackage q then 1 else 0
evalPackage (Operator _ 7 [p,q]) = if evalPackage p == evalPackage q then 1 else 0
evalPackage p  = error $ "no path for " ++ show p

run16 :: String -> IO ()
run16 input = do
        let bits = concatMap hexToBits (rstrip input)
        let package = evalState parsePackage bits
        putStrLn $ "Part 1: " ++ show (sumVersions package)
        putStrLn $ "Part 2: " ++ show (evalPackage package)
