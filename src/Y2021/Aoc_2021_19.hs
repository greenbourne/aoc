{-# LANGUAGE TemplateHaskell #-}
module Y2021.Aoc_2021_19
  ( run19,
  )
where

import Control.Monad (unless)
import Control.Monad.State
import Control.Lens (over, view)
import Control.Lens.TH (makeLenses)
import Data.Bifunctor (second)
import Data.Functor.Identity
import qualified Data.Map as M
import qualified Data.Matrix as MA
import Data.Maybe (listToMaybe, mapMaybe)
import Data.Monoid ((<>))
import qualified Data.Set as S
import qualified Data.Vector as V
import Text.Parsec hiding (Empty, State)
import Utility.ParseUtil (parseNegativeNumber, parsePositiveNumber)
import Utility.VectorUtil (addVec, subVec)

data Point = Point {
        x:: Int,
        y:: Int,
        z:: Int } deriving (Show, Eq)
instance Ord Point where
    compare a b = compare (x a) (x b) <> compare (y a) (y b) <> compare (z a) (z b)

data StateStructure = StateStructure {
    _visitedScanners :: !(S.Set Int),
    _unknownScannerPoints :: !(M.Map Int [Point]),
    _knownScannerPoints :: !(M.Map (V.Vector Int) [Point])
    } deriving (Eq, Show)
makeLenses ''StateStructure

-- All (physical) possible 90 degree rotations of a cube
matrixOrientations :: [MA.Matrix Int]
matrixOrientations = map MA.fromLists [
    [[1,0,0], [0,1,0], [0,0,1]],
    [[1,0,0], [0,0,-1], [0,1,0]],
    [[1,0,0], [0,-1,0], [0,0,-1]],
    [[1,0,0], [0,0,1], [0,-1,0]],
    [[0,-1,0], [1,0,0], [0,0,1]],
    [[0,0,1], [1,0,0], [0,1,0]],
    [[0,1,0], [1,0,0], [0,0,-1]],
    [[0,0,-1], [1,0,0], [0,-1,0]],
    [[-1,0,0], [0,-1,0], [0,0,1]],
    [[-1,0,0], [0,0,-1], [0,-1,0]],
    [[-1,0,0], [0,1,0], [0,0,-1]],
    [[-1,0,0], [0,0,1], [0,1,0]],
    [[0,1,0], [-1,0,0], [0,0,1]],
    [[0,0,1], [-1,0,0], [0,-1,0]],
    [[0,-1,0], [-1,0,0], [0,0,-1]],
    [[0,0,-1], [-1,0,0], [0,1,0]],
    [[0,0,-1], [0,1,0], [1,0,0]],
    [[0,1,0], [0,0,1], [1,0,0]],
    [[0,0,1], [0,-1,0], [1,0,0]],
    [[0,-1,0], [0,0,-1], [1,0,0]],
    [[0,0,-1], [0,-1,0], [-1,0,0]],
    [[0,-1,0], [0,0,1], [-1,0,0]],
    [[0,0,1], [0,1,0], [-1,0,0]],
    [[0,1,0], [0,0,-1], [-1,0,0]]
    ]

-- Parse Input
parseInput :: ParsecT String u Identity (M.Map Int [Point])
parseInput = do
    M.fromList <$> parseScanner `sepBy` newline

parseScanner :: ParsecT String u Identity (Int, [Point])
parseScanner = do
    string "--- scanner "
    n <- parsePositiveNumber
    string " ---"
    newline
    coords <- parsePoint `endBy` newline
    return (n, coords)

parsePoint :: ParsecT String u Identity Point
parsePoint = do
    xN <- parseNegativeNumber <|> parsePositiveNumber
    char ','
    yN <- parseNegativeNumber <|> parsePositiveNumber
    char ','
    zN <- parseNegativeNumber <|> parsePositiveNumber
    return Point { x = xN, y = yN, z = zN }

-- Utility
catPairs :: [(a,a)] -> [a]
catPairs [] = []
catPairs ((x,y):xys) = x:y:catPairs xys

distancePoints :: Point -> Point -> Float
distancePoints a b = sqrt (fromIntegral (x a - x b)^^2 + fromIntegral (y a - y b)^^2 + fromIntegral (z a - z b)^^2)

createVector :: Point -> Point -> V.Vector Int
createVector p q = V.fromList [x p - x q, y p - y q, z p - z q]

pointToVector :: Point -> V.Vector Int
pointToVector p = V.fromList [x p, y p, z p]

addPoint :: Point -> Point -> Point
addPoint a b = Point { x = x a + x b, y = y a + y b, z = z a + z b }

addVecToPoint :: Point -> V.Vector Int -> Point
addVecToPoint p v = Point { x = x p + v V.!0, y = y p + v V.! 1, z = z p + v V.!2 }

vecToPoint :: V.Vector Int -> Point
vecToPoint v = Point { x =v V.!0, y = v V.! 1, z =  v V.! 2 }

-- Part 1
part1And2 :: State StateStructure (Int,Int)
part1And2 = do
    findAllScannerAndBeacons
    allBeacons <- gets (M.elems . view knownScannerPoints)
    let allUniqueBeacons = S.size . S.fromList . concat $ allBeacons
    allScannerPos <- gets (M.keys . view knownScannerPoints)
    let maxManhattenDistance =  maximum [manhattenDistance a b | a <- allScannerPos, b <- allScannerPos, a /= b]
    return (allUniqueBeacons, maxManhattenDistance)

manhattenDistance :: V.Vector Int -> V.Vector Int -> Int
manhattenDistance a b = sum $ map (\idx -> abs (a V.! idx - b V.! idx)) [0..2]

findAllScannerAndBeacons :: State StateStructure ()
findAllScannerAndBeacons = do
    s <- get
    let unknown = M.assocs (view unknownScannerPoints s)
    forM_ unknown (\(scannerId,unPoints) -> do
        s' <- get
        let assocKnown = M.assocs (view knownScannerPoints s')
        forM_ assocKnown (\(kScannerPoint, kBeaconPoints) -> do
                visited <- gets (view visitedScanners)
                if scannerId `S.member` visited
                    then return ()
                    else do
                        case findNextScannerPos kBeaconPoints unPoints of
                            Nothing -> return ()
                            Just (newScannerPos, newBeaconsPos) -> do
                                s'' <- get
                                let s''' = over unknownScannerPoints (M.delete scannerId) $ over visitedScanners (S.insert scannerId) s''
                                let scannerRelPos = newScannerPos `addVec` kScannerPoint
                                let scannerRelBeacons = map (`addVec` newScannerPos) newBeaconsPos
                                let newState = over knownScannerPoints (M.insert newScannerPos (map vecToPoint scannerRelBeacons)) s'''
                                put newState
                                return ()
                            )
        )
    sAfter <- get
    unless (M.null (view unknownScannerPoints sAfter)) findAllScannerAndBeacons

findNextScannerPos :: [Point] -> [Point] -> Maybe (V.Vector Int, [V.Vector Int])
findNextScannerPos src target =
    let match = findMatch src target
        in case match of
            Nothing -> Nothing
            Just (orient, pointPairs) ->
                let srcPoint = fst $ head pointPairs
                    targetPoint = snd$ head  pointPairs
                    targetsOriented = MA.getMatrixAsVector . flip MA.multStd orient . MA.rowVector . pointToVector $ targetPoint
                    allTargetesOriented = map (MA.getMatrixAsVector . flip MA.multStd orient . MA.rowVector . pointToVector) target
                    in Just (subVec (pointToVector srcPoint) targetsOriented, allTargetesOriented)

findMatch :: [Point] -> [Point] -> Maybe (MA.Matrix Int,[(Point,Point)])
findMatch given target =
    let gV = getAllVecs given
        tV = getAllVecs target
        in listToMaybe $ mapMaybe (\mo ->
                case matchForOrientation gV tV mo of
                    Nothing -> Nothing
                    Just foo -> Just (mo,foo)) matrixOrientations

matchForOrientation :: M.Map (V.Vector Int) [(Point,Point)] -> M.Map (V.Vector Int) [(Point,Point)] -> MA.Matrix Int -> Maybe [(Point,Point)]
matchForOrientation src target orient =
    let tM = map MA.rowVector (M.keys target)
        targetWithOrient = map (MA.getMatrixAsVector . flip MA.multStd orient) tM
        targetZip = M.fromList $ zip targetWithOrient (M.keys target)
        interVs = S.intersection (S.fromList (M.keys src)) (S.fromList targetWithOrient)
        srcToTar = map (\inter -> (src M.! inter, target M.! (targetZip M.! inter))) $ S.toList interVs
        targetPoints = S.toList . S.fromList . catPairs $ concatMap (\v -> target M.! (targetZip M.! v)) (S.toList interVs)
        in if length targetPoints >= 12
               then Just $ matchPairs srcToTar
               else Nothing

matchPairs :: [([(Point, Point)], [(Point,Point)])] -> [(Point, Point)]
matchPairs pointPairs =
    let candidates = foldr (
                    \(srcPoints, targetPoints) acc ->
                        let startAdded = foldr ( \(srcStart, _) acc' -> M.insertWith (++) srcStart (map fst targetPoints) acc') acc srcPoints
                            endAdded = foldr ( \(_, srcEnd) acc' -> M.insertWith (++) srcEnd (map snd targetPoints) startAdded) startAdded srcPoints
                            in endAdded
            ) M.empty pointPairs
        in S.toList . S.fromList $ minimizeMap S.empty candidates

minimizeMap :: S.Set Point -> M.Map Point [Point] -> [(Point, Point)]
minimizeMap assigned m
    | M.null m = []
    | otherwise =
        let cleanseMap = M.filter (not . null) $ M.map (\ps -> S.toList $ S.fromList ps `S.difference` assigned) m
            one2one = filter ((==) 1 . length . snd) $ M.assocs m
            newPairs = map (second head) one2one
            newAssigned = S.fromList (concatMap (\p -> [fst p, snd p]) newPairs)
            in newPairs ++ minimizeMap (assigned `S.union` newAssigned) cleanseMap


getAllVecs :: [Point] -> M.Map (V.Vector Int) [(Point,Point)]
getAllVecs [] = M.empty
getAllVecs (p:ps) =
    let vs = [(createVector p q, q) | q <- ps]
        m = foldr (\v acc -> M.insertWithKey (\k new old -> new++old) (fst v) [(p,snd v)] acc) M.empty vs
        in M.unionWith (++) m $ getAllVecs ps

-- | Run task
run19 :: String -> IO ()
run19 input = do
    let parsed = runParser parseInput () "" input
    case parsed of
        Left e -> print e
        Right m -> do
            let startState = StateStructure {
                    _visitedScanners = S.singleton 0,
                    _unknownScannerPoints = M.filterWithKey (\k _ -> k /= 0) m,
                    _knownScannerPoints =  M.fromList [(V.fromList [0,0,0], m M.! 0)]
                    }
            let (part1, part2) = evalState part1And2 startState
            putStrLn $ "Part1: " ++ show part1
            putStrLn $ "Part2: " ++ show part2
