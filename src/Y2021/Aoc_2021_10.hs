module Y2021.Aoc_2021_10
( run10,
)
where

import Text.Parsec

import qualified Control.Monad.State.Lazy as S

import Data.Functor.Identity
import Data.List (sort)
import Data.Maybe (mapMaybe)

-- parse part1
parseChunk :: ParsecT String u Identity [()]
parseChunk = do
        many1 (parseCurly <|> parsePara <|> parseSpike <|> parsePara2)

parseCurly :: ParsecT String u Identity ()
parseCurly = do
        char '{'
        char '}' <|> (parseChunk >> char '}')
        return ()

parsePara :: ParsecT String u Identity ()
parsePara = do
        char '('
        char ')' <|> (parseChunk >> char ')')
        return ()

parsePara2 :: ParsecT String u Identity ()
parsePara2 = do
        char '['
        char ']' <|> (parseChunk >> char ']')
        return ()

parseSpike :: ParsecT String u Identity ()
parseSpike = do
        char '<'
        char '>' <|> (parseChunk >> char '>')
        return ()

-- part1
getValue :: Char -> Int
getValue c
    | c == ')' = 3
    | c == ']' = 57
    | c == '}' = 1197
    | otherwise = 25137

findIncompleted :: String -> Maybe String
findIncompleted l = let parsedLine = runParser parseChunk () "" l
                      in case parsedLine of
                             Right _ -> undefined
                             Left e -> let col = sourceColumn . errorPos $ e
                                           in if col >= length l
                                                  then Just l
                                                  else Nothing

findAllIncompleted :: [String] -> [String]
findAllIncompleted = mapMaybe findIncompleted

-- part2
getMissingClosings :: String -> S.State String String
getMissingClosings [] = reversePara <$> S.get
getMissingClosings (c:cs)
            | c == '[' || c == '(' || c == '{' || c == '<' = do
                    s <- S.get
                    S.put (c:s)
                    getMissingClosings cs
            | otherwise = do
                s <- S.get
                S.put (tail s)
                getMissingClosings cs

reversePara :: String -> String
reversePara = map getReversePara

getReversePara :: Char -> Char
getReversePara '[' = ']'
getReversePara '<' = '>'
getReversePara '{' = '}'
getReversePara c = ')'

computeScore :: String -> Int
computeScore = foldr (\c acc -> acc * 5 + getValue c) 0 . reverse
        where getValue ')' = 1
              getValue ']' = 2
              getValue '}' = 3
              getValue c = 4

-- main
run10 :: String -> IO ()
run10 input = do
        let incompleted = findAllIncompleted (lines input)
        let missingParas = map (\i -> S.evalState (getMissingClosings i) "") incompleted
        let s = sort . map computeScore $ missingParas
        print (s !! (length s `div` 2))
