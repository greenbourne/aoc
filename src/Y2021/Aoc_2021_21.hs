{-# LANGUAGE TemplateHaskell #-}
module Y2021.Aoc_2021_21
  ( run21,
  )
where

import Control.Lens (over, set, view)
import Control.Lens.TH (makeLenses)
import Control.Monad (join)
import Data.Bifunctor (Bifunctor(bimap))
import Data.Either (lefts, rights)
import Data.Functor.Identity
import Data.Map.Lazy as M
import Text.Parsec hiding (Empty, State)
import Text.Read (readMaybe)
import Data.Maybe (fromJust)
import qualified Data.List.NonEmpty as NE

parseInput :: ParsecT String u Identity (Int, Int)
parseInput = do
       string "Player 1 starting position: "
       posP1 <- parseNumber
       newline
       string "Player 2 starting position: "
       posP2 <- parseNumber
       return (posP1, posP2)

parseNumber :: ParsecT String u Identity Int
parseNumber = (castToInt :: String -> Int) <$> many1 digit
            where castToInt :: String -> Int
                  castToInt s = case readMaybe s of
                                    Just n -> n
                                    Nothing -> error $ s ++ " can't be parsed to a number."

--- Part 1
generateSteps :: ([Int], [Int])
generateSteps = let threeRounds = NE.take 300 $ NE.cycle $ NE.fromList [1..100]
                    allStepSizes = sumSteps threeRounds
                    in splitSteps allStepSizes
                where sumSteps :: [Int] -> [Int]
                      sumSteps [] = []
                      sumSteps [x] = error "This should not happen"
                      sumSteps [x,y] = error "This should not happen"
                      sumSteps (x:y:z:xs) = (x+y+z): sumSteps xs
                      splitSteps :: [Int] -> ([Int],[Int])
                      splitSteps [] = ([],[])
                      splitSteps [x] = error "This should not happen"
                      splitSteps (x:y:xs) = bimap (x:) (y:) $ splitSteps xs

getScores :: Int -> [Int] -> [Int]
getScores pos [] = []
getScores pos (step:steps) =
        nextPos: getScores nextPos steps
            where nextPos = if((pos + step) `mod` 10) == 0
                    then 10
                    else (pos + step) `mod` 10

getTurnReachingX :: Int -> Int -> [Int] -> Int
getTurnReachingX _ _ [] = error "this should not happen"
getTurnReachingX scoreToReach acc (x:xs)
                | acc >= scoreToReach = 0
                | otherwise        = 1 + getTurnReachingX scoreToReach (acc + x) xs

countTill :: Int -> [Int] -> Int
countTill 0 _ = 0
countTill _ [] = error "This should not happen"
countTill turns (step:steps) = step + countTill (turns-1) steps

part1 :: Int -> Int -> Int
part1 startPosP1 startPosP2 =
        let (movesP1, movesP2) = generateSteps
            scoresP1 = getScores startPosP1 movesP1
            scoresP2 = getScores startPosP2 movesP2
            turnsP1  = getTurnReachingX 1000 0 (cycle scoresP1)
            turnsP2  = getTurnReachingX 1000 0 (cycle scoresP2)
            numberOfTurns = if turnsP1 < turnsP2
                                then turnsP1*3 + (turnsP1-1)*3
                                else turnsP2*6
            pointsOtherPlayer = if turnsP1 < turnsP2
                                    then countTill (turnsP1-1) (cycle scoresP2)
                                    else countTill turnsP2 (cycle scoresP1)
            in numberOfTurns*pointsOtherPlayer

-- Part 2
data Player = Player
  { _points :: !Int
  , _position :: !Int
  } deriving (Eq,Show)
makeLenses ''Player

data GameState = GameState
   {
     _player1 :: !Player
   , _player2 :: !Player
    } deriving (Eq,Show)
makeLenses ''GameState
instance Ord GameState where
        compare gs1 gs2
            | view player1 gs1 == view player1 gs2 && view player2 gs1 == view player2 gs2 = EQ
            | view player1 gs1 /= view player1 gs2 = LT
            | otherwise = GT

move :: Bool -> Int -> GameState -> GameState
move isPlayer1 roll gs
    | isPlayer1 = over player1 (`updatePlayer` roll) gs
    | otherwise   = over player2 (`updatePlayer` roll) gs

getCurrentScore :: Bool -> GameState -> Int
getCurrentScore isPlayer1 gs
    | isPlayer1 = view points $ view player1 gs
    | otherwise = view points $ view player2 gs

updatePlayer :: Player -> Int -> Player
updatePlayer player roll =
        let currentPos =  view position player
            nextPos = if (currentPos + roll) `mod` 10 == 0
                        then 10
                        else (currentPos + roll) `mod` 10
        in set position nextPos $ over points (+nextPos) player

frequencies :: [(Int,Int)]
frequencies = [(3,1),(4,3),(5,6),(6,7),(7,6),(8,3),(9,1)]

scoreToReach :: Int
scoreToReach = 21

turn :: Bool -> M.Map GameState Int -> (M.Map GameState Int, Int)
turn isPlayer1 gameFreq =
        let rs = join $ Prelude.map (\(g, oldTimes) ->
                    Prelude.map (\(roll, times) ->
                        let newState = move isPlayer1 roll g
                            currentScore = getCurrentScore isPlayer1 newState
                            newCount = oldTimes*times
                            in if currentScore >= scoreToReach
                                then Left newCount
                                else Right (newState, newCount)
                        ) frequencies) (M.assocs gameFreq)
        in (Prelude.foldr (\(gs',count) acc -> M.insertWith (+) gs' count acc) M.empty (rights rs), sum $ lefts rs)

part2 :: Int -> Int -> (Int, Int)
part2 startPos1 startPos2 =
        let initialGS = GameState { _player1 = Player { _points = 0, _position = startPos1 }, _player2 = Player { _points = 0, _position = startPos2 } }
        in go True (M.singleton initialGS 1)
            where go :: Bool -> M.Map GameState Int -> (Int, Int)
                  go isPlayer1 gameFreq
                        | M.null gameFreq = (0,0)
                        | otherwise = let (gameFreq', victories) = turn isPlayer1 gameFreq
                                          (vPlayer1,vPlayer2) = go (not isPlayer1) gameFreq'
                                          in if isPlayer1
                                                 then (vPlayer1+victories,vPlayer2)
                                                 else (vPlayer1,vPlayer2+victories)

run21 :: String -> IO ()
run21 input = do
        let parsed = runParser parseInput () "" input
        case parsed of
            Left e -> print e
            Right p@(posP1, posP2) -> do
                putStrLn $ "Part1: " ++ show (part1 posP1 posP2)
                putStrLn $ "Part2: " ++ show (uncurry max $ part2 posP1 posP2)
