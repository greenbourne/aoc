{-# LANGUAGE LambdaCase #-}
module Y2021.Aoc_2021_15
  ( run15,
  )
where

import Control.Monad (when)
import Control.Monad.RWS.Lazy
import Data.Array
import Data.Bifunctor (bimap)
import Data.Char (digitToInt)
import Data.Maybe (fromMaybe)
import qualified Data.Map as M
import qualified Data.PSQueue as PSQ
import qualified Data.Set as S

-- Helper Types
type Distance = Int
type Point = (Int, Int)
type Graph = M.Map Point (S.Set (Point, Distance))

-- Build graph
mkGraph :: Array Point Distance -> Graph
mkGraph a = let ind = indices a
                (lBound, mBound) = bounds a
                in foldr (\p acc -> M.insert p (getDistances a . getNeighbours mBound $ p) acc) M.empty ind

getDistances :: Array Point Distance -> [Point] -> S.Set (Point, Distance)
getDistances a ps = S.fromList (map (\p -> (p, a ! p)) ps)

getNeighbours :: Point -> Point -> [Point]
getNeighbours maxBound current =
        let deltas = [(-1,0), (1,0), (0,-1), (0, 1)]
            allNeighbours = map (\d -> bimap (fst d +) (snd d +) current) deltas
            in filter (inRange ((0,0),maxBound)) allNeighbours

-- Part 1
dijkstra :: Point -> Point -> RWS Graph () (M.Map Point Distance, S.Set Point, PSQ.PSQ Point Int) ()
dijkstra destination current = do
        if current == destination
            then return ()
            else do
                graph <- ask
                let neighbours = graph M.! current
                (distanceMap, visited, pq) <- get
                let pq_deleteMin = PSQ.delete current pq
                put (distanceMap, visited, pq_deleteMin)
                let distanceCurrent =  distanceMap M.! current
                mapM_ (\(n_coord, n_dist) -> do
                    (distanceMap_loop, visited, pq_loop) <- get
                    let n_oldDistance = fromMaybe 30000 (n_coord `M.lookup` distanceMap_loop)
                    let n_newDistance = n_dist + distanceCurrent
                    when (n_newDistance < n_oldDistance) $ do
                            let updateVisited = S.insert current visited
                            let distance_u = M.insertWithKey (\k newVal _ -> newVal) n_coord n_newDistance distanceMap_loop
                            put (distance_u, updateVisited, PSQ.alter (\case
                                Just _ -> Just n_newDistance
                                Nothing -> Just n_newDistance ) n_coord pq_loop)) $ S.toList neighbours
                (bla, foo, pq_updated) <- get
                maybe (return ()) (dijkstra destination . PSQ.key) (PSQ.findMin pq_updated )

-- Part 2
generateTimes5 :: [[Int]] -> [[Int]]
generateTimes5 = generateBiggerMapVer . generateBiggerMapHor

generateBiggerMapHor :: [[Int]] -> [[Int]]
generateBiggerMapHor = map (\row ->
                                let row' = map increaseRiskByOne row
                                    row'' = map increaseRiskByOne row'
                                    row''' = map increaseRiskByOne row''
                                    row'''' = map increaseRiskByOne row'''
                                    in row ++ row' ++ row'' ++ row''' ++ row''''
                                )

generateBiggerMapVer :: [[Int]] -> [[Int]]
generateBiggerMapVer orig = let iter1 = iter orig
                                iter2 = iter iter1
                                iter3 = iter iter2
                                iter4 = iter iter3
                                in orig ++ iter1 ++ iter2 ++ iter3 ++ iter4
        where iter = map (map increaseRiskByOne)

increaseRiskByOne n = max ((n + 1) `mod` 10) 1

run15 :: String -> IO ()
run15 input = do
    let ls = lines input
        maxBound = (5 * length (head ls) - 1, (5 * length ls) - 1)
        riskList = map (map digitToInt) ls
        bigRiskList = generateTimes5 riskList
        arr = listArray ((0,0), maxBound) (concat bigRiskList)
        g = mkGraph arr
    let (_, (distanceMap, _,_), _) = runRWS (dijkstra maxBound (0,0)) g (M.singleton (0,0) 0, S.empty, PSQ.singleton (0,0) 0)
    print $ distanceMap M.! maxBound
