module Y2021.Aoc_2021_01
  ( run01,
  )
where

slidingWindow :: Int -> [Int] -> Int
slidingWindow 0 sonars = slidingWindow (sum $ take 3 sonars) (tail sonars)
slidingWindow n [a,b]  = 0
slidingWindow prev sonars = let slideValue = sum $ take 3 sonars
                                rec        = slidingWindow slideValue (tail sonars)
                                in if slideValue > prev
                                       then 1 + rec
                                       else rec


run01 :: String -> IO ()
run01 input = do
    -- parse input
    let sonars = map (read :: String -> Int) $ lines input

    -- part 1
    let part1 = fst . foldr (\s (incs, prev) -> if s > prev then (incs+1, s) else (incs, s)) (0, head sonars) $ reverse sonars
    print part1

    -- part 2
    print (slidingWindow 0 sonars)
