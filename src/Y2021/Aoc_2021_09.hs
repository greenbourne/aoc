module Y2021.Aoc_2021_09
( run09,
)
where

import Control.Monad.RWS.Lazy

import Control.Monad.Loops

import Data.Bifunctor (bimap, second, first)
import Data.Char (digitToInt)
import Data.List (sort)
import Data.Maybe (fromJust, isJust, mapMaybe, catMaybes)
import qualified Data.Set as S
import qualified Data.Vector as V

--- Helper Types ---
type Grid a = V.Vector (V.Vector a)
type Point = (Int, Int)

--- Utility functions ---
(!!?) :: Grid a -> Point -> Maybe a
(!!?) g (c, r) = do
    col <- g V.!? c
    col V.!? r

getNeighbours :: Grid a -> Point -> [a]
getNeighbours grid p =
        let ms = [(1,0),(0,1),(-1,0),(0,-1)]
            coords = map (\m -> bimap (fst m +) (snd m +) p) ms
            in mapMaybe (grid !!?) coords

getNeighboursI :: Grid a -> Point -> [(Point, a)]
getNeighboursI grid p =
        let ms = [(1,0),(0,1),(-1,0),(0,-1)]
            coords = map (\m -> bimap (fst m +) (snd m +) p) ms
            in map (second fromJust). filter (isJust . snd) $ map (\c -> (c, grid !!? c)) coords

-- part 1
getLowpoints :: Grid Int -> [Point]
getLowpoints g = let lpGrid = findLowPoints g
                     in catMaybes . concat . V.toList . V.map V.toList $ lpGrid

findLowPoints :: Grid Int -> Grid (Maybe Point)
findLowPoints g = V.imap (\c_idx r -> 
                         V.imap (\r_idx v->
                                let m = minimum $ g `getNeighbours` (c_idx, r_idx)
                                    in if v < m
                                           then Just (c_idx, r_idx)
                                           else Nothing
                                           ) r
                                           ) g

-- part 2
getBasins :: Grid Int -> [Point] -> [Int]
getBasins g ps = 
        let (_,_,w) = runRWS (iterateWhile isJust getBasinSize) g (ps, S.empty )
            in w

getBasinSize :: RWS (Grid Int) [Int] ([Point], S.Set Point) (Maybe Int)
getBasinSize = do
        p <- get
        if null $ fst p
            then return Nothing
            else do
                let lp = head $ fst p
                put (first tail p)
                basinSize <- stepThroughBasin lp
                tell [basinSize]
                return $ Just basinSize

stepThroughBasin :: Point -> RWS (Grid Int) [Int] ([Point], S.Set Point) Int
stepThroughBasin p = do
        s <- get
        if p `S.member` snd s
            then return 0
            else do
                put (second (S.insert p) s)
                g <- ask
                let allNeighbours = getNeighboursI g p
                    validNeighbours = map fst . filter (\r -> fst r `S.notMember` snd s) . filter ((/= ) 9 . snd) $ allNeighbours
                (+ 1) . sum <$> forM validNeighbours stepThroughBasin

-- main
run09 :: String -> IO ()
run09 input = do
        let parsed = map (map digitToInt) $ lines input
            grid = V.fromList . map V.fromList $ parsed
            basins = getBasins grid (getLowpoints grid)
            greatestBasins = take 3 . reverse $ sort basins
        print $ product greatestBasins
