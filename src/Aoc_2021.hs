module Aoc_2021 where

import Y2021.Aoc_2021_01
import Y2021.Aoc_2021_02
import Y2021.Aoc_2021_03
import Y2021.Aoc_2021_04
import Y2021.Aoc_2021_05
import Y2021.Aoc_2021_06
import Y2021.Aoc_2021_07
import Y2021.Aoc_2021_08
import Y2021.Aoc_2021_09
import Y2021.Aoc_2021_10
import Y2021.Aoc_2021_11
import Y2021.Aoc_2021_12
import Y2021.Aoc_2021_13
import Y2021.Aoc_2021_14
import Y2021.Aoc_2021_15
import Y2021.Aoc_2021_16
import Y2021.Aoc_2021_17
import Y2021.Aoc_2021_18
import Y2021.Aoc_2021_19
import Y2021.Aoc_2021_20
import Y2021.Aoc_2021_21
import Y2021.Aoc_2021_22
import Y2021.Aoc_2021_23
import Y2021.Aoc_2021_24
import Y2021.Aoc_2021_25

run2021 :: Int -> String -> IO ()
run2021 number input = do
    case number of
        1 -> run01 input
        2 -> run02 input
        3 -> run03 input
        4 -> run04 input
        5 -> run05 input
        6 -> run06 input
        7 -> run07 input
        8 -> run08 input
        9 -> run09 input
        10 -> run10 input
        11 -> run11 input
        12 -> run12 input
        13 -> run13 input
        14 -> run14 input
        15 -> run15 input
        16 -> run16 input
        17 -> run17 input
        18 -> run18 input
        19 -> run19 input
        20 -> run20 input
        21 -> run21 input
        22 -> run22 input
        23 -> run23 input
        24 -> run24 input
        25 -> run25 input
        _ -> putStrLn "Challenge does not exist"

