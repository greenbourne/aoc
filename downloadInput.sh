#!/usr/bin/env bash
set -x

if [[ $# -ne 1 && $# -ne 2 ]]
then
    echo "Missing input!"
    echo "Usage: downloadInput.sh YEAR DAY"
    exit 1
fi

if [ -z "$AOC_SESSION" ]
then
    echo 'Read session id from file token'
    AOC_SESSION=$(cat token)
fi

if [[ $# -eq 1 ]]
then
    echo "Download all input of year $1"
    for i in {1..25}; do mkdir -p data/"$1"/"$i"/;curl --cookie "session=53616c7465645f5f561a06938fdf9bf3d3ca364c3a694c0995df88923013380ede9bc92a7f5c4ce726b43d078e2561ebea168f9f497487341146d3f8720409ee"  -o data/"$1"/"$i"/input.txt https://adventofcode.com/"$1"/day/"$i"/input; done
fi
if [[ $# -eq 2 ]]
then
    curl --cookie "session=$AOC_SESSION" https://adventofcode.com/"$1"/day/"$2"/input
fi
